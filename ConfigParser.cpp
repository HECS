

#include "ConfigParser.h"


const char STARTBRACKET = '[';
const char ENDBRACKET = ']';
const char COMMENT = '#';
const char NEWLINE = '\n';

ConfigParser::ConfigParser () { }

ConfigParser::~ConfigParser () { }


ConfigParser::ConfigParser (const QString& fileName)
{
    fileStr = fileName;
    openFile();
}


void ConfigParser::openFile ()
{
	
      QString secStr, tokStr;
      QFile file(fileStr);

      
      if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
      {
           fileExists = false;
           return;
      }

      fileExists = true;
      QTextStream in(&file);
      QString line = "";      

      while (!line.isNull())
      {
         line = in.readLine();
         tokStr = line.trimmed();

        if (tokStr.startsWith(STARTBRACKET))
            {
                secStr = parseSection(tokStr);
                continue;    
            }

        else if (tokStr.startsWith(COMMENT))
            continue ; 
        else if (tokStr.isEmpty())
            continue ; 
        else if (secStr.startsWith("HLE"))
                parseHleEntry(secStr,tokStr);
        else
            parseEntry(secStr,tokStr);
      }
}

bool ConfigParser::getFileExists()
{
    return fileExists;
}

    
    //Parse Tokens of line

QString ConfigParser::parseSection (const QString& lineStrIn)
{

        QString lineStr;
	lineStr = lineStrIn.trimmed();
	lineStr = lineStr.remove (STARTBRACKET);
	lineStr = lineStr.remove (ENDBRACKET);
	
        //qDebug ("" + lineStr.toLatin1());
	return lineStr;
}

void ConfigParser::parseEntry (const QString& secName, const QString& token)
{
	QString keyname, value;
	
	keyname = token.section('=', 0, 0);
	keyname = keyname.trimmed();
	keyname = secName+ ":" + keyname;
	
	value = token.section('=', 1, 1);
	value = value.trimmed();

	inputData[keyname] = value;

}

void ConfigParser::parseHleEntry (const QString& secName, const QString& token)
{
	QString keyname, value;

	keyname = token.section('=', 0, 0);
        keyname = keyname.trimmed();
	value = token.section('=', 1, 1);
	value = value.trimmed();

        if (keyname == "name")
            m_currentElemName = value;

        //HLEGarage:name = door
	keyname = secName+ ":" + m_currentElemName + ":" + keyname;
	

        //Stick in a StringList to use elsewhere
        elementKeyNames << keyname;        

	inputData[keyname] = value;

}

QMap <QString, QString>  ConfigParser::getInputData() const 
{
    return inputData;
}

double ConfigParser::getValueDouble (const QString& key)
{
	QString value;

        //If key doesn't exist silently return 0.00
        if (!inputData.contains(key))
            return 0.00;

	value = inputData[key];
	return value.toDouble();
}

int ConfigParser::getValueInt (const QString& key)
{
	QString value;
        
        //If key doesn't exist silently return 0
        if (!inputData.contains(key))
            return 0;

	value = inputData[key];
	return value.toInt();
}


QString ConfigParser::getValueString (const QString& key)
{
	QString value;
        
        //If key doesn't exist silently return 0
        if (!inputData.contains(key))
            return " ";

	value = inputData[key];
	return value;
}

bool ConfigParser::getValueBool (const QString& key)
{
	QString value;
        
        //If key doesn't exist silently return 0
        if (!inputData.contains(key))
            return 0;

	value = inputData[key];
	//return value.toBool();
        return ' ';
}

QStringList ConfigParser::getElementKeyNames()  const
{
    return elementKeyNames;
}

QStringList ConfigParser::findHleEntries ()
{
  QMapIterator <QString, QString> i(inputData);
  QStringList valsOut;
   QString item,lastElement,currElement;

   while (i.hasNext())
   {
       i.next();
       item = i.key();
       if (item.startsWith("HLE"))
        {
                        //Chop HLE Bit
            item.remove(0, 3);
            currElement = item.section(':',0,0);
            
            if (currElement == lastElement)
                continue;
            else
            {
                valsOut << currElement;
                lastElement = currElement;
            }
        }
   }
   return valsOut;
}
