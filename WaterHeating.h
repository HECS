/***************************************************************************
 *                                                                         *
 *  WaterHeating.h Copyright (C) 2008 by Jon Rumble                        *
 *  j.w.rumble@reading.ac.uk   				                   *
 *                                                                         *
 *  This file is part of HECS,                                             *
 *                                                                         *
 *  HECS is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation, either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *  HECS is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/


#ifndef WATERHEATING_H
#define WATERHEATING_H

#include "DwellingDimensions.h"
#include "ConfigParser.h"
#include "DwellingDimensions.h"
#include <cmath>

class WaterHeating
  {

  public:

    // Constructors/Destructors
    //


    /**
     * CONSTRUCTORS
     */
    WaterHeating(const ConfigParser& config,DwellingDimensions *dims);
    WaterHeating();
    ~WaterHeating();

    void initVars();
    /**
     * METHODS
     */
    //Accessors
      void setEnergyContentHotWater ( double val_in );
      double getEnergyContentHotWater ( );
      void setDistributionLoss ( double val_in );
      double getDistributionLoss ( );
      void setHotWaterUsage ( double val_in );
      double getHotWaterUsage ( );
      void setManDeclaredLoss ( double val_in );
      double getManDeclaredLoss ( );
      void setTempFactor ( double val_in );
      double getTempFactor ( );
      void setEnergyLostWaterStore ( double val_in );
      double getEnergyLostWaterStore ( );
      void setCylinderVolume ( double val_in );
      double getCylinderVolume ( );
      void setHotWaterStoreLossFactor ( double val_in );
      double getHotWaterStoreLossFactor ( );
      void setVolumeFactor ( double val_in );
      double getVolumeFactor ( );
      void setSolarStorageVal ( double val_in );
      double getSolarStorageVal ( );
      void setPrimaryCircuitLoss ( double val_in );
      double getPrimaryCircuitLoss ( );
      void setCombiLoss ( double val_in );
      double getCombiLoss ( );
      void setSolarDhw ( double val_in );
      double getSolarDhw ( );
      void setWaterHeaterOutput ( double val_in );
      double getWaterHeaterOutput ( );
      void setHotWaterHeatGains ( double val_in );
      double getHotWaterHeatGains ( );
      void calcAll();

    

  private:

    // Private attributes
    //
    ConfigParser cf;
    DwellingDimensions *ptrDims; 
    
     double m_energyContentHotWater;
     double m_distributionLoss;
     double m_hotWaterUsage;
     double m_manDeclaredLoss;
     double m_tempFactor;
     double m_energyLostWaterStore;
     double m_cylinderVolume;
     double m_hotWaterStoreLossFactor;
     double m_volumeFactor;
     double m_solarStorageVal;
     double m_primaryCircuitLoss;
     double m_combiLoss;
     double m_solarDhw;
     double m_waterHeaterOutput;
     double m_hotWaterHeatGains; 
     double m_insultaionThickness;
     int m_declaredLossKnown;
     double m_looseJacket;
     double m_combiStore;
     int m_storageType;
     int m_systemType;
    //Methods
    //

     void calcHotWaterGains();
     double tempFactorLossKnown();
     double tempFactorLossNotKnown();
     void primaryCircuitLoss();

    };

#endif // WATERHEATING_H
