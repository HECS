/***************************************************************************
 *                                                                         *
 *  HeatLosses.cpp Copyright (C) 2008 by Jon Rumble                        *
 *  j.w.rumble@reading.ac.uk   				                   *
 *                                                                         *
 *  This file is part of HECS,                                             *
 *                                                                         *
 *  HECS is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation, either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *  HECS is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/

#include "HeatLosses.h"
// Constructors/Destructors
//

HeatLosses::HeatLosses(const ConfigParser& config,
                       DwellingDimensions *dims, 
                       Ventilation *ventilation)
{
    ptrDims = dims;
    ptrVentilation = ventilation;
    cf = config;
    initVars();
}

HeatLosses::~HeatLosses ( )
{ 
    //TODO 
}

//
// Methods
//

// Calculate U-Value For Windows
//
double HeatLosses::calcUValueWindow (double uW )
{
    double uWeffec = 1 / ((1/uW)+ 0.04);
    return uWeffec;
}

void HeatLosses::buildLT()
{
    int regionSel = ptrDims->get_region();

    TableReader tr ("uValsEnglandWales");

    switch (regionSel)
    {

        //England & Wales U-Values
        case 1:	
        
            uValuesMap = tr.getTableMap();
            break;

        //Scotland U-Values
        case 2:	
            tr.readFile("uValsScotland");
            uValuesMap = tr.getTableMap();
            break;

        //Northern Ireland U-Values
        case 3:	
            tr.readFile("uValsNorthernIreland");
            uValuesMap = tr.getTableMap();
            break;

        //England & Wales by default....
        default:	
            tr.readFile("uValsEnglandWales");
            uValuesMap = tr.getTableMap();
            break;
    }				/* -----  end switch  ----- */
}

// Accessor methods
//
  void HeatLosses::setTotalAreaElems ( double val_in )   {
      m_totalAreaElems = val_in;
  }


  double HeatLosses::getTotalAreaElems ( )   {
    return m_totalAreaElems;
  }


  void HeatLosses::setFabricHeatLoss ( double val_in )   {
      m_fabricHeatLoss = val_in;
  }


  double HeatLosses::getFabricHeatLoss ( )   {
    return m_fabricHeatLoss;
  }


  void HeatLosses::setThermalBridges ( double val_in )   {
      m_thermalBridges = val_in;
  }


  double HeatLosses::getThermalBridges ( )   {
    return m_thermalBridges;
  }


  void HeatLosses::setTotalFabricHeatLoss ( double val_in )   {
      m_totalFabricHeatLoss = val_in;
  }


  double HeatLosses::getTotalFabricHeatLoss ( ) const   {
    return m_totalFabricHeatLoss;
  }


  void HeatLosses::setVentilationHeatLoss ( double val_in )   {
      m_ventilationHeatLoss = val_in;
  }


  double HeatLosses::getVentilationHeatLoss ( )   {
    return m_ventilationHeatLoss;
  }

  
  void HeatLosses::setHeatLossCoeff ( double val_in )   {
      m_heatLossCoeff = val_in;
  }


  double HeatLosses::getHeatLossCoeff ( )   {
    return m_heatLossCoeff;
  }


  void HeatLosses::setHeatLossParam ( double val_in )   {
      m_heatLossParam = val_in;
  }


  double HeatLosses::getHeatLossParam ( )   {
    return m_heatLossParam;
  }

  void HeatLosses::createHeatLossElement(const QString& elemNameIn)
  {
      if(!elementMap.contains(elemNameIn))
        {
            HeatLossElement heatLoss;
            elementMap.insert(elemNameIn,heatLoss);
        }
  }

  void HeatLosses::addHeatLoss(const QString& elemName,
                   const QString& lossName, 
                   double area, 
                   double uValue)
 {
    
      QMapIterator <QString,HeatLossElement> i (elementMap);
      
      while (i.hasNext())
      {
          i.next();
          if (i.key() == elemName)
              break;
      }

      HeatLossElement temp =i.value();
      
      temp.insertLoss(lossName,area,uValue);
      elementMap.insert(elemName,temp);
  }

  void HeatLosses::calcTotalAreaElems()
  {
       QMapIterator <QString,HeatLossElement> i(elementMap);

       m_totalAreaElems = 0.00, m_fabricHeatLoss = 0.00;
       while (i.hasNext())
       {
           i.next();
           HeatLossElement temp = i.value();
           temp.calcElementTotal();
           m_totalAreaElems += temp.getElementAreaTotal();
           m_fabricHeatLoss += temp.getElementLossTotal();
   
        }

  }
  void HeatLosses::calcHeatLossParam ()
  {


      m_thermalBridges = 0.15 * m_totalAreaElems;
      m_totalFabricHeatLoss =  m_fabricHeatLoss + m_thermalBridges;
      m_ventilationHeatLoss =  ptrVentilation->get_effecAirChangeRate() *
                                                                   0.33 *
                                          ptrDims->get_dwellingVolume();

      m_heatLossCoeff = m_totalFabricHeatLoss + m_ventilationHeatLoss;
      
      m_heatLossParam = m_heatLossCoeff / 
                        ptrDims->get_totalFloorArea();
  }


void HeatLosses::calcAll()
{
    //Calculate Element Area + Fabric Heatloss W/K totals
    calcTotalAreaElems();
    calcHeatLossParam();
}

void HeatLosses::initVars()
{

    fetchHeatLosses();
    buildLT();

    m_totalAreaElems = cf.getValueDouble("HeatLosses:totalAreaElems");
    m_fabricHeatLoss = cf.getValueDouble("HeatLosses:fabricHeatLoss");
    m_thermalBridges = cf.getValueDouble("HeatLosses:thermalBridges");
    m_totalFabricHeatLoss = cf.getValueDouble("HeatLosses:totalFabricHeatLoss");
    m_ventilationHeatLoss = cf.getValueDouble("HeatLosses:ventilationHeatLoss");
    m_heatLossCoeff = cf.getValueDouble("HeatLosses:heatLossCoeff");
    m_heatLossParam = cf.getValueDouble("HeatLosses:heatLossParam");

}


void HeatLosses::fetchHeatLosses()
{

    QStringList names, allElements, subList;
    QString elemName, lossName, currItem;
    //entries = cf.findHleEntries();
    double areaOut = 0.00, uValueOut = 0.00; 
    allElements = cf.getElementKeyNames();
    names = cf.findHleEntries();
    //QMap <QString, QString> hleMap = cf.getInputData();

    //Search For elements
    for (int i=0; i < names.size(); ++i)
    {
        subList = allElements.filter(names.at(i));
        elemName = names.at(i);
        
        //Create Our Element
        createHeatLossElement(elemName);
        
        //Search For individual Losees - IMPORTANT Relies on ini file order 
        //wont work otherwise !!
        for (int j=0; j < subList.size(); ++j)
        {
            currItem = subList.at(j);

            if (currItem.contains("name"))
                lossName = cf.getValueString(currItem); 
            if (currItem.contains("area"))
                areaOut = cf.getValueDouble(currItem);
            if (currItem.contains("uValue"))
            {
                uValueOut = cf.getValueDouble(currItem);
                addHeatLoss(elemName,lossName,areaOut,uValueOut);
            }

        }

        //Add Found Losses


                //addHeatLoss(elemName,lossName,areaOut,uValueOut);
    }


}
