/***************************************************************************
 *                                                                         *
 *  Gains.h Copyright (C) 2008 by Jon Rumble                               *
 *  j.w.rumble@reading.ac.uk   				                   *
 *                                                                         *
 *  This file is part of HECS,                                             *
 *                                                                         *
 *  HECS is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation, either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *  HECS is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/


#ifndef GAINS_H
#define GAINS_H

#include <QString>
#include <QMap>
#include <QStringList>

#include "SolarGains.h"
#include "Misc.h"
#include "ConfigParser.h"
#include "HeatLosses.h"
#include "WaterHeating.h"
#include "TableReader.h"
#include "DoubleReader.h"

class Gains
  {

  public:

    // Constructors/Destructors
    //

    Gains(const ConfigParser& config, WaterHeating* waterheat, 
                HeatLosses* heatingLosses,DwellingDimensions *dims);
   ~Gains();


   /**
    * ACSESSORS
    */ 
    
  void setLightAppCookMetabolicGains ( double val_in );
  double getLightAppCookMetabolicGains ( );
  void setLowEnergyLightRed ( double val_in );
  double getLowEnergyLightRed ( );
  void setAddtnGains ( double val_in );
  double getAddtnGains ( );
  void setWaterHeating ( double val_in );
  double getWaterHeating ( );
  void setTotalIntGains ( double val_in );
  double getTotalIntGains ( );
  void setTotalGains ( double val_in );
  double getTotalGains ( );
  void setSolarGLR ( double val_in );
  double getSolarGLR ( );
  void setSolarUtilFactor ( double val_in );
  double getSolarUtilFactor ( );
  void setTotalSolarGains ( double val_in );
  double getTotalSolarGains ( );
  void setMeanTempLivingArea ( double val_in );
  double getMeanTempLivingArea ( );
  void setTempAdjustment ( double val_in );
  double getTempAdjustment ( );
  void setAdjustForGains ( double val_in );
  double getAdjustForGains ( );
  void setAdjustLivingTemp ( double val_in );
  double getAdjustLivingTemp ( );
  void setTempDiffZones ( double val_in );
  double getTempDiffZones ( );
  void setLivingAreaFrac ( double val_in );
  double getLivingAreaFrac ( );
  void setRestOfHouseFrac ( double val_in );
  double getRestOfHouseFrac ( );
  void setMeanIntTemp ( double val_in );
  double getMeanIntTemp ( );
  void setTempRiseFromGains ( double val_in );
  double getTempRiseFromGains ( );
  void setBaseTemp ( double val_in );
  double getBaseTemp ( );
  void setDegreeDays ( double val_in );
  double getDegreeDays ( );
  void setSpaceHeatingReq ( double val_in );
  double getSpaceHeatingReq ( );
  double getGainLossRatio ();
  void setGainLossRatio (double val_in);
  double getUtilisationFactor ();
  void setUtilisationFactor (double val_in);
  double getUsefulGains ();
  void setUsefulGains(double val_in);
  double getAnnualLightEnergy();
  void setAnnualLightEnergy(double val_in);
  /**
   * METHODS
   */

  void calcAll();
  void createSolarGain(QString gainName,
                          int overshadow,
                          int orientation,
                          double area,
                          int frameType,
                          int glazingType,
                          char season);


  private:

    // Private Methods 
    //
    void calcUsefulSolarGains();
    void calcLightingEnergy();
    void calcTotalInternalGains();
    void calcMeanInternalTemp();
    void loadSolarGain(const QString& gainName);
    void initVars();
    void buildLT();
    void calcSpaceHeatReq();
    void calcMeanIntTempLiving();
    void calctempDiffBetweenZones();
    void calcDegreeDays();
    void calcGains();
    ConfigParser cf;
    WaterHeating *ptrWaterHeat;
    HeatLosses *ptrHeatLosses;
    DwellingDimensions *ptrDims;

    // Private Attributes
    //
    double m_lightAppCookMetabolicGains;
    double m_lowEnergyLightRed;
    double m_addtnGains;
    double m_waterHeating;
    double m_totalIntGains;
    double m_totalGains;
    double m_solarGLR;
    double m_solarUtilFactor;
    double m_totalSolarGains;
    double m_meanTempLivingArea;
    double m_tempAdjustment;
    double m_adjustForGains;
    double m_adjustLivingTemp;
    double m_tempDiffZones;
    double m_livingAreaFrac;
    double m_restOfHouseFrac;
    double m_meanIntTemp;
    double m_tempRiseFromGains;
    double m_baseTemp;
    double m_degreeDays;
    double m_spaceHeatingReq;
    double m_usefulGains;  
    double m_gainLossRatio;
    double m_utilisationFactor;
    double m_lightRatio;
    double m_annualLightEnergy;
    int m_addtnGainsTableVal;
    int m_specificFanPower;
    int m_heatingTypeControl;
    int m_controlType;
    //
    //Lookup Tables
    //
    QMap <QString,SolarGains> gainsList;
    QMap <double, QList <double> > table10;
    QMap <double, QList <double> > table9;
    QMap <double,QList <double> > table8;
  };

#endif // GAINS_H
