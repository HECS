/***************************************************************************
 *                                                                         *
 *  HeatLosses.h Copyright (C) 2008 by Jon Rumble                          *
 *  j.w.rumble@reading.ac.uk   				                   *
 *                                                                         *
 *  This file is part of HECS,                                             *
 *                                                                         *
 *  HECS is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation, either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *  HECS is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/


#ifndef HEATLOSSES_H
#define HEATLOSSES_H

#include "HeatLossElement.h"
#include "DwellingDimensions.h"
#include "Ventilation.h"
#include "ConfigParser.h"
#include <QString>
#include <QStringList>
#include <QMap>
#include <QList>
#include <QMapIterator>
#include "TableReader.h"

class HeatLosses
  {

  public:

    // Constructors/Destructors
    //


    /**
     * CONSTRUCTORS
     */
    HeatLosses(const ConfigParser& config,
               DwellingDimensions *dims, 
               Ventilation *ventilation);

    ~HeatLosses();


    /**
     * METHODS
     */

    
 void setTotalAreaElems ( double val_in );
  double getTotalAreaElems ( );
  void setFabricHeatLoss ( double val_in );
  double getFabricHeatLoss ( );
  void setThermalBridges ( double val_in );
  double getThermalBridges ( );
  void setTotalFabricHeatLoss ( double val_in );
  double getTotalFabricHeatLoss ( ) const ;
  void setVentilationHeatLoss ( double val_in );
  double getVentilationHeatLoss ( );
  void setHeatLossCoeff ( double val_in );
  double getHeatLossCoeff ( );
  void setHeatLossParam ( double val_in );
  double getHeatLossParam ( );
  void addHeatLoss(const QString& name, double area, double uValue);
  void createHeatLossElement(const QString& elemName);

  void addHeatLoss(const QString& elemName,
                   const QString& lossName, 
                   double area, 
                   double uValue);

  void calcAll();

  private:

    // Private attributes
    //

    double m_totalAreaElems;
    double m_fabricHeatLoss;
    double m_thermalBridges;
    double m_totalFabricHeatLoss;
    double m_ventilationHeatLoss;
    double m_heatLossCoeff;
    double m_heatLossParam;

    QMap <QString,HeatLossElement> elementMap;
    QMap <int, QList <double> > uValuesMap;
    DwellingDimensions *ptrDims;
    Ventilation *ptrVentilation;
    ConfigParser cf; 

    double calcUValueWindow (double uW);
    void createHeatLossElement (const QString elemName,
                                double area,
                                double uValue);
    void initVars();
    void buildLT();
    void calcHeatLossParam();
    void calcTotalAreaElems();
    void fetchHeatLosses();
  };

#endif // HEATLOSSES_H 
