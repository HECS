/***************************************************************************
 *                                                                         * 
 *  HECSUI.h Copyright (C) 2008 by Jon Rumble                           *  
 *  j.w.rumble@reading.ac.uk   				                   *
 *                                                                         *
 *                                                                         * 
 *  This file is part of HECS,                                             *
 *                                                                         *
 *  HECS is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation, either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *                                                                         *
 *  HECS is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/

#ifndef HECSUI_H
#define HECSUI_H 

#include <QtGui>
#include "RdSap.h"
#include "DimensionsDialog.h"
#include "StartScreen.h"
#include "DwellingInformationDialog.h"
#include "HeatingDialog.h"
#include "SummaryDialog.h"

class HecsUI : public QObject {

Q_OBJECT

public:
HecsUI (RdSap *modelIn);
~HecsUI ();

void guiLoop();

protected:

private:
RdSap *sapModel;
void initVars();
StartScreen *startScreen;
DwellingInformationDialog dwellinfo;
HeatingDialog heatDiag;
SummaryDialog summaryDiag; 
};


#endif //END OF HECSUI_H
