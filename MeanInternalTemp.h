/***************************************************************************
 *                                                                         * 
 *  MeanInternalTemp.h.h Copyright (C) 2008 by Jon Rumble                  *
 *  j.w.rumble@reading.ac.uk   				                   *
 *                                                                         *
 *  This file is part of HECS,                                             *  
 *                                                                         *
 *  HECS is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation, either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *  HECS is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/


#ifndef MEANINTERNALTEMP_H
#define MEANINTERNALTEMP_H

class MeanInternalTemp
{
public:

  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  MeanInternalTemp ( );

  /**
   * Empty Destructor
   */
   ~MeanInternalTemp ( );

  // Private attribute accessor methods
  //  

  void setIntTempLivingArea ( double new_var );
  double getIntTempLivingArea ( );
  void setTempAdj ( double new_var );
  double getTempAdj ( );
  void setAdjForGains ( double new_var );
  double getAdjForGains ( );
  void setAdjLivingRoomTemp ( double new_var );
  double getAdjLivingRoomTemp ( );
  void setTempDiffZone ( double new_var );
  double getTempDiffZone ( );
  void setLivingAreaFraction ( double new_var );
  double getLivingAreaFraction ( );
  void setRestOfHouseFrac ( double new_var );
  double getRestOfHouseFrac ( );
  void setMeanIntTemp ( double new_var );
  double getMeanIntTemp ( );

  void calcMeanInternalTemp ();

private:

  // Private attributes
  //  

  double m_intTempLivingArea;
  double m_tempAdj;
  double m_adjForGains;
  double m_adjLivingRoomTemp;
  double m_tempDiffZone;
  double m_livingAreaFraction;
  double m_restOfHouseFrac;
  double m_meanIntTemp;

  void initAttributes ( ) ;

};

#endif // MEANINTERNALTEMP_H
