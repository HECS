/***************************************************************************
 *                                                                         * 
 *  ConfigParser.h Copyright (C) 2008 by Jon Rumble                        *              
 *  j.w.rumble@reading.ac.uk   				                   *
 *                                                                         *
 *                                                                         * 
 *  This file is part of HECS,                                             *
 *                                                                         *
 *  HECS is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation, either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *                                                                         *
 *  HECS is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/

#ifndef CONFIGPARSER_H
#define CONFIGPARSER_H 

#include <QMap>
#include <QString>
#include <QFile>
#include <QTextStream>
#include <iostream>
#include <QMapIterator>
#include <QStringList>
class ConfigParser {

public:
ConfigParser ();
ConfigParser (const QString& fileName);
~ConfigParser ();



double getValueDouble (const QString& key);
QString getValueString (const QString& key);
bool getValueBool (const QString& key);
int getValueInt (const QString& key);
bool getFileExists ();
QMap<QString, QString> inputData;
QStringList findHleEntries();
QStringList elementKeyNames;
QStringList getElementKeyNames() const;
QMap <QString, QString> getInputData() const ; 
protected:

private:
bool fileExists;

QString fileStr;
QString m_currentElemName;

//void parse (QString lineStr);
void openFile ();
QString parseSection (const QString& lineStrIn);
void parseHeatLossElem(const QString& lineStr);
void parseEntry (const QString& secName, const QString& token);
void parseHleEntry (const QString& secName, const QString& token);
};


#endif
