/***************************************************************************
 *                                                                         *
 *  Main.h Copyright (C) 2008 by Jon Rumble                                *
 *  j.w.rumble@reading.ac.uk   				                   *
 *                                                                         *
 *  This file is part of HECS,                                             *
 *                                                                         *
 *  HECS is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation, either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *  HECS is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/


#ifndef HECS_H 
#define HECS_H

#include <QApplication>
#include <QLabel>
#include <iostream>
#include <QFile>
#include <QDir>
#include <QString>
#include <QStringList>

#include "RdSap.h"
#include "HecsUI.h"
#include "DwellingDimensions.h"
//
//SAP OBJECT
//
//

bool checkFiles();
void printUsage();

RdSap *hecsModel;
HecsUI *hecsGui;
#endif // MAIN_H
