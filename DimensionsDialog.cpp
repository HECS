/***************************************************************************
 *                                                                         * 
 *  DimensionsDialog.cpp Copyright (C) 2008 by Jon Rumble                        *  
 *  j.w.rumble@reading.ac.uk   				                   *
 *                                                                         *
 *                                                                         * 
 *  This file is part of HECS,                                             *
 *                                                                         *
 *  HECS is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation, either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *                                                                         *
 *  HECS is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/

#include "DimensionsDialog.h"

DimensionsDialog::DimensionsDialog (QWidget *parent)
    : QDialog(parent)
{
    ui.setupUi(this);
    m_firstFloorArea = 0.00;
    m_secondFloorArea = 0.00;
    m_thirdFloorArea = 0.00;
    m_firstFloorHeight = 0.00;
    m_secondFloorHeight = 0.00;
    m_thirdFloorHeight = 0.00;
}

void DimensionsDialog::setModel(RdSap *modelIn)
{
    sapModel = modelIn;
    initVars();
}

void DimensionsDialog::calcSqMetre()
{
    double result = 0.00;
    QString numOut = ui.sqFeetInput->text();
    result = sqFeettoSquareMetres(numOut.toDouble());
    numOut.number(result);
    ui.sqMetreLbl->setText(QString::number(result,'f',2));
}


void DimensionsDialog::onFirstFloorAreaChanged()
{

        m_firstFloorArea = ui.firstFloorAreaEdit->text().toDouble(); 

        double valOut = m_firstFloorArea +
                        m_secondFloorArea +
                        m_thirdFloorArea;

        ui.totalFloorAreaLbl->setText(QString::number(valOut,'f',2));

        //Update Model through pointer
        sapModel->dwelling->set_firstFloorArea(m_firstFloorArea);
}

void DimensionsDialog::onSecondFloorAreaChanged()
{

        m_secondFloorArea = ui.secondFloorAreaEdit->text().toDouble(); 

        double valOut = m_firstFloorArea +
                        m_secondFloorArea +
                        m_thirdFloorArea;

        ui.totalFloorAreaLbl->setText(QString::number(valOut,'f',2));
    

        //Update Model through pointer
        sapModel->dwelling->set_secondFloorArea(m_secondFloorArea);
}
void DimensionsDialog::onThirdFloorAreaChanged()

{

        m_thirdFloorArea = ui.thirdFloorAreaEdit->text().toDouble(); 

        double valOut = m_firstFloorArea +
                        m_secondFloorArea +
                        m_thirdFloorArea;

        ui.totalFloorAreaLbl->setText(QString::number(valOut,'f',2));

        //Update Model through pointer
        sapModel->dwelling->set_thirdFloorArea(m_thirdFloorArea);
}

void DimensionsDialog::onNumChimneysCbxChanged()
{
    int valOut = ui.numChimneysCbx->currentIndex();
    sapModel->ventilation->set_numChimneys(valOut);
}

void DimensionsDialog::onNumWindowsCbxChanged()
{
    sapModel->windowArea();

    if (ui.numWindowsCbx->currentIndex() == 0)
    {
        double tmp = sapModel->getRdWindowArea();
        sapModel->setRdWindowArea(tmp*0.75);
    }
    
    if (ui.numWindowsCbx->currentIndex() == 2)
    {
        double tmp = sapModel->getRdWindowArea();
        sapModel->setRdWindowArea(tmp*1.25);
    }



}

void DimensionsDialog::onFirstFloorHeightChanged()
{

        m_firstFloorHeight = ui.firstFloorHeightEdit->text().toDouble(); 

        double valOut = (m_firstFloorArea*m_firstFloorHeight)+
                       (m_secondFloorArea*m_secondFloorHeight)+
                       (m_thirdFloorArea*m_thirdFloorHeight);


        ui.totalVolumeLbl->setText(QString::number(valOut,'f',2));

        //Update Model through pointer
        sapModel->dwelling->set_firstFloorHeight(m_firstFloorHeight);
}

void DimensionsDialog::onSecondFloorHeightChanged()
{

        m_secondFloorHeight = ui.secondFloorHeightEdit->text().toDouble(); 

        double valOut = (m_firstFloorArea*m_firstFloorHeight)+
                       (m_secondFloorArea*m_secondFloorHeight)+
                       (m_thirdFloorArea*m_thirdFloorHeight);


        ui.totalVolumeLbl->setText(QString::number(valOut,'f',2));

        //Update Model through pointer
        sapModel->dwelling->set_secondFloorHeight(m_secondFloorHeight);
}

void DimensionsDialog::onThirdFloorHeightChanged()
{

        m_thirdFloorHeight = ui.thirdFloorHeightEdit->text().toDouble(); 

        double valOut = (m_firstFloorArea*m_firstFloorHeight)+
                       (m_secondFloorArea*m_secondFloorHeight)+
                       (m_thirdFloorArea*m_thirdFloorHeight);


        ui.totalVolumeLbl->setText(QString::number(valOut,'f',2));

        //Update Model through pointer
        sapModel->dwelling->set_secondFloorHeight(m_secondFloorHeight);
}

void DimensionsDialog::onNumGasFiresCbxChanged()
{
    int valOut = ui.numGasFiresCbx->currentIndex();
    sapModel->ventilation->set_numGasFluelessFires(valOut);
}

void DimensionsDialog::initVars()
{
    QObject::connect(ui.firstFloorAreaEdit, SIGNAL(textChanged(QString)), this,     SLOT(onFirstFloorAreaChanged()));

     QObject::connect(ui.firstFloorHeightEdit, SIGNAL(textChanged(QString)), this,     SLOT(onFirstFloorHeightChanged()));

     QObject::connect(ui.secondFloorHeightEdit, SIGNAL(textChanged(QString)), this,     SLOT(onSecondFloorHeightChanged()));

     QObject::connect(ui.thirdFloorHeightEdit, SIGNAL(textChanged(QString)), this,     SLOT(onThirdFloorHeightChanged()));


    QObject::connect(ui.secondFloorAreaEdit, SIGNAL(textChanged(QString)), this,     SLOT(onSecondFloorAreaChanged()));

    QObject::connect(ui.thirdFloorAreaEdit, SIGNAL(textChanged(QString)), this,     SLOT(onThirdFloorAreaChanged()));
    
    QObject::connect(ui.numChimneysCbx,SIGNAL(currentIndexChanged(QString)),this    ,SLOT(onNumChimneysCbxChanged()));

    QObject::connect(ui.numGasFiresCbx,SIGNAL(currentIndexChanged(QString)),this    ,SLOT(onNumGasFiresCbxChanged()));

     QObject::connect(ui.numWindowsCbx,SIGNAL(currentIndexChanged(QString)),this    ,SLOT(onNumWindowsCbxChanged()));

   //Populate Adress Details
    ui.houseNumLbl->setText(sapModel->dwelling->get_houseNumber());
    ui.addressLbl->setText(sapModel->dwelling->get_address());
    ui.postcodeLbl->setText(sapModel->dwelling->get_postcode());

}

// END OF DimensionsDialog.cpp
