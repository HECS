/***************************************************************************
 *                                                                         * 
 *  FuelCosts.h Copyright (C) 2008 by Jon Rumble                           *                         *                                                                         *
 *  j.w.rumble@reading.ac.uk   				                   *
 *                                                                         *
 *                                                                         * 
 *  This file is part of HECS,                                             *
 *                                                                         *
 *  HECS is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation, either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *                                                                         *
 *  HECS is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/

#ifndef FUELCOSTS_H
#define FUELCOSTS_H 

#include <QMap>
#include <QString>
#include <QFile>
#include <QTextStream>
#include <iostream>
#include <QMapIterator>
#include <QList>
#include <cmath>
#include "ConfigParser.h"
#include "SpaceHeating.h"
#include "WaterHeatingEnergy.h"
#include "TableReader.h"
#include "Gains.h"

class FuelCosts {

public:
FuelCosts ();
FuelCosts (const ConfigParser& config,
                 DwellingDimensions *dims,
                 SpaceHeating *sheat,
                 WaterHeatingEnergy *wheat,
                 Gains *gains);

~FuelCosts ();

//Accessors
  void setSpaceHeatFuelPricePri ( double val_in );
  double getSpaceHeatFuelPricePri ( );
  void setSpaceHeatFuelCostPri ( double val_in );
  double getSpaceHeatFuelCostPri ( );
  void setSpaceHeatFuelPriceSec ( double val_in );
  double getSpaceHeatFuelPriceSec ( );
  void setSpaceHeatFuelCostSec ( double val_in );
  double getSpaceHeatFuelCostSec ( );
  void setWaterHeatOnPeakFrac ( double val_in );
  double getWaterHeatOnPeakFrac ( );
  void setWaterHeatOffPeakFrac ( double val_in );
  double getWaterHeatOffPeakFrac ( );
  void setWaterHeatOnPeakFuelPrice ( double val_in );
  double getWaterHeatOnPeakFuelPrice ( );
  void setWaterHeatOnPeakCost ( double val_in );
  double getWaterHeatOnPeakCost ( );
  void setWaterHeatOffPeakCost ( double val_in );
  double getWaterHeatOffPeakCost ( );
  void setWaterHeatOffPeakFuelPrice ( double val_in );
  double getWaterHeatOffPeakFuelPrice ( );
  void setWaterHeatingOtherFuelPrice ( double val_in );
  double getWaterHeatingOtherFuelPrice ( );
  void setWaterHeatingOtherFuelCost ( double val_in );
  double getWaterHeatingOtherFuelCost ( );
  void setPumpFanEnergyPrice ( double val_in );
  double getPumpFanEnergyPrice ( );
  void setPumpFanEnergyCost ( double val_in );
  double getPumpFanEnergyCost ( );
  //void setEnergyLightingReq ( double val_in );
  //double getEnergyLightingReq ( );
  void setEnergyLightingPrice ( double val_in );
  double getEnergyLightingPrice ( );
  void setEnergyLightingCost ( double val_in );
  double getEnergyLightingCost ( );
  void setAddtnStandingCharges ( double val_in );
  double getAddtnStandingCharges ( );
  void setREenergyProducedSavedYear ( double val_in );
  double getREenergyProducedSavedYear ( );
  void setREenergyProducedPrice ( double val_in );
  double getREenergyProducedPrice ( );
  void setREenergyProducedCost ( double val_in );
  double getREenergyProducedCost ( );
  void setREenergyConsumedTechReq ( double val_in );
  double getREenergyConsumedTechReq ( );
  void setREcostConsumedPrice ( double val_in );
  double getREcostConsumedPrice ( );
  void setREcostConsumedCost ( double val_in );
  double getREcostConsumedCost ( );
  void setTotalEnergyCost ( double val_in );
  double getTotalEnergyCost ( );
  double getSAPenergyCostDeflator ( );
  void setSAPenergyCostDeflator (double val_in );
  void setSAPEnergyCostFactor ( double val_in );
  double getSAPEnergyCostFactor ( );
  void setSAPrating ( double val_in );
  double getSAPrating ( );
  void setEIrating ( double val_in );
  double getEIrating ( );
  int getFuelType();
  void setFuelType(int val_in);
  void calcAll();
  QMap <int, QList <double> > getTable12();
protected:

private:

QMap <int, QList <double> > table12;
QMap <int, QList <double> > table13;

void calcTotalFuelCost ();

void initVars();
void buildLT();
void calcSapRating();
void calcEIRating();
double m_spaceHeatFuelPricePri;
double m_spaceHeatFuelCostPri;
double m_spaceHeatFuelPriceSec;
double m_spaceHeatFuelCostSec;
double m_waterHeatOnPeakFracCost;
double m_waterHeatOnPeakFuelPrice;
double m_waterHeatOnPeakCost;
double m_waterHeatOffPeakFracCost;
double m_waterHeatOffPeakCost;
double m_waterHeatOffPeakFuelPrice;
double m_waterHeatingOtherFuelPrice;
double m_waterHeatingOtherFuelCost;
double m_pumpFanEnergyPrice;
double m_pumpFanEnergyCost;
double m_EnergyLightingReq;
double m_EnergyLightingPrice;
double m_EnergyLightingCost;
double m_addtnStandingCharges;
double m_REenergyProducedSavedYear;
double m_REenergyProducedPrice;
double m_REenergyProducedCost;
double m_REenergyConsumedTechReq;
double m_REcostConsumedPrice;
double m_REcostConsumedCost;
double m_totalEnergyCost;
double m_SAPenergyCostDeflator;
double m_SAPEnergyCostFactor;
double m_SAPrating;
double m_EIrating;
int m_fuelType;

ConfigParser cf;
SpaceHeating *ptrSpaceHeating;
WaterHeatingEnergy *ptrWaterHeatingEnergy;
DwellingDimensions *ptrDims;
Gains *ptrGains;
};


#endif
