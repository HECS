/***************************************************************************
 *                                                                         * 
 *  HECSUI.cpp Copyright (C) 2008 by Jon Rumble                             *  
 *  j.w.rumble@reading.ac.uk   				                   *
 *                                                                         *
 *                                                                         * 
 *  This file is part of HECS,                                             *
 *                                                                         *
 *  HECS is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation, either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *                                                                         *
 *  HECS is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/


#include "HecsUI.h"


HecsUI::HecsUI (RdSap *modelIn)

{
    sapModel = modelIn;
    initVars();
}

HecsUI::~HecsUI () { }


void HecsUI::guiLoop()
{
    
    startScreen = new StartScreen();
    startScreen->show();
    qApp->exec();
    delete startScreen;

    DwellingInformationDialog dwellinfo;
    dwellinfo.setModel(sapModel);
    dwellinfo.show();
    qApp->exec();


    DimensionsDialog dims;
    dims.setModel(sapModel);
    dims.show();
    qApp->exec();

    HeatingDialog heatDiag;
    heatDiag.setModel(sapModel);
    heatDiag.show();
    qApp->exec();

    SummaryDialog summaryDiag; 
    summaryDiag.setModel(sapModel);
    summaryDiag.show();
    qApp->exec();


    
}

void HecsUI::initVars()
{
}

// END OF HECSUI.cpp
