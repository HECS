/***************************************************************************
 *                                                                         *
 *  Gains.cpp Copyright (C) 2008 by Jon Rumble                             *
 *  j.w.rumble@reading.ac.uk   				                   *
 *                                                                         *
 *  This file is part of HECS,                                             *
 *                                                                         *
 *  HECS is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation, either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *  HECS is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/

#include "Gains.h"
#include <iostream>
// Constructors/Destructors
//

Gains::Gains (const ConfigParser& config, WaterHeating *waterheat,
                    HeatLosses *heatingLosses,DwellingDimensions *dims)
{
  cf = config; 
  ptrWaterHeat = waterheat;
  ptrHeatLosses = heatingLosses;
  ptrDims = dims;
  initVars();
}

Gains::~Gains ( )
{ 
    //TODO 
}

//
// Methods
//


// Accessor methods
//

void Gains::setLightAppCookMetabolicGains ( double val_in ) {
  m_lightAppCookMetabolicGains = val_in;
}

double Gains::getLightAppCookMetabolicGains ( ) {
  return m_lightAppCookMetabolicGains;
}

void Gains::setLowEnergyLightRed ( double val_in ) {
  m_lowEnergyLightRed = val_in;
}

double Gains::getLowEnergyLightRed ( ) {
  return m_lowEnergyLightRed;
}

void Gains::setAddtnGains ( double val_in ) {
  m_addtnGains = val_in;
}

double Gains::getAddtnGains ( ) {
  return m_addtnGains;
}

void Gains::setWaterHeating ( double val_in ) {
  m_waterHeating = val_in;
}

double Gains::getWaterHeating ( ) {
  return m_waterHeating;
}

void Gains::setTotalIntGains ( double val_in ) {
  m_totalIntGains = val_in;
}

double Gains::getTotalIntGains ( ) {
  return m_totalIntGains;
}

void Gains::setTotalGains ( double val_in ) {
  m_totalGains = val_in;
}

double Gains::getTotalGains ( ) {
  return m_totalGains;
}

void Gains::setSolarGLR ( double val_in ) {
  m_solarGLR = val_in;
}

double Gains::getSolarGLR ( ) {
  return m_solarGLR;
}

void Gains::setSolarUtilFactor ( double val_in ) {
  m_solarUtilFactor = val_in;
}

double Gains::getSolarUtilFactor ( ) {
  return m_solarUtilFactor;
}

void Gains::setTotalSolarGains ( double val_in ) {
  m_totalSolarGains = val_in;
}

double Gains::getTotalSolarGains ( ) {
  return m_totalSolarGains;
}

void Gains::setMeanTempLivingArea ( double val_in ) {
  m_meanTempLivingArea = val_in;
}

double Gains::getMeanTempLivingArea ( ) {
  return m_meanTempLivingArea;
}

void Gains::setTempAdjustment ( double val_in ) {
  m_tempAdjustment = val_in;
}

double Gains::getTempAdjustment ( ) {
  return m_tempAdjustment;
}

void Gains::setAdjustForGains ( double val_in ) {
  m_adjustForGains = val_in;
}

double Gains::getAdjustForGains ( ) {
  return m_adjustForGains;
}

void Gains::setAdjustLivingTemp ( double val_in ) {
  m_adjustLivingTemp = val_in;
}

double Gains::getAdjustLivingTemp ( ) {
  return m_adjustLivingTemp;
}

void Gains::setTempDiffZones ( double val_in ) {
  m_tempDiffZones = val_in;
}

double Gains::getTempDiffZones ( ) {
  return m_tempDiffZones;
}

void Gains::setLivingAreaFrac ( double val_in ) {
  m_livingAreaFrac = val_in;
}

double Gains::getLivingAreaFrac ( ) {
  return m_livingAreaFrac;
}

void Gains::setRestOfHouseFrac ( double val_in ) {
  m_restOfHouseFrac = val_in;
}

double Gains::getRestOfHouseFrac ( ) {
  return m_restOfHouseFrac;
}

void Gains::setMeanIntTemp ( double val_in ) {
  m_meanIntTemp = val_in;
}

double Gains::getMeanIntTemp ( ) {
  return m_meanIntTemp;
}

void Gains::setTempRiseFromGains ( double val_in ) {
  m_tempRiseFromGains = val_in;
}

double Gains::getTempRiseFromGains ( ) {
  return m_tempRiseFromGains;
}

void Gains::setBaseTemp ( double val_in ) {
  m_baseTemp = val_in;
}

double Gains::getBaseTemp ( ) {
  return m_baseTemp;
}

void Gains::setDegreeDays ( double val_in ) {
  m_degreeDays = val_in;
}

double Gains::getDegreeDays ( ) {
  return m_degreeDays;
}

void Gains::setSpaceHeatingReq ( double val_in ) {
  m_spaceHeatingReq = val_in;
}

double Gains::getSpaceHeatingReq ( ) {
  return m_spaceHeatingReq;
}

double Gains::getGainLossRatio ( ) {
    return m_gainLossRatio;
}

void Gains::setGainLossRatio (double val_in ) {
    m_gainLossRatio = val_in;
}

double Gains::getUtilisationFactor ( ) {
    return m_utilisationFactor;
}

void Gains::setUtilisationFactor (double val_in ) {
    m_utilisationFactor = val_in;
}

double Gains::getUsefulGains ( ) {
    return m_usefulGains;
}

void Gains::setUsefulGains (double val_in ) {
    m_usefulGains = val_in;
}

double Gains::getAnnualLightEnergy(){
    return m_annualLightEnergy;
}

void Gains::setAnnualLightEnergy(double val_in){
    m_annualLightEnergy = val_in;
}

// Other methods
//

void Gains::calcLightingEnergy()
{
  //Fixed Low Energy Lightbulbs
  //
    
  //Ratio of Enegry saving to standard lights
  double C_1 = 0.00, C_2 = 0.00, GL = 0.00;
  C_1 = 1 - 0.50 * m_lightRatio;
  


  //Daylighting Calc
  GL = m_totalSolarGains / ptrDims->get_totalFloorArea();
  
  if (GL <= 0.095)
    C_2 = (52.2 * (GL * GL)) - 9.94 * GL + 1.433;
  else //GL > 0.095
      C_2 = 0.96;

  m_annualLightEnergy = 9.3 *
                      ptrDims->get_totalFloorArea() *
                      C_1 *
                      C_2;

  
  //Gains From Lighting 
  m_lowEnergyLightRed = m_annualLightEnergy * 0.15;
}

void Gains::calcMeanIntTempLiving ()
{
    double HLP = ptrHeatLosses->getHeatLossParam();
    double x1 = 0.00;
    double x3 = 0.00;
    double y1 = 0.00;
    double y3 = 0.00;
    QList <double> tmpList;
        
        QMap<double,QList <double> >::const_iterator i;

        if (HLP <= 1.00)
        {
            m_meanTempLivingArea= table8.value(1.00).at(m_heatingTypeControl);
        }
        else if (HLP >= 6.00)
        {
                
            m_meanTempLivingArea = table8.value(6.00).at(m_heatingTypeControl);
        }

        //Find Upper Bound
        else
        {
            i = table8.upperBound(HLP);
            x3 = i.key();
            tmpList = i.value();
            y3 = tmpList.at(m_heatingTypeControl);
            //Decrement by 1 for lower bound
            --i;
            x1 = i.key(); 
            tmpList = i.value();
            y1 = tmpList.at(m_heatingTypeControl);

            m_meanTempLivingArea = lerp(x1,HLP,x3,y1,y3);
        }

}


void Gains::calctempDiffBetweenZones()
{
    double HLP = ptrHeatLosses->getHeatLossParam();
    double x1 = 0.00;
    double x3 = 0.00;
    double y1 = 0.00;
    double y3 = 0.00;
    QList <double> tmpList;
        
        QMap<double,QList <double> >::const_iterator i;

        if (HLP <= 1.00)
        {
            m_tempDiffZones = table9.value(1.00).at(m_controlType);
        }
        else if (HLP >= 6.00)
        {
                
            m_tempDiffZones = table9.value(6.00).at(m_controlType);
        }

        //Find Upper Bound
        else
        {
            i = table9.upperBound(HLP);
            x3 = i.key();
            tmpList = i.value();
            y3 = tmpList.at(m_controlType);
            //Decrement by 1 for lower bound
            --i;
            x1 = i.key(); 
            tmpList = i.value();
            y1 = tmpList.at(m_controlType);

            m_tempDiffZones = lerp(x1,HLP,x3,y1,y3);
        }

}
void Gains::calcDegreeDays()
{
    double x1 = 0.00;
    double x3 = 0.00;
    double y1 = 0.00;
    double y3 = 0.00;
    QList <double> tmpList;
    m_tempRiseFromGains = m_usefulGains /
                          ptrHeatLosses->getHeatLossCoeff();
    
    m_baseTemp = m_meanIntTemp - m_tempRiseFromGains;

    if (m_baseTemp < 1.00)
        m_degreeDays = 0.00;
    else if(m_baseTemp > 20.50)
        m_degreeDays = 3330.00;

    else 
    {
          
        QMap<double,QList <double> >::const_iterator i;

    
        i = table10.upperBound(m_baseTemp);
        x3 = i.key();
        tmpList = i.value();
        y3 = tmpList.at(0);
        //Decrement by 1 for lower bound
        --i;
        x1 = i.key(); 
        tmpList = i.value();
        y1 = tmpList.at(0);

        m_degreeDays = lerp(x1,m_baseTemp,x3,y1,y3);
    }
}

void Gains::calcTotalInternalGains()
{

    double N = 0.00, W = 0.00, SFP=0.00;
    double TFA = ptrDims->get_totalFloorArea();

    //Calculate N
    //
    if (TFA <= 420.00)
        N = 0.035 * TFA - 0.000038 * (TFA * TFA);
    else
        N = 8.00;

    //Calculate Gains W
    //
    if (TFA <= 282.00)
        W = 74.00 + 2.66 * TFA + 75.5 * N;
    else
        W = 824.00 + 75.5 * N;

    m_lightAppCookMetabolicGains = W;

    if(m_specificFanPower == 1)
        SFP = 2.0;
    else
        SFP = 0.8;

    
    switch (m_addtnGainsTableVal)
    {
        case 1:	//Central Heating Pump
            m_addtnGains = 10.00;
            break;
        case 2: //Oil Boiler Pump, inside dwelling	
            m_addtnGains = 10.00;
            break;

        case 3://Warm air heating system fans
            m_addtnGains = 0.06 * ptrDims->get_dwellingVolume();
            break;
        case 4:
            m_addtnGains = SFP * 0.06 * ptrDims->get_dwellingVolume();
            break;
        default:	
            break;
    }				/* -----  end switch  ----- */


    m_waterHeating = ptrWaterHeat->getHotWaterHeatGains() / 8.76 ;
    m_totalIntGains = m_lightAppCookMetabolicGains +
                      m_addtnGains +
                      m_waterHeating -
                      m_lowEnergyLightRed;
}

void Gains::calcUsefulSolarGains()
{
   QMapIterator <QString, SolarGains> i(gainsList);

   while (i.hasNext())
   {
       i.next();
       SolarGains temp = i.value();
       m_totalSolarGains +=  temp.getSolarGains();
        
   }

   m_totalGains = m_totalIntGains + m_totalSolarGains;

   m_gainLossRatio = m_totalGains / ptrHeatLosses->getHeatLossCoeff();

   //Calculate Utilisation Factor
   m_utilisationFactor = 1 - exp(-18/m_gainLossRatio);

   m_usefulGains = m_totalSolarGains * m_utilisationFactor;
   
}

void Gains::calcSpaceHeatReq()
{
    m_spaceHeatingReq = 0.024 * 
                        m_degreeDays * 
                        ptrHeatLosses->getHeatLossCoeff();
}

void Gains::calcMeanInternalTemp()
{
    //Needs work to Fix !!!!
    //
    double R = 1.00;

    //Calc Mean Internal Living Room Temperature
    calcMeanIntTempLiving();

    //TODO - adjust from table 4e

    //TODO - responsiveness, defaulting to 1.00 for now !!

    m_adjustForGains = (m_usefulGains /   
                       ptrHeatLosses->getHeatLossCoeff() - 4.0) *
                                                          0.2 *
                                                          R;

    m_adjustLivingTemp =  m_meanTempLivingArea +
                          m_tempAdjustment     +
                          m_adjustForGains;
    
    calctempDiffBetweenZones();

    m_livingAreaFrac = ptrDims->get_livingRoomArea() / 
                       ptrDims->get_totalFloorArea();

    m_restOfHouseFrac = 1.00 - m_livingAreaFrac;
    m_meanIntTemp = m_adjustLivingTemp -
                    (m_tempDiffZones * m_restOfHouseFrac);

}

void Gains::initVars()
{

  buildLT(); 

  m_lightAppCookMetabolicGains = cf.getValueDouble("Gains:lightAppCookMetabolicGains");
  m_lowEnergyLightRed = cf.getValueDouble("Gains:lowEnergyLightRed");
  m_addtnGains = cf.getValueDouble("Gains:addtnGains");
  m_waterHeating = cf.getValueDouble("Gains:waterHeating");
  m_totalIntGains = cf.getValueDouble("Gains:totalIntGains");
  m_totalGains = cf.getValueDouble("Gains:totalGains");
  m_solarGLR = cf.getValueDouble("Gains:solarGLR");
  m_solarUtilFactor = cf.getValueDouble("Gains:solarUtilFactor");
  m_totalSolarGains = cf.getValueDouble("Gains:totalSolarGains");
  m_meanTempLivingArea = cf.getValueDouble("Gains:meanTempLivingArea");
  m_tempAdjustment = cf.getValueDouble("Gains:tempAdjustment");
  m_adjustForGains = cf.getValueDouble("Gains:adjustForGains");
  m_adjustLivingTemp = cf.getValueDouble("Gains:adjustLivingTemp");
  m_tempDiffZones = cf.getValueDouble("Gains:tempDiffZones");
  m_livingAreaFrac = cf.getValueDouble("Gains:livingAreaFrac");
  m_restOfHouseFrac = cf.getValueDouble("Gains:restOfHouseFrac");
  m_meanIntTemp = cf.getValueDouble("Gains:meantIntTemp");
  m_tempRiseFromGains = cf.getValueDouble("Gains:tempRiseFromGains");
  m_baseTemp = cf.getValueDouble("Gains:baseTemp");
  m_degreeDays = cf.getValueDouble("Gains:degreeDays");
  m_spaceHeatingReq = cf.getValueDouble("Gains:spaceHeatingReq");
  m_usefulGains  = cf.getValueDouble("Gains:usefulGains");
  m_gainLossRatio = cf.getValueDouble("Gains:gainLossRatio");
  m_utilisationFactor = cf.getValueDouble("Gains:utilisationFactor");
  m_lightRatio = cf.getValueDouble("Gains:lightRatio");
  m_addtnGainsTableVal = cf.getValueInt("Gains:addtnGainsTableVal");
  m_specificFanPower = cf.getValueInt("Gains:specificFanPower");
  m_heatingTypeControl = cf.getValueInt("Gains:heatingTypeControl");
  m_controlType = cf.getValueInt("Gains:controlType");
  m_annualLightEnergy= cf.getValueInt("Gains:annualLightEnergy");

  loadSolarGain("North");
  loadSolarGain("South");
  loadSolarGain("East");
  loadSolarGain("West");
  loadSolarGain("NorthEast");
  loadSolarGain("SouthEast");
  loadSolarGain("NorthWest");
  loadSolarGain("SouthWest");
  loadSolarGain("Rooflights");
}


void Gains::loadSolarGain(const QString& gainName)
{
    int overshadow = 0,orientation =0;
    int frameType, glazingType = 0;
    double area = 0.00; 
    
    QString searchStr = "SG" + gainName + ":";
    overshadow = cf.getValueInt(searchStr+"overshadow");
    orientation = cf.getValueInt(searchStr+"orientation");
    area = cf.getValueDouble(searchStr+"area");
    frameType = cf.getValueInt(searchStr+"frameType");
    glazingType = cf.getValueInt(searchStr+"glazingType");

    createSolarGain(gainName,
            overshadow,
            orientation,
            area,
            frameType,
            glazingType,'w');
}


void Gains::buildLT()
{
    //TEST BEFORE REMOVAL
    DoubleReader t1 ("table9");
    table9 = t1.getDoubleMap();
    DoubleReader t2 ("table8"); 
    table8 = t2.getDoubleMap(); 
    DoubleReader t3 ("table10"); 
    table10 = t3.getDoubleMap(); 

 /*   //dr.readFile("table8"); 
    DoubleReader table8r ("table8");
    table8 = table8r.getDoubleMap(); 

     DoubleReader table9r ("table9");
    table9 = table9r.getDoubleMap();

   
    //dr.readFile("table10"); 
    DoubleReader table10r ("table10");
    table10 = table10r.getDoubleMap();*/ 

}
void Gains::createSolarGain(QString gainName,
                            int overshadow,
                            int orientation,
                            double area,
                            int frameType,
                            int glazingType,
                            char season)
{
    SolarGains gain(overshadow,orientation,area,
                    frameType,glazingType,season);
    gainsList[gainName] = gain;
}

void Gains::calcAll()
{
    calcTotalInternalGains();
    calcUsefulSolarGains();
    calcLightingEnergy();
    calcMeanInternalTemp();
    calcDegreeDays();
    calcSpaceHeatReq();
}
