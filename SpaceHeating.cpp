/***************************************************************************
 *                                                                         * 
 *  SpaceHeating.cpp Copyright (C) 2008 by Jon Rumble                      *  
 *  j.w.rumble@reading.ac.uk   				                   *
 *                                                                         *
 *                                                                         * 
 *  This file is part of HECS,                                             *
 *                                                                         *
 *  HECS is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation, either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *                                                                         *
 *  HECS is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/


#include "SpaceHeating.h"


SpaceHeating::SpaceHeating (const ConfigParser& config, Gains *gains) 
{
    cf = config;
    ptrGains = gains;
    initVars();
    calcAll();
}

SpaceHeating::~SpaceHeating () { }

void SpaceHeating::calcAll()
{
    calcSpaceHeatingFuelMain();
    calcSpaceHeatingFuelSec();
}


void SpaceHeating::initVars()
{
  m_fracHeatFromSecondary = cf.getValueDouble("EnergyRequirements:fracHeatFromSecondary"); 
  m_effcyMainHeatingSys = cf.getValueInt("EnergyRequirements:effcyMainHeatingSys");
  m_effcySecondaryHeatingSystem = cf.getValueInt("EnergyRequirements:effcySecondaryHeatingSystem");
  m_spaceHeatingFuelMainReq = cf.getValueDouble("EnergyRequirements:spaceHeatingFuelMainReq");
  m_spaceHeatingFuelSecondary = cf.getValueDouble("EnergyRequirements:spaceHeatingFuelMainSecondary");

}

void SpaceHeating::calcSpaceHeatingFuelMain()
{
    m_spaceHeatingFuelMainReq = (1 - m_fracHeatFromSecondary) * 
                                ptrGains->getSpaceHeatingReq() *
                                                           100 /
                                 m_effcyMainHeatingSys;
}

void SpaceHeating::calcSpaceHeatingFuelSec()
{
    m_spaceHeatingFuelSecondary = m_fracHeatFromSecondary *
                                  ptrGains->getSpaceHeatingReq() *
                                                            100 /
                                  m_effcySecondaryHeatingSystem;
}

void SpaceHeating::setFracHeatFromSecondary ( double val_in ) {
  m_fracHeatFromSecondary = val_in;
}


double SpaceHeating::getFracHeatFromSecondary ( ) {
  return m_fracHeatFromSecondary;
}


void SpaceHeating::setEffcyMainHeatingSys ( int val_in ) {
  m_effcyMainHeatingSys = val_in;
}


int SpaceHeating::getEffcyMainHeatingSys ( ) {
  return m_effcyMainHeatingSys;
}


void SpaceHeating::setEffcySecondaryHeatingSystem ( int val_in ) {
  m_effcySecondaryHeatingSystem = val_in;
}


int SpaceHeating::getEffcySecondaryHeatingSystem ( ) {
  return m_effcySecondaryHeatingSystem;
}


void SpaceHeating::setSpaceHeatingFuelMainReq ( double val_in ) {
  m_spaceHeatingFuelMainReq = val_in;
}


double SpaceHeating::getSpaceHeatingFuelMainReq ( ) {
  return m_spaceHeatingFuelMainReq;
}


void SpaceHeating::setSpaceHeatingFuelSecondary ( double val_in ) {
  m_spaceHeatingFuelSecondary = val_in;
}


double SpaceHeating::getSpaceHeatingFuelSecondary ( ) {
  return m_spaceHeatingFuelSecondary;
}
// END OF SpaceHeating.cpp
