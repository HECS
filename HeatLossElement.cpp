/***************************************************************************
 *                                                                         *
 *  HeatLossElement.cpp Copyright (C) 2008 by Jon Rumble                   *
 *  j.w.rumble@reading.ac.uk   				                   *
 *                                                                         *
 *  This file is part of HECS,                                             *
 *                                                                         *
 *  HECS is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation, either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *  HECS is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/

#include "HeatLossElement.h"
// Constructors/Destructors
//

HeatLossElement::HeatLossElement() { }

HeatLossElement::~HeatLossElement () { }


void HeatLossElement::insertLoss(const QString& name,double area, double uValue)
{
    HeatLossItem item (area,uValue); 
    elemMap.insert(name,item);
}

void HeatLossElement::removeLoss(const QString& name)
{
   elemMap.remove(name); 
}

void HeatLossElement::updateLoss(const QString& name,double area,double uValue)
{
    removeLoss(name);
    insertLoss(name,area,uValue);

}

//
// Methods
//
   
void HeatLossElement::calcElementTotal()
{

  m_elementLossTotal = 0.00;
  m_elementAreaTotal = 0.00;
  QMapIterator <QString, HeatLossItem> i (elemMap);

  while (i.hasNext())
  {
      i.next();
      HeatLossItem temp = i.value();
      m_elementLossTotal += temp.getwK();
      m_elementAreaTotal += temp.getArea();
  }
}



// Accessor methods
//

double HeatLossElement::getElementLossTotal()
{
    calcElementTotal();
    return m_elementLossTotal;
}

void HeatLossElement::setElementLossTotal(double val_in)
{
    m_elementLossTotal = val_in;
}

double HeatLossElement::getElementAreaTotal()
{
    calcElementTotal();
    return m_elementAreaTotal;
}

void HeatLossElement::setAreaElementTotal(double val_in)
{
    m_elementLossTotal = val_in;
}
