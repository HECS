/***************************************************************************
 *                                                                         *
 *  Main.cpp Copyright (C) 2008 by Jon Rumble                              *
 *  j.w.rumble@reading.ac.uk   				                   *
 *                                                                         *
 *  This file is part of HECS,                                             *
 *                                                                         *
 *  HECS is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation, either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *  HECS is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/

#include "Hecs.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);


    if (argc != 2 )
    {
        printUsage();
        return 1;
    }
        

    QString filename (app.argv()[1]);
    //Sanity Check for file....
    if (checkFiles() != 0)
        return 1;

    hecsModel= new RdSap(filename);
    hecsModel->buildBoilerList();

    //hecsGui = new HecsUI  (hecsModel);
    //hecsGui->guiLoop();

//    return app.exec();
    delete hecsModel;
    delete hecsGui;
    return 0;
    //hecsGui.setModel(hecsModel);


    
    std::cout << "Bye !!" << std::endl;
}


bool checkFiles()
{

    /*QDir fp("data");

    QFile file (fp.filePath("table13"));
    if (!fp.exists())
    {
        qWarning("Can't Find Table13 file !!");
        return 1;
    }*/

/*    filePath = "data/table12";
    if (!filePath.exists())
    {
        qWarning("Can't Find Table12 file !!");
        qApp->exit(1);
    }

    filePath = "data/table10";
    if (!filePath.exists())
    {
        qWarning("Can't Find Table10 file !!");
        qApp->exit(1);
    }

    filePath = "data/table6e";
    if (!filePath.exists())
    {
        qWarning("Can't Find Table6e file !!");
        qApp->exit(1);
    }*/
    return 0;
} 

void printUsage()
{
    std::cout << "\nUsage:  HECS <filename>\n\n" << std::endl;
}

