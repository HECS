/***************************************************************************
 *                                                                         *
 *  Sedbuk.h Copyright (C) 2008 by Jon Rumble                              *
 *  j.w.rumble@reading.ac.uk   				                   *
 *                                                                         *
 *  This file is part of HECS,                                             *
 *                                                                         *
 *  HECS is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation, either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *  HECS is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/


#include "Sedbuk.h"

SedbukList::SedbukList(const QString fileNameIn) {
    fileName = fileNameIn;
    initVars();
    openFile(fileName);
}

SedbukList::~SedbukList() {
}

void SedbukList::openFile(QString fileName)
{
    QFile file(fileName);

    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        std::cerr << "Cannot open file for reading: "
        << qPrintable(file.errorString())
        << std::endl;
        m_sedbukAvail = 0;
        return;
    }

    QTextStream in (&file);
    while (!in.atEnd()) {
        QString out = in.readLine();
        if (out.startsWith("$001"))
            parseVersion(out);
        //Process Table 103
        else if (out.startsWith("$103"))
        {
           do{
                out = in.readLine();

                if (out.startsWith("#"))
                    continue;
                if (out.startsWith("$"))
                    continue;
                else
                    parseBoilerTable(out);

            }while (!out.startsWith("$121"));
                   }
        //Skip Comment Line
        else if (out.startsWith("#"))
            continue;
        //End Of File
        else if (out.startsWith("$999"))
            break;
        
        else
            continue;
    }

    
    m_sedbukAvail = 1;
    file.close();
    return;
}


bool SedbukList::get_sedbukAvail ()
{
    return m_sedbukAvail;
}

Boiler SedbukList::findBoiler (const QString& modelStr)
{
   QString Junk =  modelStr;
   Boiler currBoiler;
   if (!boilerHash.contains(modelStr))
       return currBoiler;

   currBoiler = boilerHash[modelStr];
   return currBoiler;


}


QStringList SedbukList::getBrandList(int fuel_type)
{

     QStringList  entries;
    

    switch (fuel_type){
    case 1:  
        //entries = buildBoilerList(gasBoilers);
        break;
    case 2:
        //entries = buildBoilerList(lpgBoilers);
        break;
    case 4:
        //entries = buildBoilerList(oilBoilers);
        break;
    }

    return entries;

   }


QStringList SedbukList::getModelList(QString brandNameIn)
{
   QStringList listIn;
   QHashIterator <QString, Boiler> i (boilerHash);

   Boiler entry;
   while (i.hasNext())
   {
       i.next();
       entry = i.value();
       if ((entry.get_brandName() == brandNameIn) && (entry.get_fuelType() == m_fuelType))
            listIn << entry.get_modelName() + entry.get_modelQualifier();
       else
           continue;
    }

   listIn.sort();
   return listIn;   
}

QString SedbukList::get_revisionDate()
{
    return m_revisionDate;
}

QStringList SedbukList::getBoilerBrands (int fuel_type)

{
   m_fuelType = fuel_type;
   QStringList listIn;
   QHashIterator <QString, Boiler> i (boilerHash);

   Boiler entry;
   while (i.hasNext())
   {
       i.next();
       entry = i.value();
       if (entry.get_fuelType() == m_fuelType)
            listIn << entry.get_brandName();
       else
           continue;
    }

   listIn.sort();
   return pruneDuplicates(listIn);
}


void SedbukList::initVars()
{
    m_revisionNum = 0;   
    m_day = 0;
    m_month = 0;
    m_year = 0;
}

void SedbukList::parseVersion(QString versionString) {
    QStringList versionInfo;
    
    m_revisionNum = versionString.section(",",1,1).toInt();
    m_day = versionString.section(",",4,4).toInt();
    m_month = versionString.section(",",3,3).toInt();
    m_year = versionString.section(",",2,2).toInt();
    m_revisionDate = versionString.section(",",4,4) + "/" +
                       versionString.section(",",3,3)+ "/" +
                      versionString.section(",",2,2);
}


void SedbukList::parseBoilerTable(QString parseStr)
{
   int str = 0,  f5 =0, f4 =0;
   QString f1, f2, f3, id;
   
   QStringList record = parseStr.split(",");
   QVector<QString> line = record.toVector();
    
    str = line[0].toInt();
    f1 = line[3];
    f2 = line[4];
    f3 = line[5];
    f4 = line[20].toDouble();
    f5 = line[9].toInt();
    Boiler tmp(str,f1,f2,f3,f4,f5);
    id = tmp.get_modelName() + tmp.get_modelQualifier(); 
    boilerHash.insert(id,tmp);
    
    
    //qDebug("" + id.toLatin1());
}

void SedbukList::updateSedbukDatabase()
{
    if (m_updateFlag)
    {
        //TODO`
    }
}

