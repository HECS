/***************************************************************************
 *                                                                         *
 *  RdSap.h Copyright (C) 2008 by Jon Rumble                               *
 *  j.w.rumble@reading.ac.uk   				                   *
 *                                                                         *
 *  This file is part of HECS,                                             *
 *                                                                         *
 *  HECS is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation, either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *  HECS is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/


#ifndef RDSAP_H
#define RDSAP_H

#include <QLabel>
#include <iostream>
#include <QString>
#include <QMap>
#include <QApplication>

#include "Misc.h"
#include "DwellingDimensions.h"
#include "DwellingEmissionRate.h"
#include "ConfigParser.h"
#include "Ventilation.h"
#include "HeatLosses.h"
#include "Gains.h"
#include "Sedbuk.h"
#include "WaterHeating.h"
#include "SpaceHeating.h"
#include "TableReader.h"
#include "FuelCosts.h"
#include "WaterHeatingEnergy.h"
#include  "Sedbuk.h"

class RdSap
  {

  public:

    // Constructors/Destructors
    //


    /**
     * CONSTRUCTORS
     */
    RdSap();
    RdSap(const QString& fileName);
    ~RdSap();

  void initVars();
    /**
     * METHODS
     */

  void buildSap();
  void destroySap();
  void outputTest();
  void calcModel();
  void buildBoilerList();
  void ventilationParams();
  void windowArea();
  void buildLT();
  void roomCountLivingArea();
void cylinderSize();
  //Accessors
  double getRdWindowArea ();
  double getRdTotalFloorArea();
  int getRdWindowReductionFactor();
  int getRdCylinderSize();
  void setRdWindowArea (double val_in);
  void setRdTotalFloorArea(double val_in);
  void setRdWindowReductionFactor(double val_in);
  void setRdCylinderSize(int val_in);

  DwellingDimensions *dwelling;
  HeatLosses *heatlosses;
  Gains *gains;
  Ventilation *ventilation;
  SpaceHeating *spaceheat;
  WaterHeating *hotwater;
  DwellingEmissionRate *emissions;
  FuelCosts *fuelcosts;
  WaterHeatingEnergy *hotwaterenergy;
  SedbukList *sedbukList;

  

  private:
  //RDSap Variables
  
  double m_RdWindowArea;
  double m_RdTotalFloorArea;
  int m_RdWindowReductionFactor;
  int m_RdCylinderSize;
  QMap <int, QList <double> >tableS3;
  QMap <int, QList <double> >tableS9;

  //SAP OBJECTS
  //
  //

  QString m_fileName;

  //Dialogs
  //

  // Private attributes
  //
  unsigned int m_housingBand;
  ConfigParser cf;
  //TableReader tr;
      };

#endif // RDSAP_H
