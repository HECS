/***************************************************************************
 *                                                                         *
 *  HeatLossElement.h Copyright (C) 2008 by Jon Rumble                     *
 *  j.w.rumble@reading.ac.uk   				                   *
 *                                                                         *
 *  This file is part of HECS,                                             *
 *                                                                         *
 *  HECS is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation, either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *  HECS is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/


#ifndef HEATLOSSELEMENT_H
#define HEATLOSSELEMENT_H

#include "DwellingDimensions.h"
#include "HeatLossItem.h"
#include <QString>
#include <QMap>
#include <QMapIterator>

class HeatLossElement
  {

  public:

    // Constructors/Destructors
    //


    /**
     * CONSTRUCTORS
     */
    HeatLossElement();
    ~HeatLossElement();
    //HeatLossElement() {};

    /**
     * METHODS
     */

    void setElementLossTotal(double var_in);
    double getElementLossTotal();

    void insertLoss(const QString& name,double area,double uValue);
    double getElementAreaTotal();
    void setAreaElementTotal(double val_in);
    void calcElementTotal();
  private:

    // Private attributes
    //
    

    QMap <QString, HeatLossItem> elemMap;


    void removeLoss(const QString& name);
    void updateLoss(const QString& name,double area,double uValue); 
    double m_elementLossTotal;
    double m_elementAreaTotal;

  };

#endif // HEATLOSSELEMENT_H
