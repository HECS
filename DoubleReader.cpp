/***************************************************************************
 *                                                                         * 
 *  DoubleReader.cpp Copyright (C) 2008 by Jon Rumble                             *  
 *  j.w.rumble@reading.ac.uk   				                   *
 *                                                                         *
 *                                                                         * 
 *  This file is part of HECS,                                             *
 *                                                                         *
 *  HECS is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation, either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *                                                                         *
 *  HECS is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/


#include "DoubleReader.h"
 
 
const char COMMENT = '#';
 
DoubleReader::DoubleReader () {}
 
DoubleReader::DoubleReader (const QString& fileName) 
{
    m_fileName = new QFile (fileName);
    //m_numElems = numElems;
    TableReader::openFile();
    parseFileD();
    closeFile();
}
 
DoubleReader::~DoubleReader () {}
 
void DoubleReader::readFile(const QString& fileNameIn)
{
    delete m_fileName;
    m_fileName = new QFile (fileNameIn);
    openFile();
    parseFile();
    closeFile();
}
 
void DoubleReader::parseFileD()
{
 
    QTextStream in (m_fileName);    
    while (!in.atEnd()) {
 
        QString line = in.readLine().trimmed();
 
        if (line.startsWith(COMMENT))
            continue; //Skip for comments !!
 
        parseLineD(line,ValueMap);
    }
}
 
void DoubleReader::parseLineD(QString lineIn, QMap<double, QList <double> >& ValueMap)
{
 
    double key = 0.00;
    QList <double> valueList;
 
    vals = lineIn.split(","); 
    key = vals.at(0).toDouble();
    for (int i = 1; i < vals.size(); ++i)
        valueList.append(vals.at(1).toDouble());
    ValueMap.insert(key,valueList);
}
 
QMap <double,QList <double> > DoubleReader::getDoubleMap()
{
    return ValueMap;
}
 
// END OF DoubleReader.cpp

