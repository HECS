
/***************************************************************************
 *                                                                         *
 *  Sedbuk.cpp Copyright (C) 2008 by Jon Rumble                            *
 *  j.w.rumble@reading.ac.uk   				                   *
 *                                                                         *
 *  This file is part of HECS,                                             *
 *                                                                         *
 *  HECS is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation, either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *  HECS is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/

#ifndef SEDBUK_H
#define SEDBUK_H 

#include <QString>
#include <QVector>
#include <QTextStream>
#include <QFile>
#include <QHash>
#include <QHashIterator>
#include <QStringList>
#include <iostream>
#include <QDate>
#include "Boiler.h"
#include "Misc.h"
#include <iostream>
class SedbukList {

    public:

        SedbukList (const QString fileNameIn);
        ~SedbukList ();
        void updateSedbukDatabase();
        QString get_revisionDate();
        bool get_sedbukAvail();
        QString fileName;
        QStringList getBrandList(int fuel_type);
        QStringList getModelList (QString brandNameIn);
        Boiler findBoiler (const QString& modelStr);
        QStringList getBoilerBrands(int fuel_type);
    protected:
        void openFile(QString fileName);

    private:

        bool m_sedbukAvail;
        bool m_updateFlag;
        int Sedbuk_revision;
        QDate Sedbuk_date;
        int m_revisionNum; 
        int m_day;
        int m_month;
        int m_year;
        QString m_revisionDate;
        QHash<QString, Boiler> boilerHash;
        void parseVersion(QString versionString);
        void parseBoilerTable(QString parseStr);
        void initVars();

        int m_fuelType;
};



#endif

