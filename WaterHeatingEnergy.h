/***************************************************************************
 *                                                                         * 
 *  WaterHeatingEnergy.h Copyright (C) 2008 by Jon Rumble                           *  
 *  j.w.rumble@reading.ac.uk   				                   *
 *                                                                         *
 *                                                                         * 
 *  This file is part of HECS,                                             *
 *                                                                         *
 *  HECS is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation, either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *                                                                         *
 *  HECS is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/

#ifndef WATERHEATINGENERGY_H
#define WATERHEATINGENERGY_H 

#include "WaterHeating.h"
#include "ConfigParser.h"

class WaterHeatingEnergy {

public:
WaterHeatingEnergy (const ConfigParser& config, WaterHeating *waterHeat);
WaterHeatingEnergy ();
~WaterHeatingEnergy ();


  void setEffcyOfWaterHeater ( int val_in );
  int getEffcyOfWaterHeater ( );
  void setEnergyReqdForWaterHeat ( double val_in );
  double getEnergyReqdForWaterHeat ( );
  void setElecForCentralHeatingPumps ( double val_in );
  double getElecForCentralHeatingPumps ( );
  void setElecForFanAssitFlue ( double val_in );
  double getElecForFanAssitFlue ( );
  void setElecForWarmHeatingSysFans ( double val_in );
  double getElecForWarmHeatingSysFans ( );
  void setElecForMechVentilation ( double val_in );
  double getElecForMechVentilation ( );
  void setElecForKeepHotGasCombi ( double val_in );
  double getElecForKeepHotGasCombi ( );
  void setElecForSolarWaterPump ( double val_in );
  double getElecForSolarWaterPump ( );
  void setTotalForAboveElec ( double val_in );
  double getTotalForAboveElec ( );
  void calcAll();

protected:

private:
  int m_effcyOfWaterHeater;
  double m_energyReqdForWaterHeat;
  double m_elecForCentralHeatingPumps;
  double m_elecForFanAssitFlue;
  double m_elecForWarmHeatingSysFans;
  double m_elecForMechVentilation;
  double m_elecForKeepHotGasCombi;
  double m_elecForSolarWaterPump;
  double m_totalForAboveElec;

  ConfigParser cf;
  WaterHeating *ptrWaterHeat;

void initVars();
void calcEnergyReqWaterHeating();
void calcElecForAbove();
};


#endif //END OF WATERHEATINGENERGY_H
