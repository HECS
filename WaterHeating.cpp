/***************************************************************************
 *                                                                         * 
 *  WaterHeating.cpp Copyright (C) 2008 by Jon Rumble                      *
 *  j.w.rumble@reading.ac.uk   				                   *
 *                                                                         *
 *  This file is part of HECS,                                             *                   
 *                                                                         * 
 *  HECS is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation, either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *  HECS is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/

#include "WaterHeating.h"

// Constructors/Destructors
//  

WaterHeating::WaterHeating (const ConfigParser& config, DwellingDimensions *dims) 
{
    cf = config;
    ptrDims = dims;
    initVars();
    calcAll();
}

WaterHeating::WaterHeating () { }

WaterHeating::~WaterHeating ( ) { }

//  
// Methods
//  


// Accessor methods
//  


// Other methods
//  


void WaterHeating::setEnergyContentHotWater ( double val_in ) {
  m_energyContentHotWater = val_in;
}

double WaterHeating::getEnergyContentHotWater ( ) {
  return m_energyContentHotWater;
}


void WaterHeating::setDistributionLoss ( double val_in ) {
  m_distributionLoss = val_in;
}


double WaterHeating::getDistributionLoss ( ) {
  return m_distributionLoss;
}


void WaterHeating::setHotWaterUsage ( double val_in ) {
  m_hotWaterUsage = val_in;
}


double WaterHeating::getHotWaterUsage ( ) {
  return m_hotWaterUsage;
}


void WaterHeating::setManDeclaredLoss ( double val_in ) {
  m_manDeclaredLoss = val_in;
}


double WaterHeating::getManDeclaredLoss ( ) {
  return m_manDeclaredLoss;
}


void WaterHeating::setTempFactor ( double val_in ) {
  m_tempFactor = val_in;
}


double WaterHeating::getTempFactor ( ) {
  return m_tempFactor;
}


void WaterHeating::setEnergyLostWaterStore ( double val_in ) {
  m_energyLostWaterStore = val_in;
}


double WaterHeating::getEnergyLostWaterStore ( ) {
  return m_energyLostWaterStore;
}


void WaterHeating::setCylinderVolume ( double val_in ) {
  m_cylinderVolume = val_in;
}


double WaterHeating::getCylinderVolume ( ) {
  return m_cylinderVolume;
}


void WaterHeating::setHotWaterStoreLossFactor ( double val_in ) {
  m_hotWaterStoreLossFactor = val_in;
}


double WaterHeating::getHotWaterStoreLossFactor ( ) {
  return m_hotWaterStoreLossFactor;
}


void WaterHeating::setVolumeFactor ( double val_in ) {
  m_volumeFactor = val_in;
}


double WaterHeating::getVolumeFactor ( ) {
  return m_volumeFactor;
}


void WaterHeating::setSolarStorageVal ( double val_in ) {
  m_solarStorageVal = val_in;
}


double WaterHeating::getSolarStorageVal ( ) {
  return m_solarStorageVal;
}


void WaterHeating::setPrimaryCircuitLoss ( double val_in ) {
  m_primaryCircuitLoss = val_in;
}


double WaterHeating::getPrimaryCircuitLoss ( ) {
  return m_primaryCircuitLoss;
}

void WaterHeating::setCombiLoss ( double val_in ) {
  m_combiLoss = val_in;
}


double WaterHeating::getCombiLoss ( ) {
  return m_combiLoss;
}


void WaterHeating::setSolarDhw ( double val_in ) {
  m_solarDhw = val_in;
}


double WaterHeating::getSolarDhw ( ) {
  return m_solarDhw;
}


void WaterHeating::setWaterHeaterOutput ( double val_in ) {
  m_waterHeaterOutput = val_in;
}


double WaterHeating::getWaterHeaterOutput ( ) {
  return m_waterHeaterOutput;
}


void WaterHeating::setHotWaterHeatGains ( double val_in ) {
  m_hotWaterHeatGains = val_in;
}


double WaterHeating::getHotWaterHeatGains ( ) {
  return m_hotWaterHeatGains;
}

void WaterHeating::calcAll()
{
    calcHotWaterGains();
}


void WaterHeating::calcHotWaterGains()
{
   //Some Temp Vars for ease
   double N= 0.00, TFA= 0.00;
   TFA = ptrDims->get_totalFloorArea();
    
   
   if (TFA <= 420.00)
       N = 0.035 * TFA - 0.000038 * TFA*TFA;
   else
       //Floor Area is above 420 sq Meters,
       N = 8;

   m_hotWaterUsage = (25 * N) + 38;
   m_energyContentHotWater = ((61 * N)+ 92) * 0.85 * 8.76;
   m_distributionLoss = ((61 * N) + 92) * 0.15 * 8.76;


   //Method A
   if (m_declaredLossKnown == 1)
   {
       m_tempFactor = tempFactorLossKnown();
       m_energyLostWaterStore = (m_manDeclaredLoss * m_tempFactor)*365;
   }
   
   //Method B
   else 
   {
     double L = 0.00, VF= 0.00;

     if (m_looseJacket == 1)
        L = 0.005 + 1.76 / (m_insultaionThickness + 12.80);
     else
         L = 0.005 + 0.55 / (m_insultaionThickness + 4.00);

     VF = pow((120.00 / m_cylinderVolume),0.33333333);

     m_tempFactor = tempFactorLossNotKnown();
     m_energyLostWaterStore = m_cylinderVolume*
                             L                *
                             VF               *
                             m_tempFactor     *
                             365;
   }

   //m_solarStorageVal = m_energyLostWaterStore;

   primaryCircuitLoss();

   m_solarDhw = 0.00; //FIX

   m_waterHeaterOutput = m_energyContentHotWater + 
                        m_distributionLoss      +
                        m_solarStorageVal       +
                        m_primaryCircuitLoss    +
                        m_combiLoss             -
                        m_solarDhw;        

  m_hotWaterHeatGains = 0.25 * (m_energyContentHotWater + m_combiLoss) +
                        0.8                                            *
                               (m_distributionLoss + m_solarStorageVal + 
                                m_primaryCircuitLoss);
}


double WaterHeating::tempFactorLossKnown()
{
    
    switch (m_storageType)
    {
        //Cylinder
        case 1:	
            m_tempFactor = 0.60;
            break;
        //Cylinder - Cylinder Thermostat Absent.
        case 2:	
            m_tempFactor = 0.60*1.3;
            break;
        //Cylinder - Seperate time control 
        case 3:	
            m_tempFactor = 0.60*0.9;
            break;
        //Combi-Primary
        case 4:
            m_tempFactor = 0.00;
            break;
        //Combi-Secondary
        case 5:
            m_tempFactor = 0.00;
            break;
        //Hot water only thermal store
        case 6:
            m_tempFactor = 0.89;
            break;
        //Hot water only thermal store - seperate timer
        case 7:
            m_tempFactor = 0.89*0.81;
            break;
        //Hot water only thermal store - not in airing cupboard
        case 8:
            m_tempFactor = 0.89*1.1;
            break;
        //Integrated thermal store gasfired CPSU
        case 9:
            m_tempFactor = 0.89;
            break;
        //Integrated thermal store gasfired CPSU - seperate timer
        case 10:
            m_tempFactor = 0.89*0.81;
            break;
        //Integrated thermal store gasfired CPSU - not in airing cupboard
        case 11:
            m_tempFactor = 0.89*1.1;
            break;
        //Electrical CPSU winter op temp 85C
        case 12:
            m_tempFactor = 1.09;
            break;
        //Electrical CPSU winter op temp 90C
        case 13:
            m_tempFactor = 1.15;
            break;
        //Electrical CPSU winter op temp 95C
        case 14:
            m_tempFactor = 1.21;
            break;
        default:	
            break;
    }				/* -----  end switch  ----- */
    return m_tempFactor;
}

double WaterHeating::tempFactorLossNotKnown()
{
    switch (m_storageType)
    {
        //Cylinder
        case 1:	
            m_tempFactor = 0.60;
            break;
        //Cylinder - Cylinder Thermostat Absent.
        case 2:	
            m_tempFactor = 0.60*1.3;
            break;
        //Cylinder - Seperate time control 
        case 3:	
            m_tempFactor = 0.60*0.9;
            break;
        //Combi-Primary
        case 4:
            if (m_combiStore >= 115.00)
                m_tempFactor = 0.82;
            else
                m_tempFactor = 0.82 + 0.0022 * (115.00 - m_combiStore);
            break;

        //Combi-Secondary
        case 5:
            if (m_combiStore >= 115.00)
                m_tempFactor = 0.60;
            else
                m_tempFactor = 0.60 + 0.0016 * (115.00 - m_combiStore);
            break;
        //Hot water only thermal store
        case 6:
            m_tempFactor = 1.08;
            break;
        //Hot water only thermal store - seperate timer
        case 7:
            m_tempFactor = 1.08*0.81;
            break;
        //Hot water only thermal store - not in airing cupboard 
        case 8:
            m_tempFactor = 1.08*1.1;
            break;
        //Integrated thermal store gasfired CPSU
        case 9:
            m_tempFactor = 1.08;
            break;
        //Integrated thermal store gasfired CPSU - seperate timer
        case 10:
            m_tempFactor = 1.08*0.81;
            break;
        //Integrated thermal store gasfired CPSU - not in airing cupboard
        case 11:
            m_tempFactor = 1.08*1.1;
            break;
        //Electrical CPSU winter op temp 85C
        case 12:
            m_tempFactor = 1.00;
            break;
        //Electrical CPSU winter op temp 90C
        case 13:
            m_tempFactor = 1.00;
            break;
        //Electrical CPSU winter op temp 95C
        case 14:
            m_tempFactor = 1.00;
            break;
        default:	
            break;
    }				/* -----  end switch  ----- */
    return m_tempFactor;

}

void WaterHeating::primaryCircuitLoss()
{

    switch (m_systemType)
    {
        //Electric Immersion Heater
        case 1:	
            m_primaryCircuitLoss = 0.00;
            break;
        //Boiler Uninsulated Pipework, no cyl thermostat
        case 2:	
            m_primaryCircuitLoss = 1220.00;
            break;
        //Boiler insulated Pipework, no cyl thermostat 
        case 3:	
            m_primaryCircuitLoss = 610.00;
            break;
        //Boiler uninsulated Pipework, cyl thermostat
        case 4:
            m_primaryCircuitLoss = 610.00;
            break;
        //Boiler insulated Pipework, cyl thermostat
        case 5:
            m_primaryCircuitLoss = 360.00;
            break;
        //Combi Boiler
        case 6:
            m_primaryCircuitLoss = 0.00;
            break;
        //CPSU (including electric CPSU)
        case 7:
            m_primaryCircuitLoss = 0.00;
            break;
        //Boiler and thermal store within a single casing (cyl, thermo present)
        case 8:
            m_primaryCircuitLoss = 0.00;
            break;
        //Sep. boiler and thermal store  1.5m<= of ins pipe.
        case 9:
            m_tempFactor = 0.00;
            break;
        //Sep boiler and thermal store - uninsulated pipework
        case 10:
            m_tempFactor = 470.00;
            break;
        //Sep boiler and thermal store - >=1.5m insulated pipework
        case 11:
            m_tempFactor = 280.00;
            break;
        //Community Heating
        case 12:
            m_tempFactor = 360.00;
            break;
        default:	
            break;
    }				/* -----  end switch  ----- */
}


void WaterHeating::initVars ( ) {

  m_energyContentHotWater = cf.getValueDouble ("WaterHeating:energyContentHotWater"); 
  m_distributionLoss = cf.getValueDouble ("WaterHeating:distributionLoss"); 
  m_hotWaterUsage = cf.getValueDouble ("WaterHeating:distributionLoss"); 
  m_manDeclaredLoss = cf.getValueDouble ("WaterHeating:manDeclaredLoss");
  m_tempFactor = cf.getValueDouble ("WaterHeating:tempFactor");
  m_energyLostWaterStore = cf.getValueDouble ("WaterHeating:energyLostWaterStore");
  m_cylinderVolume = cf.getValueDouble ("WaterHeating:cylinderVolume");
  m_hotWaterStoreLossFactor = cf.getValueDouble ("WaterHeating:hotWaterStoreLossFactor");
  m_volumeFactor = cf.getValueDouble ("WaterHeating:volumeFactor");
  m_solarStorageVal = cf.getValueDouble ("WaterHeating:solarStorageVal");
  m_primaryCircuitLoss = cf.getValueDouble ("WaterHeating:primaryCircuitLoss");
  m_solarDhw = cf.getValueDouble ("WaterHeating:solarDhw");
  m_waterHeaterOutput = cf.getValueDouble ("WaterHeating:waterHeaterOutput");
  m_hotWaterHeatGains = cf.getValueDouble ("WaterHeating:hotWaterHeatGains");
  m_insultaionThickness = cf.getValueDouble ("WaterHeating:insulationThickness");
  m_declaredLossKnown = cf.getValueInt ("WaterHeating:declaredLossKnown");
  m_looseJacket = cf.getValueDouble ("WaterHeating:looseJacket");
  m_combiStore = cf.getValueDouble ("WaterHeating:combiStore");
  m_storageType= cf.getValueDouble ("WaterHeating:storageType");
  m_systemType = cf.getValueDouble ("WaterHeating:systemType");
}
