/***************************************************************************
 *                                                                         * 
 *  HeatingDialog.cpp Copyright (C) 2008 by Jon Rumble                     *     *                                                                         * 
 *  j.w.rumble@reading.ac.uk   				                   *
 *                                                                         *
 *                                                                         * 
 *  This file is part of HECS,                                             *
 *                                                                         *
 *  HECS is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation, either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *                                                                         *
 *  HECS is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/

#include "HeatingDialog.h"

HeatingDialog::HeatingDialog (QWidget *parent)
    : QDialog(parent)
{
    ui.setupUi(this);
}


void HeatingDialog::setModel(RdSap *modelIn)
{
    sapModel = modelIn;
    initVars();
}


void HeatingDialog::onBoilerFuelTypeCbxChange()
{

    m_fuelFlag = ui.boilerFuelTypeCbx->currentIndex();
    ui.boilerManufacturerCbx->clear();

   switch (m_fuelFlag)
   {
       case 0: 
       ui.boilerManufacturerCbx->insertItems(
       1,sapModel->sedbukList->getBoilerBrands(1));
               break; // Gas
       case 1: 
       ui.boilerManufacturerCbx->insertItems(
       1,sapModel->sedbukList->getBoilerBrands(2));
               break; //LPG
       case 2:
       ui.boilerManufacturerCbx->insertItems(
       1,sapModel->sedbukList->getBoilerBrands(4));
        break;
               //Why 4 ?! Listed in spec.... 
        ui.boilerManufacturerCbx->setCurrentIndex(0);
   }
 }

void HeatingDialog::onBoilerManufacturerCbxChange()
{
    ui.boilerModelCbx->clear();
    
    QString searchString = ui.boilerManufacturerCbx->currentText();
    
    ui.boilerModelCbx->insertItems(
       1,sapModel->sedbukList->getModelList(searchString));
    
}

void HeatingDialog::onBoilerModelCbxChange()
{
    QString searchString = "";
    searchString = ui.boilerModelCbx->currentText();

    Boiler tmp;
    tmp = sapModel->sedbukList->findBoiler(searchString);
    QString lblTxt = QString::number(tmp.get_sapSeasonalEffcy(),'f',1) + "%";
    ui.effcyLbl->setText(lblTxt);
    sapModel->spaceheat->setEffcyMainHeatingSys(tmp.get_sapSeasonalEffcy());
}


void HeatingDialog::buildModelTable()
{

}

void HeatingDialog::initVars()
{
ui.boilerManufacturerCbx->insertItems(1,sapModel->sedbukList->getBoilerBrands(1));
 
//ui.boilerModelCbx->insertItems(1,sapModel->sedbukList->getModelList("Baxi"));
 
   QObject::connect(ui.boilerFuelTypeCbx, SIGNAL(currentIndexChanged(QString)), this,SLOT(onBoilerFuelTypeCbxChange()));

   QObject::connect(ui.boilerManufacturerCbx, SIGNAL(currentIndexChanged(QString)), this,SLOT(onBoilerManufacturerCbxChange()));

   QObject::connect(ui.boilerModelCbx, SIGNAL(currentIndexChanged(QString)), this,SLOT(onBoilerModelCbxChange()));
   //
    //Populate Adress Details
    ui.houseNumLbl->setText(sapModel->dwelling->get_houseNumber());
    ui.addressLbl->setText(sapModel->dwelling->get_address());
    ui.postcodeLbl->setText(sapModel->dwelling->get_postcode());


 }

// END OF HeatingDialog.cpp
