/***************************************************************************
 *                                                                         *
 *  SolarGains.h Copyright (C) 2008 by Jon Rumble                          *
 *  j.w.rumble@reading.ac.uk   				                   *
 *                                                                         *
 *  This file is part of HECS,                                             *
 *                                                                         *
 *  HECS is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation, either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *  HECS is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/


#ifndef SOLARGAINS_H
#define SOLARGAINS_H

#include <QString>
class SolarGains
  {

  public:

    // Constructors/Destructors
    //


    /**
     * CONSTRUCTORS
     */
    SolarGains( const int overshadow,
                const int orientation,
                const double area,
                const int frameType,
                const int glazingType,
                const char season
                );

   ~SolarGains();
    SolarGains() {};      

    /**
     * METHODS
     */
    void initVars();
   
     void setAccessFactor ( double var_in );
     double getAccessFactor ( );
     void setArea ( double var_in );
     double getArea ( );
     void setSolarFlux ( int var_in );
     int getSolarFlux ( );
     void setGlazTransmitanceFactor ( double var_in );
     double getGlazTransmitanceFactor ( );
     void setFrameFactor ( double var_in );
     double getFrameFactor ( );
     void setSolarGains ( double var_in );
     double getSolarGains ( );

     void calcSolarGains ();

    
  private:
    // Private attributes
    //
    // Solar Gains
      double m_accessFactor;
      double m_area;
      int m_solarFlux;
      double m_glazTransmitanceFactor;
      double m_frameFactor;
      double m_solarGains;
      char m_season;
      int m_frameType;
      int m_overshadow;
      int m_glazingType;
      int m_orientation;
      
    //Lookup Tables
   
    

    
  };

#endif // SOLARGAINS_H
