/***************************************************************************
 *                                                                         * 
 *  TableReader.cpp Copyright (C) 2008 by Jon Rumble                             *  
 *  j.w.rumble@reading.ac.uk   				                   *
 *                                                                         *
 *                                                                         * 
 *  This file is part of HECS,                                             *
 *                                                                         *
 *  HECS is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation, either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *                                                                         *
 *  HECS is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/


#include "TableReader.h"


const char COMMENT = '#';

TableReader::TableReader () {}

TableReader::TableReader (QString fileName) 
{
    m_fileName = new QFile (fileName);
    //m_numElems = numElems;
    openFile();
    parseFile();
    closeFile();
    
}

TableReader::~TableReader () {}

void TableReader::readFile(QString fileNameIn)
{
    m_fileName = new QFile (fileNameIn);
    openFile();
    parseFile();
    closeFile();
}

void TableReader::openFile ()
{
      if (!m_fileName->open(QIODevice::ReadOnly | QIODevice::Text))
           return;
}

void TableReader::closeFile ()
{
    m_fileName->close();
}

void TableReader::parseFile ()
{

    QTextStream in (m_fileName);    
    int lineNum =1;
    while (!in.atEnd()) {
        
        QString line = in.readLine().trimmed();

        if (line.startsWith(COMMENT))
            continue; //Skip for comments !!
        dValueMap.insert(lineNum,parseLine(line));
        lineNum++;
    }
}

QList <double> TableReader::parseLine (QString lineIn)
{

    QList <double> valueList;
    vals = lineIn.split(","); 
    for (int i = 0; i < vals.size(); ++i)
        valueList.append(vals.at(i).toDouble());

    return valueList;
}

QMap <int, QList <double> > TableReader::getTableMap()
{
    return dValueMap;
}

void TableReader::initVars()
{

}
// END OF TableReader.cpp
