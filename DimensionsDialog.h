/***************************************************************************
 *                                                                         * 
 *  DimensionsDialog.h Copyright (C) 2008 by Jon Rumble                          *  
 *  j.w.rumble@reading.ac.uk   				                   *
 *                                                                         *
 *                                                                         * 
 *  This file is part of HECS,                                             *
 *                                                                         *
 *  HECS is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation, either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *                                                                         *
 *  HECS is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/

#ifndef DIMENSIONSDIALOG_H
#define DIMENSIONSDIALOG_H 

#include "ui_dimensions.h"
#include "RdSap.h"
#include "Misc.h"
#include <QDialog>
#include <QString>
class DimensionsDialog : public QDialog
{

    Q_OBJECT

    public:
    DimensionsDialog (QWidget *parent =0);
    void setModel(RdSap *modelIn);
    //virtual ~DimensionsDialog ();

    public slots:
    void onFirstFloorHeightChanged();
    void onSecondFloorHeightChanged();
    void onThirdFloorHeightChanged();
    void calcSqMetre();
    void onFirstFloorAreaChanged();
    void onSecondFloorAreaChanged();
    void onThirdFloorAreaChanged();
    void onNumChimneysCbxChanged();
    void onNumGasFiresCbxChanged();

    void onNumWindowsCbxChanged();
    signals:
    void calcSqMetreDone(double val);


    private: 
    Ui::DimensionsDialog ui;
    RdSap *sapModel; 
    void initVars();

    //Internal Vars for widget
    double m_firstFloorArea;
    double m_secondFloorArea;
    double m_thirdFloorArea;

    double m_firstFloorHeight;
    double m_secondFloorHeight;
    double m_thirdFloorHeight;
};


#endif //END OF DIMENSIONSDIALOG_H
