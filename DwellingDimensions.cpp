/***************************************************************************
 *                                                                         * 
 *  DwellingDimensions.h.cpp Copyright (C) 2008 by Jon Rumble              *
 *  j.w.rumble@reading.ac.uk   				                   *
 *                                                                         *
 *  This file is part of HECS,                                             *                   
 *                                                                         * 
 *  HECS is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation, either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *  HECS is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/

#include "DwellingDimensions.h"

// Constructors/Destructors
//  

DwellingDimensions::DwellingDimensions (const ConfigParser& config) 
{
    cf = config;
    initVars();
    calcAll();
}

DwellingDimensions::~DwellingDimensions ( ) { }

//  
// Methods
//  


// Accessor methods
//  

double DwellingDimensions::get_groundFloorArea()
{
    return m_groundFloorArea;
}

double DwellingDimensions::get_groundFloorHeight()
{
    return m_groundFloorHeight;
}

double DwellingDimensions::get_groundFloorVolume()
{
    return m_groundFloorVolume;
}

double DwellingDimensions::get_firstFloorArea()
{
    return m_firstFloorArea;
}

double DwellingDimensions::get_firstFloorHeight()
{
    return m_firstFloorHeight;
}

double DwellingDimensions::get_firstFloorVolume()
{
    return m_firstFloorVolume;
}

double DwellingDimensions::get_secondFloorArea()
{
    return m_secondFloorArea;
}

double DwellingDimensions::get_secondFloorHeight()
{
    return m_secondFloorHeight;
}

double DwellingDimensions::get_secondFloorVolume()
{

    return m_secondFloorVolume;
}

double DwellingDimensions::get_thirdFloorArea()
{
    return m_thirdFloorArea;
}

double DwellingDimensions::get_thirdFloorHeight()
{
    return m_thirdFloorHeight;
}

double DwellingDimensions::get_thirdFloorVolume()
{
    return m_thirdFloorVolume;
}
double DwellingDimensions::get_totalFloorArea()
{
    return m_totalFloorArea;
}

double DwellingDimensions::get_livingRoomArea()
{
    return m_livingRoomArea;
}


double DwellingDimensions::get_dwellingVolume()
{
    return m_dwellingVolume;
}

double DwellingDimensions::get_avgStoreyHeight()
{
    return m_avgStoreyHeight;
}


QString DwellingDimensions::get_houseNumber()
{
    return m_houseNumber;
}


QString DwellingDimensions::get_address()
{
    return m_address;
}

QString DwellingDimensions::get_town()
{
    return m_town;
}

QString DwellingDimensions::get_postcode()
{
    return m_postcode;
}

QString DwellingDimensions::get_county()
{
    return m_county;
}


int DwellingDimensions::get_region()
{
    return m_region;
}

int DwellingDimensions::get_ageBand()
{
    return m_ageBand;
}

int DwellingDimensions::get_propertyType()
{
    return m_propertyType;
}

int DwellingDimensions::get_archType()
{
    return m_archType;
}

int DwellingDimensions::get_habitableRooms()
{
    return m_habitableRooms;
}

void DwellingDimensions::set_groundFloorArea(double val_in)
{
    m_groundFloorArea = val_in;
}

void DwellingDimensions::set_groundFloorHeight(double val_in)
{
    m_groundFloorHeight = val_in;

}

void DwellingDimensions::set_groundFloorVolume(double val_in)
{
    m_groundFloorVolume = val_in;
}

void DwellingDimensions::set_firstFloorArea (double val_in)
{
    m_firstFloorArea = val_in;
}

void DwellingDimensions::set_firstFloorHeight (double val_in)
{
    m_firstFloorHeight = val_in;
}

void DwellingDimensions::set_firstFloorVolume (double val_in)
{
    m_firstFloorVolume = val_in;
}

void DwellingDimensions::set_secondFloorArea(double val_in)
{
    m_secondFloorArea = val_in;
}

void DwellingDimensions::set_secondFloorHeight(double val_in)
{
    m_secondFloorHeight = val_in;
}
void DwellingDimensions::set_thirdFloorArea (double val_in)
{
    m_thirdFloorArea = val_in;
}

void DwellingDimensions::set_thirdFloorHeight (double val_in)
{
    m_thirdFloorHeight = val_in;
}
void DwellingDimensions::set_thirdFloorVolume (double val_in)
{
    m_thirdFloorVolume = val_in;
}

void DwellingDimensions::set_totalFloorArea (double val_in)
{
    m_totalFloorArea = val_in;
}
void DwellingDimensions::set_dwellingVolume (double val_in)
{
    m_dwellingVolume = val_in;
}
void DwellingDimensions::set_avgStoreyHeight(double val_in)
{
    m_avgStoreyHeight = val_in;
}

void DwellingDimensions::set_livingRoomArea(double val_in)
{
    m_livingRoomArea = val_in;
}

void DwellingDimensions::set_houseNumber(QString val_in)
{
    m_houseNumber = val_in;
}


void DwellingDimensions::set_address(QString val_in)
{
    m_address = val_in;
}

void DwellingDimensions::set_town(QString val_in)
{
    m_town = val_in;
}

void DwellingDimensions::set_postcode(QString val_in)
{
    m_postcode = val_in;
}

void DwellingDimensions::set_county(QString val_in)
{
    m_county = val_in;
}


void DwellingDimensions::set_region(int val_in)
{
    m_region = val_in;
}

void DwellingDimensions::set_ageBand(int val_in)
{
    m_ageBand = val_in;
}

void DwellingDimensions::set_propertyType(int val_in)
{
    m_propertyType = val_in;
}

void DwellingDimensions::set_archType(int val_in)
{
    m_archType = val_in;
}

void DwellingDimensions::set_habitableRooms(int val_in)
{
    m_habitableRooms = val_in;
}

// Other methods
//  


void DwellingDimensions::calcAll()
{
    calcTotalFloorArea();
    calcDwellingVol();
}

void DwellingDimensions::calcTotalFloorArea()
{
    m_totalFloorArea = m_groundFloorArea+
                       m_firstFloorArea+
                       m_secondFloorArea+
                       m_thirdFloorArea;
}

void DwellingDimensions::calcDwellingVol()
{
    m_dwellingVolume = (m_groundFloorArea*m_groundFloorHeight)+
                       (m_firstFloorArea*m_firstFloorHeight)+
                       (m_secondFloorArea*m_secondFloorHeight)+
                       (m_thirdFloorArea*m_thirdFloorHeight);
}

void DwellingDimensions::initVars() {


  m_groundFloorArea = cf.getValueDouble ("DwellingDimensions:groundFloorArea");
  m_groundFloorHeight = cf.getValueDouble ("DwellingDimensions:groundFloorHeight");
  m_groundFloorVolume = cf.getValueDouble ("DwellingDimensions:groundFloorVolume");
  m_firstFloorArea = cf.getValueDouble ("DwellingDimensions:firstFloorArea");
  m_firstFloorHeight = cf.getValueDouble ("DwellingDimensions:firstFloorHeight");
  m_firstFloorVolume = cf.getValueDouble ("DwellingDimensions:firstFloorVolume");
  m_secondFloorArea = cf.getValueDouble ("DwellingDimensions:secondFloorArea");
  m_secondFloorHeight = cf.getValueDouble ("DwellingDimensions:secondFloorHeight");
  m_secondFloorVolume = cf.getValueDouble ("DwellingDimensions:secondFloorVolume");
  m_thirdFloorArea = cf.getValueDouble ("DwellingDimensions:thirdFloorArea");
  m_thirdFloorHeight = cf.getValueDouble ("DwellingDimensions:thirdFloorHeight");
  m_thirdFloorVolume = cf.getValueDouble ("DwellingDimensions:thirdFloorVolume");
  m_avgStoreyHeight = cf.getValueDouble ("DwellingDimensions:avgStoreyHeight");
  m_livingRoomArea = cf.getValueDouble ("DwellingDimensions:livingRoomArea");
  m_houseNumber = cf.getValueString ("DwellingDimensions:houseNumber");
  m_address = cf.getValueString ("DwellingDimensions:address");
  m_postcode = cf.getValueString ("DwellingDimensions:postcode");
  m_county = cf.getValueString ("DwellingDimensions:county");
  m_region = cf.getValueInt ("DwellingDimensions:region");
  m_ageBand = cf.getValueInt ("DwellingDimensions:ageBand");
  m_propertyType = cf.getValueInt ("DwellingDimensions:propertyType");
  m_archType = cf.getValueInt ("DwellingDimensions:archType");
  m_totalFloorArea = 0.0;
  m_dwellingVolume = 0.0;
}

