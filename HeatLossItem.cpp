/***************************************************************************
 *                                                                         *
 *  HeatLossItem.cpp Copyright (C) 2008 by Jon Rumble                        *
 *  j.w.rumble@reading.ac.uk   				                   *
 *                                                                         *
 *  This file is part of HECS,                                             *
 *                                                                         *
 *  HECS is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation, either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *  HECS is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/

#include "HeatLossItem.h"
// Constructors/Destructors
//

HeatLossItem::HeatLossItem(double area, double uValue)
{
    m_area = area;
    m_uValue = uValue;
    m_wK = calcWk(m_area,m_uValue);
}

HeatLossItem::~HeatLossItem () { }

//
// Methods
//
    void HeatLossItem::setArea(double var_in)
    {
        m_area = var_in;
    }

    void HeatLossItem::setuValue(double var_in)
    {
        m_uValue = var_in;
    }

    double HeatLossItem::getArea()
    {
        return m_area;
    }
    
    double HeatLossItem::getuValue()
    {
        return m_uValue;
    }

    double HeatLossItem::getwK()
    {
        calcWk(m_area,m_uValue);
        return m_wK;
    }





// Accessor methods
//


