/***************************************************************************
 *                                                                         * 
 *  DwellingInformationDialog.cpp Copyright (C) 2008 by Jon Rumble                        *  
 *  j.w.rumble@reading.ac.uk   				                   *
 *                                                                         *
 *                                                                         * 
 *  This file is part of HECS,                                             *
 *                                                                         *
 *  HECS is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation, either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *                                                                         *
 *  HECS is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/

#include "DwellingInformationDialog.h"

DwellingInformationDialog::DwellingInformationDialog (QWidget *parent)
    : QDialog(parent)
{
    ui.setupUi(this);
}


void DwellingInformationDialog::setModel(RdSap *modelIn)
{
    sapModel = modelIn;
    initVars();
}

void DwellingInformationDialog::onHouseNameEditChange()
{
    QString tmp = ui.houseNameEdit->text();
    sapModel->dwelling->set_houseNumber(tmp);
}

void DwellingInformationDialog::onAddressEditChange()
{
    QString tmp = ui.addressEdit->toPlainText();
    sapModel->dwelling->set_address(tmp);
}

void DwellingInformationDialog::onTownEditChange()
{
    QString tmp = ui.townEdit->text();
    sapModel->dwelling->set_town(tmp);
}

void DwellingInformationDialog::onPostcodeEditChange()
{
    QRegExp poscodeRx ("(GIR 0AA|[A-PR-UWYZ]([0-9]{1,2}|([A-HK-Y][0-9]|[A-HK-Y][0-9]([0-9]|[ABEHMNPRV-Y]))|[0-9][A-HJKS-UW]) [0-9][ABD-HJLNP-UW-Z]{2})");



    QString tmp = ui.postcodeEdit->text();
    sapModel->dwelling->set_postcode(tmp);
}

void DwellingInformationDialog::onCountyCbxChange()
{
    QString tmp = ui.countyCbx->currentText();
    sapModel->dwelling->set_county(tmp);
}


void DwellingInformationDialog::onRegionCbxChange()
{
    int tmp = ui.regionCbx->currentIndex();
    sapModel->dwelling->set_region(tmp);
    if (ui.regionCbx->currentIndex() == 0)
    {
        ui.ageBandCbx->clear();
        ui.ageBandCbx->addItem("Before 1900");
        ui.ageBandCbx->addItem("1900 - 1929");
        ui.ageBandCbx->addItem("1930 - 1949");
        ui.ageBandCbx->addItem("1950 - 1966");
        ui.ageBandCbx->addItem("1967 - 1975");
        ui.ageBandCbx->addItem("1976 - 1982");
        ui.ageBandCbx->addItem("1983 - 1990");
        ui.ageBandCbx->addItem("1991 - 1995");
        ui.ageBandCbx->addItem("1996 - 2002");
        ui.ageBandCbx->addItem("2003 - 2006");
        ui.ageBandCbx->addItem("2007 onwards");
    }

    if (ui.regionCbx->currentIndex() == 1)
        {
            ui.ageBandCbx->clear();
            ui.ageBandCbx->addItem("Before 1919");
            ui.ageBandCbx->addItem("1919 - 1929");
            ui.ageBandCbx->addItem("1930 - 1949");
            ui.ageBandCbx->addItem("1950 - 1964");
            ui.ageBandCbx->addItem("1965 - 1975");
            ui.ageBandCbx->addItem("1976 - 1983");
            ui.ageBandCbx->addItem("1984 - 1991");
            ui.ageBandCbx->addItem("1992 - 1998");
            ui.ageBandCbx->addItem("1999 - 2002");
            ui.ageBandCbx->addItem("2003 - 2007");
            ui.ageBandCbx->addItem("2008 onwards");
        }

    if (ui.regionCbx->currentIndex() == 2)
        {
            ui.ageBandCbx->clear();
            ui.ageBandCbx->addItem("Before 1919");
            ui.ageBandCbx->addItem("1919 - 1929");
            ui.ageBandCbx->addItem("1930 - 1949");
            ui.ageBandCbx->addItem("1950 - 1973");
            ui.ageBandCbx->addItem("1974 - 1977");
            ui.ageBandCbx->addItem("1978 - 1985");
            ui.ageBandCbx->addItem("1986 - 1991");
            ui.ageBandCbx->addItem("1992 - 1999");
            ui.ageBandCbx->addItem("2000 - 2006");
            ui.ageBandCbx->addItem("(not applicable)");
            ui.ageBandCbx->addItem("2007 onwards");
        }
}


void DwellingInformationDialog::onAgeBandCbxChange()
{
    sapModel->dwelling->set_ageBand(ui.ageBandCbx->currentIndex());
}

void DwellingInformationDialog::initVars()
{
   QObject::connect(ui.houseNameEdit, SIGNAL(textChanged(QString)), this,SLOT(onHouseNameEditChange()));

   QObject::connect(ui.addressEdit, SIGNAL(textChanged()), this,SLOT(onAddressEditChange()));
   QObject::connect(ui.townEdit, SIGNAL(textChanged(QString)), this,SLOT(onTownEditChange()));
   QObject::connect(ui.postcodeEdit, SIGNAL(textChanged(QString)), this,SLOT(onPostcodeEditChange()));
   QObject::connect(ui.countyCbx, SIGNAL(currentIndexChanged(QString)), this,SLOT(onCountyCbxChange()));
    QObject::connect(ui.regionCbx, SIGNAL(currentIndexChanged(QString)), this,SLOT(onRegionCbxChange()));
    QObject::connect(ui.ageBandCbx, SIGNAL(currentIndexChanged(QString)), this,SLOT(onAgeBandCbxChange()));

     QObject::connect(ui.regionCbx,SIGNAL(currentIndexChanged(QString)),this    ,SLOT(onRegionCbxChange()));


}

// END OF DwellingInformationDialog.cpp
