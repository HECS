/***************************************************************************
 *                                                                         * 
 *  TableReader.h Copyright (C) 2008 by Jon Rumble                           *  
 *  j.w.rumble@reading.ac.uk   				                   *
 *                                                                         *
 *                                                                         * 
 *  This file is part of HECS,                                             *
 *                                                                         *
 *  HECS is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation, either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *                                                                         *
 *  HECS is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/

#ifndef TABLEREADER_H
#define TABLEREADER_H 
 
#include <QString>
#include <QStringList>
#include <QList>
#include <QFile>
#include <QTextStream>
#include <QMap>
 
class TableReader {
 
public:
TableReader (QString fileName);
TableReader();
virtual ~TableReader ();
 
void readFile(QString fileNameIn);
 
QMap <int, QList <double> > getTableMap();
 
protected:
void openFile();
void parseFile();
void closeFile();
virtual QList <double> parseLine(const QString lineIn); 
 
 
 
 
QFile *m_fileName;
void initVars();
QStringList vals;
int m_numOfValues;
int m_numElems;
int m_keyType;
private:
 
QMap <int, QList <double> > dValueMap;
 
};
 
 
#endif //END OF TABLEREADER_H
