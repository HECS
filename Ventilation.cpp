/***************************************************************************
 *                                                                         *
 *  Ventilation.cpp Copyright (C) 2008 by Jon Rumble                       *
 *  j.w.rumble@reading.ac.uk   				                   *
 *                                                                         *
 *  This file is part of HECS,                                             *
 *                                                                         *
 *  HECS is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation, either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *  HECS is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/

#include "Ventilation.h"

// Constructors/Destructors
//

Ventilation::Ventilation(const ConfigParser& config,DwellingDimensions* dims)
{
  cf = config;
  ptrDims = dims;
  initVars();
  calcAll();
}

Ventilation::~Ventilation ( )
{ 
    //TODO 
}

//
// Methods
//


// Accessor methods
//

void Ventilation::set_numChimneys(int val_in)
{
    m_numChimneys = val_in;
}
void Ventilation::set_numOpenFlues(int val_in)
{
    m_numOpenFlues = val_in;
}

void Ventilation::set_numPasivFanVents(int val_in)
{
    m_numPasivFanVents = val_in;
}
void Ventilation::set_numGasFluelessFires(int val_in)
{
    m_numGasFluelessFires = val_in;
}

void Ventilation::set_cvfInfiltration(int val_in)
{
    m_cvfInfiltration = val_in;
}

void Ventilation::set_numAirChanges(double val_in) 
{
    m_numAirChanges = val_in;
}

void Ventilation::set_pressureTest(bool val_in) 
{
    m_pressureTest = val_in;
}

void Ventilation::set_numStoreys(int val_in) 
{
    m_numStoreys = val_in;
}

void Ventilation::set_addtnInfiltration(double val_in)
{
    m_addtnInfiltration = val_in;
}

void Ventilation::set_structInfiltrationType(int val_in) 
{
       m_structInfiltrationType = val_in;
}

void Ventilation::set_structInfiltration(double val_in)
{
        m_structInfiltration = val_in;
}

void Ventilation::set_suspFloorType(char val_in) 
{
       m_suspFloorType = val_in;
}

void Ventilation::set_draughtLobbyPresent(bool val_in)
{
        m_draughtLobbyPresent = val_in;
}

void Ventilation::set_draughtLobby(double val_in) 
{
        m_draughtLobbyPresent = val_in;
}

void Ventilation::set_draughtStrippedPctg(int val_in)
{
        m_draughtStrippedPctg = val_in;
}

void Ventilation::set_windowInfiltration(double val_in)
{
        m_windowInfiltration = val_in;
}

void Ventilation::set_infiltrationRate(double val_in) 
{
        m_infiltrationRate = val_in;
}

void Ventilation::set_numShelteredSides(int val_in)
{
        m_numShelteredSides = val_in;
}

void Ventilation::set_adjInfiltrationRate(double val_in)
{
        m_adjInfiltrationRate = val_in;
}

void Ventilation::set_effecAirChangeRate(double val_in) 
{
        m_effecAirChangeRate = val_in;
}

void Ventilation::set_airChangeType(int val_in) 
{
        m_airChangeType = val_in;
}


//Accessors

int Ventilation::get_numChimneys()
{
    return m_numChimneys; 
}

int Ventilation::get_numOpenFlues() 
{ 
    return m_numOpenFlues; 
}

int Ventilation::get_numPasivFanVents() 
{ 
    return m_numPasivFanVents; 
}

int Ventilation::get_numGasFluelessFires() 
{ 
    return m_numGasFluelessFires; 
}

double Ventilation::get_cvfInfiltration() 
{ 
    return m_cvfInfiltration; 
}

double Ventilation::get_numAirChanges() 
{ 
    return m_numAirChanges; 
}

bool Ventilation::get_pressureTest() 
{ 
    return m_pressureTest; 
}

int Ventilation::get_numStoreys() 
{ 
    return m_numStoreys; 
}

double Ventilation::get_addtnInfiltration()
{ 
    return m_addtnInfiltration; 
}

int Ventilation::get_structInfiltrationType() 
{ 
    return m_structInfiltrationType; 
}

double Ventilation::get_structInfiltration() 
{ 
    return m_structInfiltration;
}

int Ventilation::get_suspFloorType() 
{ 
    return m_suspFloorType; 
}

bool Ventilation::get_draughtLobbyPresent() 
{ 
    return m_draughtLobbyPresent; 
}

double Ventilation::get_draughtLobby() 
{ 
    return m_draughtLobby; 
}

int Ventilation::get_draughtStrippedPctg() 
{ 
    return m_draughtStrippedPctg; 
}

double Ventilation::get_windowInfiltration() 
{ 
    return m_windowInfiltration; 
}

double Ventilation::get_infiltrationRate() 
{
    return m_infiltrationRate; 
}

int Ventilation::get_numShelteredSides() 
{ 
    return m_numShelteredSides; 
}

double Ventilation::get_adjInfiltrationRate() 
{ 
    return m_adjInfiltrationRate; 
}

double Ventilation::get_shelterFactor()
{
    return m_shelterFactor;
}

double Ventilation::get_effecAirChangeRate() 
{ 
    return m_effecAirChangeRate; 
}

int Ventilation::get_airChangeType()
{
    return m_airChangeType;
}

// Other methods
//


void Ventilation::calculateEffecAirChangeRate()
{
    m_cvfInfiltration = ((m_numChimneys*40)+
                        (m_numOpenFlues*20)+
                        (m_numPasivFanVents*10)+
                        (m_numGasFluelessFires*40));

    m_numAirChanges = m_cvfInfiltration / ptrDims->get_dwellingVolume();
    
    m_addtnInfiltration = (m_numStoreys-1)*0.1;

    //Select Either Massonary or Steel Structural Infiltration
    if (m_structInfiltrationType == 1)
        m_structInfiltration = 0.35;
    else
        m_structInfiltration = 0.25;
    
  
    //
    //Setup suspened floor value
    //

    switch (m_suspFloorType)
    {
        case 1:
            //Unsealed
            m_suspFloorValue = 0.2;
            break;
        
        case 2:
            //Sealed
            m_suspFloorValue = 0.1;
            break;
        
        case 0:
            //None
            m_suspFloorValue = 0.0;
            break;

        default:	
            m_suspFloorValue = 0.0;
            break;
    }

    //
    //setup draught lobby values
    //

    if (m_draughtLobbyPresent == 1)
        m_draughtLobby = 0.00;
    else
        m_draughtLobby = 0.05;


  //Calculate Window Infiltration Rate
    m_windowInfiltration = 0.25 - ((0.2*m_draughtStrippedPctg)/100.00);
   
    
     m_infiltrationRate= (m_numAirChanges +
                         m_addtnInfiltration + 
                         m_structInfiltration +
                         m_suspFloorValue +
                         m_draughtLobby +
                         m_windowInfiltration);
    //
    //adjust for pressure change
    //


    if (m_pressureTest == 1)
            m_infiltrationRate = (m_q50Val / 20.00)+m_numAirChanges;
    else
            //no change

    //shelter factor
    m_shelterFactor = 1.00 - (0.075 * m_numShelteredSides);

    //adjust infiltration rate
    m_adjInfiltrationRate = m_infiltrationRate * m_shelterFactor;

    //effective air change rate
    //
    //FIXMe
    
    switch (m_airChangeType)
    {
        case 1:
            m_effecAirChangeRate = m_adjInfiltrationRate + 0.17;
            break;

        case 2:
            m_effecAirChangeRate = m_adjInfiltrationRate + 0.5;
            break;

        case 3:
            if (m_adjInfiltrationRate < 0.25)
                m_effecAirChangeRate = 0.5;
            else
                m_effecAirChangeRate = 0.25 + m_adjInfiltrationRate;
            break;

        case 4:
            if (m_adjInfiltrationRate >= 1.00)
                m_effecAirChangeRate = m_adjInfiltrationRate;
            else
                m_effecAirChangeRate = 0.5 + ((m_adjInfiltrationRate*m_adjInfiltrationRate)
                                               *0.5);
            break;

        default:	
            break;
    }


}

void Ventilation::calcAll()
{
    calculateEffecAirChangeRate();
}

void Ventilation::initVars()
{
  m_numChimneys        = cf.getValueInt ("Ventilation:numChimneys");
  m_numOpenFlues       = cf.getValueInt ("Ventilation:numOpenFlues");
  m_numPasivFanVents   = cf.getValueInt ("Ventilation:numPasivFanVents");
  m_numGasFluelessFires = cf.getValueInt ("Ventilation:numFluelessFires");
  m_cvfInfiltration    = cf.getValueDouble ("Ventilation:cvfInfiltration");
  m_numAirChanges     = 0;
  m_pressureTest      = 0;
  m_numStoreys         = cf.getValueInt ("Ventilation:numStoreys");
  m_addtnInfiltration  = 0;
  m_structInfiltrationType = cf.getValueInt ("Ventilation:structInfiltrationType");
  m_structInfiltration = 0;
  m_suspFloorType      =  cf.getValueInt("Ventilation:suspFloorType");
  m_airChangeType = cf.getValueInt ("Ventilation:airChangeType");
  m_draughtLobbyPresent = 0;
  m_draughtLobby        = 0;
  m_draughtStrippedPctg = cf.getValueDouble ("Ventilation:draughtStrippedPctg");
  m_windowInfiltration = 0;
  m_infiltrationRate = 0;
  m_numShelteredSides  = cf.getValueInt ("Ventilation:numShelteredSides");
  m_adjInfiltrationRate = 0;
  m_effecAirChangeRate  = 0;
}

