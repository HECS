

#include "FuelCosts.h"


FuelCosts::FuelCosts (const ConfigParser& config, 
                            DwellingDimensions *dims,
                             SpaceHeating *sheat,
                             WaterHeatingEnergy *wheat,
                             Gains *gains)
                             
{
    cf = config;
    ptrSpaceHeating = sheat;
    ptrWaterHeatingEnergy = wheat;
    ptrDims = dims;
    ptrGains= gains;
    initVars();
}

FuelCosts::~FuelCosts () { }

void FuelCosts::setSpaceHeatFuelPricePri ( double val_in ) {
  m_spaceHeatFuelPricePri = val_in;
}


double FuelCosts::getSpaceHeatFuelPricePri ( ) {
  return m_spaceHeatFuelPricePri;
}


void FuelCosts::setSpaceHeatFuelCostPri ( double val_in ) {
  m_spaceHeatFuelCostPri = val_in;
}

double FuelCosts::getSpaceHeatFuelCostPri ( ) {
  return m_spaceHeatFuelCostPri;
}

void FuelCosts::setSpaceHeatFuelPriceSec ( double val_in ) {
  m_spaceHeatFuelPriceSec = val_in;
}


double FuelCosts::getSpaceHeatFuelPriceSec ( ) {
  return m_spaceHeatFuelPriceSec;
}


void FuelCosts::setSpaceHeatFuelCostSec ( double val_in ) {
  m_spaceHeatFuelCostSec = val_in;
}


double FuelCosts::getSpaceHeatFuelCostSec ( ) {
  return m_spaceHeatFuelCostSec;
}


void FuelCosts::setWaterHeatOnPeakFrac ( double val_in ) {
  m_waterHeatOnPeakFracCost = val_in;
}

double FuelCosts::getWaterHeatOnPeakFrac ( ) {
  return m_waterHeatOnPeakFracCost;
}

void FuelCosts::setWaterHeatOffPeakFrac ( double val_in ) {
  m_waterHeatOffPeakFracCost = val_in;
}

double FuelCosts::getWaterHeatOffPeakFrac ( ) {
  return m_waterHeatOffPeakFracCost;
}

void FuelCosts::setWaterHeatOnPeakFuelPrice ( double val_in ) {
  m_waterHeatOnPeakFuelPrice = val_in;
}

double FuelCosts::getWaterHeatOnPeakFuelPrice ( ) {
  return m_waterHeatOnPeakFuelPrice;
}

void FuelCosts::setWaterHeatOnPeakCost ( double val_in ) {
  m_waterHeatOnPeakCost = val_in;
}

double FuelCosts::getWaterHeatOnPeakCost ( ) {
  return m_waterHeatOnPeakCost;
}


void FuelCosts::setWaterHeatOffPeakCost ( double val_in ) {
  m_waterHeatOffPeakCost = val_in;
}

double FuelCosts::getWaterHeatOffPeakCost ( ) {
  return m_waterHeatOffPeakCost;
}

void FuelCosts::setWaterHeatOffPeakFuelPrice ( double val_in ) {
  m_waterHeatOffPeakFuelPrice = val_in;
}

double FuelCosts::getWaterHeatOffPeakFuelPrice ( ) {
  return m_waterHeatOffPeakFuelPrice;
}

void FuelCosts::setWaterHeatingOtherFuelPrice ( double val_in ) {
  m_waterHeatingOtherFuelPrice = val_in;
}

double FuelCosts::getWaterHeatingOtherFuelPrice ( ) {
  return m_waterHeatingOtherFuelPrice;
}

void FuelCosts::setWaterHeatingOtherFuelCost ( double val_in ) {
  m_waterHeatingOtherFuelCost = val_in;
}

double FuelCosts::getWaterHeatingOtherFuelCost ( ) {
  return m_waterHeatingOtherFuelCost;
}

void FuelCosts::setPumpFanEnergyPrice ( double val_in ) {
  m_pumpFanEnergyPrice = val_in;
}

double FuelCosts::getPumpFanEnergyPrice ( ) {
  return m_pumpFanEnergyPrice;
}

void FuelCosts::setPumpFanEnergyCost ( double val_in ) {
  m_pumpFanEnergyCost = val_in;
}

double FuelCosts::getPumpFanEnergyCost ( ) {
  return m_pumpFanEnergyCost;
}

/*void FuelCosts::setEnergyLightingReq ( double val_in ) {
  m_EnergyLightingReq = val_in;
}

double FuelCosts::getEnergyLightingReq ( ) {
  return m_EnergyLightingReq;
}*/

void FuelCosts::setEnergyLightingPrice ( double val_in ) {
  m_EnergyLightingPrice = val_in;
}

double FuelCosts::getEnergyLightingPrice ( ) {
  return m_EnergyLightingPrice;
}

void FuelCosts::setEnergyLightingCost ( double val_in ) {
  m_EnergyLightingCost = val_in;
}

double FuelCosts::getEnergyLightingCost ( ) {
  return m_EnergyLightingCost;
}

void FuelCosts::setAddtnStandingCharges ( double val_in ) {
  m_addtnStandingCharges = val_in;
}

double FuelCosts::getAddtnStandingCharges ( ) {
  return m_addtnStandingCharges;
}

void FuelCosts::setREenergyProducedSavedYear ( double val_in ) {
  m_REenergyProducedSavedYear = val_in;
}

double FuelCosts::getREenergyProducedSavedYear ( ) {
  return m_REenergyProducedSavedYear;
}

void FuelCosts::setREenergyProducedPrice ( double val_in ) {
  m_REenergyProducedPrice = val_in;
}

double FuelCosts::getREenergyProducedPrice ( ) {
  return m_REenergyProducedPrice;
}

void FuelCosts::setREenergyProducedCost ( double val_in ) {
  m_REenergyProducedCost = val_in;
}

double FuelCosts::getREenergyProducedCost ( ) {
  return m_REenergyProducedCost;
}

void FuelCosts::setREenergyConsumedTechReq ( double val_in ) {
  m_REenergyConsumedTechReq = val_in;
}

double FuelCosts::getREenergyConsumedTechReq ( ) {
  return m_REenergyConsumedTechReq;
}

void FuelCosts::setREcostConsumedPrice ( double val_in ) {
  m_REcostConsumedPrice = val_in;
}

double FuelCosts::getREcostConsumedPrice ( ) {
  return m_REcostConsumedPrice;
}

void FuelCosts::setREcostConsumedCost ( double val_in ) {
  m_REcostConsumedCost = val_in;
}

int FuelCosts::getFuelType ( ) {
  return m_fuelType;
}

void FuelCosts::setFuelType ( int val_in ) {
  m_fuelType = val_in;
}

double FuelCosts::getREcostConsumedCost ( ) {
  return m_REcostConsumedCost;
}

void FuelCosts::setTotalEnergyCost ( double val_in ) {
  m_totalEnergyCost = val_in;
}

double FuelCosts::getTotalEnergyCost ( ) {
  return m_totalEnergyCost;
}

void FuelCosts::setSAPenergyCostDeflator  (double val_in ) {
  m_SAPenergyCostDeflator = val_in;
}

double FuelCosts::getSAPenergyCostDeflator ( ) {
  return m_SAPenergyCostDeflator;
}

void FuelCosts::setSAPEnergyCostFactor ( double val_in ) {
  m_SAPEnergyCostFactor = val_in;
}

double FuelCosts::getSAPEnergyCostFactor ( ) {
  return m_SAPEnergyCostFactor;
}

void FuelCosts::setSAPrating ( double val_in ) {
  m_SAPrating = val_in;
}

double FuelCosts::getSAPrating ( ) {
  return m_SAPrating;
}


QMap <int, QList <double> > FuelCosts::getTable12()
{
    return table12;
}

void FuelCosts::initVars()
{
    buildLT();

  m_spaceHeatFuelPricePri = cf.getValueDouble("FuelCosts:spaceHeatFuelPricePri");
  m_spaceHeatFuelCostPri = cf.getValueDouble("FuelCosts:spaceHeatFuelCostPri");

  m_spaceHeatFuelPriceSec = cf.getValueDouble("FuelCosts:spaceHeatFuelPriceSec");
  m_spaceHeatFuelCostSec = cf.getValueDouble("FuelCosts:spaceHeatFuelCostSec");
  m_waterHeatOnPeakFracCost = cf.getValueDouble("FuelCosts:waterHeatOnPeakFracCost");
  m_waterHeatOnPeakFuelPrice = cf.getValueDouble("FuelCosts:waterHeatOnPeakFuelPrice");
  m_waterHeatOnPeakCost = cf.getValueDouble("FuelCosts:waterHeatOnPeakCost");
  m_waterHeatOnPeakFracCost = cf.getValueDouble("FuelCosts:waterHeatOnPeakFracCost");
  m_waterHeatOffPeakCost = cf.getValueDouble("FuelCosts:waterHeatOffPeakCost");
  m_waterHeatingOtherFuelPrice = cf.getValueDouble("FuelCosts:waterHeatingOtherFuelPrice");
  m_waterHeatingOtherFuelCost = cf.getValueDouble("FuelCosts:waterHeatingOtherFuelCost");
  m_pumpFanEnergyPrice = cf.getValueDouble("FuelCosts:pumpFanEnergyPrice");
  m_pumpFanEnergyCost = cf.getValueDouble("FuelCosts:pumpFanEnergyCost");
  //m_EnergyLightingReq = cf.getValueDouble("FuelCosts:EnergyLightingReq");
  m_EnergyLightingPrice = cf.getValueDouble("FuelCosts:EnergyLightingPrice");
  m_EnergyLightingCost = cf.getValueDouble("FuelCosts:EnergyLightingCost");
  m_addtnStandingCharges = cf.getValueDouble("FuelCosts:addtnStandingCharges");
  m_REenergyProducedSavedYear = cf.getValueDouble("FuelCosts:REenergyProducedSavedYear");
  m_REenergyProducedPrice = cf.getValueDouble("FuelCosts:REenergyProducedPrice");
  m_REenergyProducedCost = cf.getValueDouble("FuelCosts:REenergyProducedCost");
  m_REenergyConsumedTechReq = cf.getValueDouble("FuelCosts:REenergyConsumedTechReq");
  m_REcostConsumedPrice = cf.getValueDouble("FuelCosts:REcostConsumedPrice");
  m_REcostConsumedCost = cf.getValueDouble("FuelCosts:REcostConsumedCost");
  m_totalEnergyCost = cf.getValueDouble("FuelCosts:totalEnergyCost");
  m_SAPenergyCostDeflator = 0.91;
  m_SAPEnergyCostFactor = cf.getValueDouble("FuelCosts:SAPEnergyCostFactor");
  m_SAPrating = cf.getValueDouble("FuelCosts:SAPrating");
  m_fuelType = cf.getValueDouble("FuelCosts:fuelType");
}

void FuelCosts::calcAll()
{

    calcTotalFuelCost();
    calcSapRating();
}

void FuelCosts::buildLT()
{
    TableReader tr ("table12");
    table12 = tr.getTableMap();
    tr.readFile("table13");
    table13 = tr.getTableMap();
}


void FuelCosts::calcTotalFuelCost()
{
    m_spaceHeatFuelPricePri = table12.value(m_fuelType).at(1);
    m_spaceHeatFuelPriceSec = table12.value(m_fuelType).at(1);

    m_spaceHeatFuelCostPri = ptrSpaceHeating->getSpaceHeatingFuelMainReq() *
                                              0.01 *
                                              m_spaceHeatFuelPricePri;


    m_spaceHeatFuelCostSec= ptrSpaceHeating->getSpaceHeatingFuelSecondary() *
                                             0.01 *
                                             m_spaceHeatFuelPriceSec;

    //Lookup Table13 Value
    //m_waterHeatOnPeakFracCost = table12.value(m_fuelType).at(1);
    m_addtnStandingCharges  = table12.value(m_fuelType).at(0);


    m_waterHeatOffPeakFracCost= 1.00 - m_waterHeatOnPeakFracCost;
    

    m_waterHeatOnPeakCost = ptrWaterHeatingEnergy->getEnergyReqdForWaterHeat() *
                            m_waterHeatOffPeakFracCost *
                            0.01 *
                            m_waterHeatOnPeakFuelPrice;

    m_waterHeatOffPeakCost = ptrWaterHeatingEnergy->getEnergyReqdForWaterHeat() *
                             0.01 *
                             m_waterHeatOnPeakFuelPrice;

    m_waterHeatingOtherFuelPrice = table12.value(m_fuelType).at(1);
    m_waterHeatingOtherFuelCost = 
                        ptrWaterHeatingEnergy->getEnergyReqdForWaterHeat() *
                                  0.01 *
                                  m_waterHeatingOtherFuelPrice;
    m_pumpFanEnergyCost = ptrWaterHeatingEnergy->getTotalForAboveElec() * m_pumpFanEnergyCost;

    m_EnergyLightingPrice = table12.value(13).at(1);
    m_EnergyLightingCost = ptrGains->getAnnualLightEnergy() * m_EnergyLightingPrice * 0.01;
     
    
    //Renewable Stuff to go in here...


    m_totalEnergyCost =   m_spaceHeatFuelCostPri +
                          m_spaceHeatFuelPriceSec +
                          m_waterHeatOnPeakCost +
                          m_waterHeatOffPeakCost +
                          m_waterHeatingOtherFuelCost +
                          m_pumpFanEnergyCost +
                          m_EnergyLightingCost +
                          m_addtnStandingCharges -
                          m_REenergyProducedCost +
                          m_REcostConsumedCost;
}


void FuelCosts::calcSapRating()
{
    m_SAPEnergyCostFactor = ((m_totalEnergyCost * 
                             m_SAPenergyCostDeflator) - 30.00) /
                            (ptrDims->get_totalFloorArea() + 45.00);
                             
    if (m_SAPEnergyCostFactor >= 3.5)
        m_SAPrating = 111 - 110 * log10(m_SAPEnergyCostFactor);
    else
        m_SAPrating = 100 - 13.96 * m_SAPEnergyCostFactor;
}


