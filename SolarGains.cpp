/***************************************************************************
 *                                                                         *
 *  SolarGains.cpp Copyright (C) 2008 by Jon Rumble                        *
 *  j.w.rumble@reading.ac.uk   				                   *
 *                                                                         *
 *  This file is part of HECS,                                             *
 *                                                                         *
 *  HECS is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation, either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *  HECS is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/

#include "SolarGains.h"

// Constructors/Destructors
//  

SolarGains::SolarGains(const int overshadow,
                       const int orientation,
                       const double area,
                       const int frameType,
                       const int glazingType,
                       const char season
                       )

{
  //Zero Everything First
  initVars();
  m_overshadow = overshadow,
  m_orientation = orientation,
  m_area = area ;
  m_frameType = frameType;
  m_glazingType = glazingType;
  m_season = season;
  calcSolarGains();
}

SolarGains::~SolarGains ( ) { }

//  
// Methods
//  


void SolarGains::calcSolarGains()
{

    //Set Access Factor
    switch (m_season)
    {
        case 'w' :
            if (m_overshadow == 1)
                        m_accessFactor = 0.3;
            else if (m_overshadow == 2)
                        m_accessFactor = 0.54;
            else if (m_overshadow == 3)
                        m_accessFactor = 0.77;
            else if (m_overshadow == 4)
                        m_accessFactor = 1.00;
            //Default
            else 
                        m_accessFactor = 1.00;
            break;

        case 's' :	
            if      (m_overshadow == 1)
                        m_accessFactor = 0.5;
            else if (m_overshadow == 2)
                        m_accessFactor = 0.7;
            else if (m_overshadow == 3)
                        m_accessFactor = 0.9;
            else if (m_overshadow == 4)
                        m_accessFactor = 1.00;
            //Default
            else 
                        m_accessFactor = 1.00;
            break;

        default:	
            break;
    }				/* -----  end switch  ----- */
    

    // Set Solar Flux
    //

    if (m_season == 's')
    {
        switch (m_orientation)
        {
            case 1:	
                // North
                m_solarFlux = 75;
                break;

            case 2:	
                // NE/NW
                m_solarFlux = 89;
                break;

            case 3:
                // E/W
                m_solarFlux = 109;
                break;

            case 4:	
                // SE/SW
                m_solarFlux = 112;
                break;

            case 5:	
                // South
                m_solarFlux = 107;
                break;


            default:	
                break;
        }				/* -----  end switch  ----- */
    }

    else  //Default To Heating Season
    {
        //Heating Season
        switch (m_orientation)
        {
            case 1:	
                // North
                m_solarFlux = 29;
                break;

            case 2:	
                // NE/NW
                m_solarFlux = 34;
                break;

            case 3:
                // E/W
                m_solarFlux = 48;
                break;

            case 4:	
                // SE/SW
                m_solarFlux = 64;
                break;

            case 5:	
                // South 
                m_solarFlux = 72;
                break;


            default:	
                break;
        }				/* -----  end switch  ----- */
    }

    // Total Solar energy transmittance
    //
    
    switch (m_glazingType)
    {
        case 1:
            //Single Glazed
            m_glazTransmitanceFactor = 0.85;
            break;
        case 2:
            //Double Glazed (air or argon filled)
            m_glazTransmitanceFactor = 0.76;
            break;
        case 3:
            //Double Glazed (Low-E, hard-coat)
            m_glazTransmitanceFactor = 0.72;
            break;
        case 4:
            //Double Glazed (Low-E, soft-coat)
            m_glazTransmitanceFactor = 0.63;
            break;
        case 5:
            //Triple Glazed (air or argon filled)
            m_glazTransmitanceFactor = 0.68;
            break;
        case 6:
            //Triple Glazed (Low-E, hard-coat)
            m_glazTransmitanceFactor = 0.64;
            break;
        case 7:
            //Triple Glazed (Low-E, soft-coat)
            m_glazTransmitanceFactor = 0.57;
            break;
        default:
            break;
    }

    //Set Frame Factor    
    //

    switch (m_frameType)
    {
        case 1:	
            //Wood
            m_frameFactor = 0.7;
            break;

        case 2:	
            //Metal
            m_frameFactor = 0.8;
            break;

        case 3:
            //Metal, thermal Break
            m_frameFactor = 0.8;
            break;

        case 4:	
            //PVC-U
            m_frameFactor = 0.7;
            break;

        default:	
            break;
    }				/* -----  end switch  ----- */
    
    m_solarGains = m_accessFactor * 
                   m_area *
                   m_solarFlux *
                   0.9 *
                   m_glazTransmitanceFactor *
                   m_frameFactor ;
}

// Accessor methods
//  



void SolarGains::setAccessFactor ( double var_in ) {
  m_accessFactor = var_in;
}


double SolarGains::getAccessFactor ( ) {
  return m_accessFactor;
}

void SolarGains::setArea ( double var_in ) {
  m_area = var_in;
}

double SolarGains::getArea ( ) {
  return m_area;
}

void SolarGains::setSolarFlux ( int var_in ) {
  m_solarFlux = var_in;
}

int SolarGains::getSolarFlux ( ) {
  return m_solarFlux;
}

void SolarGains::setGlazTransmitanceFactor ( double var_in) {
  m_glazTransmitanceFactor = var_in;
}

double SolarGains::getGlazTransmitanceFactor ( ) {
  return m_glazTransmitanceFactor;
}

void SolarGains::setFrameFactor ( double var_in ) {
  m_frameFactor = var_in;
}

double SolarGains::getFrameFactor ( ) {
  return m_frameFactor;
}

void SolarGains::setSolarGains ( double var_in ) {
  m_solarGains = var_in;
}

double SolarGains::getSolarGains ( ) {
  return m_solarGains;
}

// Other methods
//  


void SolarGains::initVars ( ) {

      m_accessFactor = 0.00;
      m_area = 0.00;
      m_solarFlux = 0;
      m_glazTransmitanceFactor = 0;
      m_frameFactor = 0;
      m_solarGains = 0;
      m_season = 'w';
      m_frameType = 0;
      m_overshadow = 0;
      m_glazingType = 0;
      m_orientation = 0;
}

