/***************************************************************************
 *                                                                         * 
 *  SummaryDialog.cpp Copyright (C) 2008 by Jon Rumble                        *  
 *  j.w.rumble@reading.ac.uk   				                   *
 *                                                                         *
 *                                                                         * 
 *  This file is part of HECS,                                             *
 *                                                                         *
 *  HECS is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation, either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *                                                                         *
 *  HECS is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/

#include "SummaryDialog.h"

SummaryDialog::SummaryDialog (QWidget *parent)
    : QDialog(parent)
{
    ui.setupUi(this);
}

void SummaryDialog::setModel(RdSap *modelIn)
{
    sapModel = modelIn;
    initVars();
}



void SummaryDialog::initVars()
{

sapModel->calcModel();
ui.SAPRatingLbl->setText(QString::number(sapModel->fuelcosts->getSAPrating(),'f',0));
ui.annualEnergyLbl->setText(QString::number(sapModel->fuelcosts->getTotalEnergyCost(),'f',2));
//    QObject::connect(ui.firstFloorAreaEdit, SIGNAL(textChanged(QString)), this,     SLOT(onFirstFloorAreaChanged()));

   //Populate Adress Details
    ui.houseNumLbl->setText(sapModel->dwelling->get_houseNumber());
    ui.addressLbl->setText(sapModel->dwelling->get_address());
    ui.postcodeLbl->setText(sapModel->dwelling->get_postcode());

}

// END OF SummaryDialog.cpp
