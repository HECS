/***************************************************************************
 *                                                                         *
 *  Boiler.h Copyright (C) 2008 by Jon Rumble                              *
 *  j.w.rumble@reading.ac.uk   				                   *
 *                                                                         *
 *  This file is part of HECS,                                             *
 *                                                                         *
 *  HECS is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation, either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *  HECS is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/


#include <QString>

class Boiler {

public:

Boiler (int productIndexNo, 
        QString brandName,
        QString modelName,
        QString modelQualifier,
        double sapSeasonalEffcy,
        int fuelType);

Boiler ();
~Boiler ();




    //Accessors
    
    int  get_productIndexNo();
    QString  get_brandName ();
    QString  get_modelName ();
    QString  get_modelQualifier ();
    double get_sapSeasonalEffcy ();
    int      get_fuelType ();


    //void set_productIndexNo();
	 
protected:

    int m_productIndexNo;
    QString m_brandName;
    QString m_modelName;
    QString m_modelQualifier;
    double m_sapSeasonalEffcy;
    int m_fuelType;


private:


};

