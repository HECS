/***************************************************************************
 *                                                                         *
 *  Ventilation.h Copyright (C) 2008 by Jon Rumble                         *
 *  j.w.rumble@reading.ac.uk   				                   *
 *                                                                         *
 *  This file is part of HECS,                                             *
 *                                                                         *
 *  HECS is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation, either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *  HECS is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/


#ifndef VENTILATION_H
#define VENTILATION_H


#include <QMap>
#include "ConfigParser.h"
#include "DwellingDimensions.h"
#include <iostream>
class Ventilation
  {

  public:

    // Constructors/Destructors
    //


    /**
     * CONSTRUCTORS
     */
    Ventilation(const ConfigParser& config, DwellingDimensions* dims);
   ~Ventilation();


    /**
     * METHODS
     */

    void clearAll();
    void initVars();

    void set_numChimneys(int val_in);
    void set_numOpenFlues(int val_in);
    void set_numPasivFanVents(int val_in);
    void set_numGasFluelessFires(int val_in);
    void set_cvfInfiltration(int val_in);
    void set_numAirChanges(double val_in);
    void set_pressureTest(bool val_in);
    void set_m_q50Val(double val_in);
    void set_numStoreys(int val_in);
    void set_addtnInfiltration(double val_in);
    void set_structInfiltrationType(int val_in);
    void set_structInfiltration(double val_in);
    void set_suspFloorType(char val_in);
    void set_draughtLobbyPresent(bool val_in);
    void set_draughtLobby(double val_in);
    void set_draughtStrippedPctg(int val_in);
    void set_windowInfiltration(double val_in);
    void set_infiltrationRate(double val_in);
    void set_numShelteredSides(int val_in);
    void set_adjInfiltrationRate(double val_in);
    void set_effecAirChangeRate(double val_in);
    void set_airChangeType(int val_in);

    int get_numChimneys();
    int get_numOpenFlues(); 
    int get_numPasivFanVents(); 
    int get_numGasFluelessFires(); 
    double get_cvfInfiltration(); 
    double get_numAirChanges(); 
    bool get_pressureTest(); 
    int get_numStoreys(); 
    double get_addtnInfiltration(); 
    int get_structInfiltrationType(); 
    double get_structInfiltration(); 
    int get_suspFloorType(); 
    bool get_draughtLobbyPresent(); 
    double get_draughtLobby(); 
    int get_draughtStrippedPctg(); 
    double get_windowInfiltration(); 
    double get_infiltrationRate(); 
    int get_numShelteredSides(); 
    double get_shelterFactor();
    int get_airChangeType();
    double get_adjInfiltrationRate();
    double get_effecAirChangeRate(); 
    void calcAll();

  private:

    // Private attributes
    //

    ConfigParser cf;
    DwellingDimensions *ptrDims;

    //Constants
    static const double STEEL_TIMBER_FRAME = 0.25;
    static const double MASSONARY_FRAME = 0.35;
    static const double SUSP_FLOOR_SEALED = 0.1;
    static const double SUSP_FLOOR_UNSEALED = 0.2;
    static const double SUSP_FLOOR_NONE= 0.0;
    static const double DRAUGHT_LOBBY_PRESENT = 0.1;
    static const double DRAUGHT_LOBBY_MISSING = 0.0;
    static const int DRAUGHT_STRIPPED_NEW_BUILD = 100;

    //Number Of Chimneys
    int m_numChimneys;
    //Number Of Open Flues
    int m_numOpenFlues;
    //Number Of Intermitent Fans Or Passive Vents
    int m_numPasivFanVents;
    //Number Of Flueless Gas Fires
    int m_numGasFluelessFires;
    //Infiltration Due To Chimneys,Flues and fans
    double m_cvfInfiltration;
    //Number of Air Changes Per Hour
    double m_numAirChanges;
    //Pressure Test Carried out ? YES (1) NO (0)
    bool m_pressureTest;
    //If yes, Q50 here 
    double m_q50Val;
    //Number Of Storeys In The Dwelling 
    int m_numStoreys;
    //Additional Infiltration
    double m_addtnInfiltration;
    //Structural Infiltration Type ? STEEL/TIMBER (s) MASSONARY (m)
    int m_structInfiltrationType;
    //Holds Calculated Structual Infiltration Value
    double m_structInfiltration;
    //Wooden Suspended Floor Type SEALED (s) UN-SEALED (u) NONE (n)
    int m_suspFloorType;
    //Suspended Floor Value
    double m_suspFloorValue;
    //Draught Lobby Present ? YES (1) NO (0)
    bool m_draughtLobbyPresent;
    //Draught Lobby Value
    double m_draughtLobby;
    //Percentage of Windows / Doors Draught Stripped
    double m_draughtStrippedPctg;
    //Window Infiltration
    double m_windowInfiltration;
    //Infiltration Rate
    double m_infiltrationRate;
    //Number Of Sheltered Sides Default 2
    int m_numShelteredSides;
    //Holds Shelter Factor
    double m_shelterFactor;
    //Adjusted Infiltration Rate
    double m_adjInfiltrationRate;
    //Air Change Type 1, 2, 3 or 4 
    int m_airChangeType;
    //Effective Air Change Rate
    double m_effecAirChangeRate;


    void calculateEffecAirChangeRate();
  };

#endif // VENTILATION_H
