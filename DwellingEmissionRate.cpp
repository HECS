/***************************************************************************
 *                                                                         * 
 *  DwellingEmissionRate.cpp Copyright (C) 2008 by Jon Rumble              *
 *  j.w.rumble@reading.ac.uk   				                   *
 *                                                                         *
 *  This file is part of HECS,                                             *                   
 *                                                                         * 
 *  HECS is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation, either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *  HECS is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/

#include "DwellingEmissionRate.h"
// Constructors/Destructors
//  

DwellingEmissionRate::DwellingEmissionRate() {}

DwellingEmissionRate::DwellingEmissionRate (const ConfigParser& config,
                                            DwellingDimensions *dims, 
                                            SpaceHeating *spaceheat, 
                                            WaterHeatingEnergy *waterheat,
                                            Gains *gains,
                                            FuelCosts *fcosts) 
{

    cf = config;
    ptrDims = dims;
    ptrSpaceHeat = spaceheat;
    ptrWaterHeatEnergy = waterheat;
    ptrGains = gains;
    ptrFuelCosts = fcosts;
    initVars();
}

DwellingEmissionRate::~DwellingEmissionRate ( ) { }

//  
// Methods
//  


// Accessor methods
//  
    void DwellingEmissionRate::setSpaceHeatingEnergy ( double val_in ) {
  m_spaceHeatingEnergy = val_in;
}


double DwellingEmissionRate::getSpaceHeatingEnergy ( ) {
  return m_spaceHeatingEnergy;
}


void DwellingEmissionRate::setSpaceHeatingEmisFactor ( double val_in ) {
  m_spaceHeatingEmisFactor = val_in;
}


double DwellingEmissionRate::getSpaceHeatingEmisFactor ( ) {
  return m_spaceHeatingEmisFactor;
}


void DwellingEmissionRate::setSpaceHeatingEmissionsYear ( double val_in ) {
  m_spaceHeatingEmissionsYear = val_in;
}


double DwellingEmissionRate::getSpaceHeatingEmissionsYear ( ) {
  return m_spaceHeatingEmissionsYear;
}


void DwellingEmissionRate::setSecondarySpaceHeating ( double val_in ) {
  m_secondarySpaceHeating = val_in;
}


double DwellingEmissionRate::getSecondarySpaceHeating ( ) {
  return m_secondarySpaceHeating;
}


void DwellingEmissionRate::setSecondarySpaceHeatEmisFactor ( double val_in ) {
  m_secondarySpaceHeatEmisFactor = val_in;
}


double DwellingEmissionRate::getSecondarySpaceHeatEmisFactor ( ) {
  return m_secondarySpaceHeatEmisFactor;
}


void DwellingEmissionRate::setSecondarySpaceHeatingEmissionsYear ( double val_in ) {
  m_secondarySpaceHeatingEmissionsYear = val_in;
}


double DwellingEmissionRate::getSecondarySpaceHeatingEmissionsYear ( ) {
  return m_secondarySpaceHeatingEmissionsYear;
}


void DwellingEmissionRate::setEnergyForWaterHeating ( double val_in ) {
  m_energyForWaterHeating = val_in;
}


double DwellingEmissionRate::getEnergyForWaterHeating ( ) {
  return m_energyForWaterHeating;
}


void DwellingEmissionRate::setWaterHeatingEmissionsFactor ( double val_in ) {
  m_waterHeatingEmissionsFactor = val_in;
}


double DwellingEmissionRate::getWaterHeatingEmissionsFactor ( ) {
  return m_waterHeatingEmissionsFactor;
}


void DwellingEmissionRate::setWaterHeatingEmissionsYear ( double val_in ) {
  m_waterHeatingEmissionsYear = val_in;
}


double DwellingEmissionRate::getWaterHeatingEmissionsYear ( ) {
  return m_waterHeatingEmissionsYear;
}


void DwellingEmissionRate::setSpaceWaterHeating ( double val_in ) {
  m_spaceWaterHeating = val_in;
}


double DwellingEmissionRate::getSpaceWaterHeating ( ) {
  return m_spaceWaterHeating;
}


void DwellingEmissionRate::setElecForPumpsFansEmisFactor ( double val_in ) {
  m_elecForPumpsFansEmisFactor = val_in;
}


double DwellingEmissionRate::getElecForPumpsFansEmisFactor ( ) {
  return m_elecForPumpsFansEmisFactor;
}


void DwellingEmissionRate::setElecForPumpsFansEmissionsYear ( double val_in ) {
  m_elecForPumpsFansEmissionsYear = val_in;
}


double DwellingEmissionRate::getElecForPumpsFansEmissionsYear ( ) {
  return m_elecForPumpsFansEmissionsYear;
}


void DwellingEmissionRate::setEnergyProdOrSavedFactor ( double val_in ) {
  m_energyProdOrSavedFactor = val_in;
}


double DwellingEmissionRate::getEnergyProdOrSavedFactor ( ) {
  return m_energyProdOrSavedFactor;
}


void DwellingEmissionRate::setEnergyConsumed ( double val_in ) {
  m_energyConsumedYear = val_in;
}


double DwellingEmissionRate::getEnergyConsumed ( ) {
  return m_energyConsumedYear;
}


void DwellingEmissionRate::setTotalCO2KgYear ( double val_in ) {
  m_totalCO2KgYear = val_in;
}


double DwellingEmissionRate::getTotalCO2KgYear ( ) {
  return m_totalCO2KgYear;
}


void DwellingEmissionRate::setDwelCO2EmmRate ( double val_in ) {
  m_dwelCO2EmmRate = val_in;
}


double DwellingEmissionRate::getDwelCO2EmmRate ( ) {
  return m_dwelCO2EmmRate;
}


void DwellingEmissionRate::setLightingEmisFactor ( double val_in ) {
  m_lightingEmisFactor = val_in;
}

double DwellingEmissionRate::getLightingEmisFactor ( ) {
  return m_lightingEmisFactor;
}


void DwellingEmissionRate::setLightingEmissionsYear ( double val_in ) {
  m_lightingEmissionsYear = val_in;
}


double DwellingEmissionRate::getLightingEmissionsYear ( ) {
  return m_lightingEmissionsYear;
}


void DwellingEmissionRate::setSpaceAndWaterHeating ( double val_in ) {
  m_spaceAndWaterHeating = val_in;
}


double DwellingEmissionRate::getSpaceAndWaterHeating ( ) {
  return m_spaceAndWaterHeating;
}


void DwellingEmissionRate::setEnergyProdOrSavedEmissionsYear ( double val_in ) {
  m_energyProdOrSavedEmissionsYear = val_in;
}


double DwellingEmissionRate::getEnergyProdOrSavedEmissionsYear ( ) {
  return m_energyProdOrSavedEmissionsYear;
}


void DwellingEmissionRate::setEnergyConsumeTechnologyEmisFactor ( double val_in ) {
  m_energyConsumeTechnologyEmisFactor = val_in;
}


double DwellingEmissionRate::getEnergyConsumeTechnologyEmisFactor ( ) {
  return m_energyConsumeTechnologyEmisFactor;
}

void DwellingEmissionRate::setEIrating ( double val_in ) {
  m_EIrating = val_in;
}

double DwellingEmissionRate::getEIrating ( ) {
  return m_EIrating;
}

void DwellingEmissionRate::calcEIRating()
{
    double CF = 0.00;

    CF = (m_totalCO2KgYear)/(ptrDims->get_totalFloorArea()+45.00);

    if (CF >= 28.3)
    {
        m_EIrating = 200 - 95 * log10(CF);
    }
    else
    {
        m_EIrating = 100 - 1.34 * CF;
    }
}

// Other methods
//  


void DwellingEmissionRate::calcDwellingCO2EmisRate()
{
   //Make a local Copy of Table12 for use here
   QMap <int, QList <double> > emisList = ptrFuelCosts->getTable12();
  
   //Fuel For Main Heating System
   int fuelType = ptrFuelCosts->getFuelType();

   m_spaceHeatingEmisFactor = emisList.value(fuelType).at(2);

   m_spaceHeatingEmissionsYear = m_spaceHeatingEmisFactor *
                                  ptrSpaceHeat->getSpaceHeatingFuelMainReq();


   //m_secondarySpaceHeatingEmissionsYear = m_secondarySpaceHeatEmisFactor *
     //                         ptrSpaceHeat->getSpaceHeatingFuelSecondary();

   m_waterHeatingEmissionsFactor = emisList.value(fuelType).at(2);

   m_waterHeatingEmissionsYear = m_waterHeatingEmissionsFactor *
                                 ptrWaterHeatEnergy->getEnergyReqdForWaterHeat();
   
   m_spaceAndWaterHeating = m_spaceHeatingEmissionsYear +
                            m_secondarySpaceHeatingEmissionsYear +
                            m_waterHeatingEmissionsYear;

   m_elecForPumpsFansEmissionsYear = m_elecForPumpsFansEmisFactor *
                                     ptrWaterHeatEnergy->getTotalForAboveElec();
   

   m_lightingEmisFactor = emisList.value(13).at(2);
  

   m_lightingEmissionsYear = 
                             m_lightingEmisFactor * 
                             //Quick Hack divide by 0.15 to inverse val in gains
                             (ptrGains->getLowEnergyLightRed() / 0.15);
    
   m_energyProdOrSavedEmissionsYear = m_energyProdOrSavedFactor *
                                      ptrFuelCosts->getREenergyProducedSavedYear();
                                      
   //Assuming Electricity Here ?
   m_energyConsumeTechnologyEmisFactor = emisList.value(13).at(2);

   m_energyConsumedYear = m_energyConsumeTechnologyEmisFactor *
                      ptrFuelCosts->getREenergyConsumedTechReq();

   m_totalCO2KgYear = m_spaceAndWaterHeating +
                      m_elecForPumpsFansEmissionsYear +
                      m_lightingEmissionsYear -
                      m_energyProdOrSavedEmissionsYear +
                      m_energyConsumedYear;

   m_dwelCO2EmmRate = m_totalCO2KgYear / ptrDims->get_dwellingVolume();
}

void DwellingEmissionRate::calcAll()
{
    calcDwellingCO2EmisRate();    
    calcEIRating();
}

void DwellingEmissionRate::initVars ( ) {

      m_spaceHeatingEnergy = cf.getValueDouble("DER:spaceHeatingEnergy");
      m_spaceHeatingEmisFactor = cf.getValueDouble("DER:spaceHeatingEmisFactor");
      m_spaceHeatingEmissionsYear = cf.getValueDouble("DER:spaceHeatingEmisFactor");
      m_secondarySpaceHeating = cf.getValueDouble("DER:secondarySpaceHeating");
      m_secondarySpaceHeatEmisFactor = cf.getValueDouble("DER:secondarySpaceHeatEmisFactor");
      m_secondarySpaceHeatingEmissionsYear = cf.getValueDouble("DER:secondarySpaceHeatingEmissionsYear");
      m_energyForWaterHeating = cf.getValueDouble("DER:energyForWaterHeating");
      m_waterHeatingEmissionsFactor = cf.getValueDouble("DER:waterHeatingEmissionsFactor");
      m_waterHeatingEmissionsYear = cf.getValueDouble("DER:waterHeatingEmissionsYear");
      m_spaceWaterHeating = cf.getValueDouble("DER:spaceWaterHeating");
      m_elecForPumpsFansEmisFactor = cf.getValueDouble("DER:elecForPumpsFansEmisFactor");
      m_elecForPumpsFansEmissionsYear = cf.getValueDouble("DER:elecForPumpsFansEmissionsFactor");
      m_energyProdOrSavedFactor = cf.getValueDouble("DER:energyProdOrSavedFactor");
      m_energyConsumedYear = cf.getValueDouble("DER:energyConsumedYear");
      m_totalCO2KgYear = cf.getValueDouble("DER:totalCO2KgYear");
      m_dwelCO2EmmRate = cf.getValueDouble("DER:dwelCO2EmmRate");
      m_lightingEmisFactor = cf.getValueDouble("DER:lightingEmisFactor");
      m_lightingEmissionsYear = cf.getValueDouble("DER:lightingEmissionsYear");
      m_spaceAndWaterHeating = cf.getValueDouble("DER:spaceAndWaterHeating");
      m_energyProdOrSavedEmissionsYear = cf.getValueDouble("DER:energyProdOrSavedEmissionsYear");
      m_energyConsumeTechnologyEmisFactor = cf.getValueDouble("DER:energyConsumeTechnologyEmisFactor");
      m_EIrating = cf.getValueDouble("DER:EIrating");


}       
