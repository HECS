/***************************************************************************
 *                                                                         *
 *  Gains.cpp Copyright (C) 2008 by Jon Rumble                             *
 *  j.w.rumble@reading.ac.uk   				                   *
 *                                                                         *
 *  This file is part of HECS,                                             *
 *                                                                         *
 *  HECS is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation, either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *  HECS is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/

#include "Misc.h"

double kWHtoGigaJoules(double val_in)
{
    double val_out = 0.00;

    return val_out = (val_in * 3600000) / 1000000000;


}

double lerp(const double& x1, 
                  double& x2, 
                  double& x3, 
                  double& y1, 
                  double& y3)
{
    double y2 = 0.00;
    
    y2= y1 + (x2 - x1) *(y3 - y1)/(x3 - x1);
    return y2;
}

double calcWk(double area, double uValue)
{
    double wK = 0.00;
    return wK = area * uValue;
}

double sqFeettoSquareMetres(double sqFeetIn)
{
    return sqFeetIn / 10.7639104;
}

/*QString sedbukGrade(int sedbukVal)
{
    QString rating;

    if (sedbuk >= 90)
        return rating = "A";

    if (sedbuk >= 86 && < 90 )
        return rating = "A";

    if (sedbuk >= 90)
        return rating = "A";

    if (sedbuk >= 90)
        return rating = "A";

    if (sedbuk >= 90)
        return rating = "A";

}*/

//NB QStringList must be sorted or it won't work !!!
QStringList pruneDuplicates(QStringList strListIn)
{

    QStringList prunedList;
    QString curEntry,lastEntry = "";
    for (int i = 0; i < strListIn.size(); ++i)
    {
        curEntry = strListIn.at(i);
        
        if (curEntry == lastEntry)
            continue;
        else
            prunedList << curEntry;

        lastEntry = curEntry;   
    }

    return prunedList;
}
