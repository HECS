/***************************************************************************
 *                                                                         *
 *  DwellingEmissionRate.h Copyright (C) 2008 by Jon Rumble                *
 *  j.w.rumble@reading.ac.uk   				                   *
 *                                                                         *
 *  This file is part of HECS,                                             *
 *                                                                         *
 *  HECS is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation, either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *  HECS is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/

#ifndef DWELLINGEMISSIONRATE_H
#define DWELLINGEMISSIONRATE_H

#include "DwellingDimensions.h"
#include "ConfigParser.h"
#include "WaterHeatingEnergy.h"
#include "SpaceHeating.h"
#include "FuelCosts.h"
#include "Gains.h"

#include <QMap>
#include <QList>

class DwellingEmissionRate
  {

  public:

    // Constructors/Destructors
    //


    /**
     * CONSTRUCTORS
     */
    DwellingEmissionRate();
    DwellingEmissionRate(const ConfigParser& config,
                               DwellingDimensions *dims, 
                               SpaceHeating *spaceheat, 
                               WaterHeatingEnergy *waterheat,
                               Gains *gains,
                               FuelCosts *fcosts); 
    ~DwellingEmissionRate();

    void initVars();
    /**
     * METHODS
     */
    //Accessors
   void setSpaceHeatingEnergy ( double val_in );
  double getSpaceHeatingEnergy ( );
  void setSpaceHeatingEmisFactor ( double val_in );
  double getSpaceHeatingEmisFactor ( );
  void setSpaceHeatingEmissionsYear ( double val_in );
  double getSpaceHeatingEmissionsYear ( );
  void setSecondarySpaceHeating ( double val_in );
  double getSecondarySpaceHeating ( );
  void setSecondarySpaceHeatEmisFactor ( double val_in );
  double getSecondarySpaceHeatEmisFactor ( );
  void setSecondarySpaceHeatingEmissionsYear ( double val_in );
  double getSecondarySpaceHeatingEmissionsYear ( );
  void setEnergyForWaterHeating ( double val_in );
  double getEnergyForWaterHeating ( );
  void setWaterHeatingEmissionsFactor ( double val_in );
  double getWaterHeatingEmissionsFactor ( );
  void setWaterHeatingEmissionsYear ( double val_in );
  double getWaterHeatingEmissionsYear ( );
  void setSpaceWaterHeating ( double val_in );
  double getSpaceWaterHeating ( );
  void setElecForPumpsFansEmisFactor ( double val_in );
  double getElecForPumpsFansEmisFactor ( );
  void setElecForPumpsFansEmissionsYear ( double val_in );
  double getElecForPumpsFansEmissionsYear ( );
  void setEnergyProdOrSavedFactor ( double val_in );
  double getEnergyProdOrSavedFactor ( );
  void setEnergyConsumed ( double val_in );
  double getEnergyConsumed ( );
  void setTotalCO2KgYear ( double val_in );
  double getTotalCO2KgYear ( );
  void setDwelCO2EmmRate ( double val_in );
  double getDwelCO2EmmRate ( );
  void setLightingEmisFactor ( double val_in );
  double getLightingEmisFactor ( );
  void setLightingEmissionsYear ( double val_in );
  double getLightingEmissionsYear ( );
  void setSpaceAndWaterHeating ( double val_in );
  double getSpaceAndWaterHeating ( );
  void setEnergyProdOrSavedEmissionsYear ( double val_in );
  double getEnergyProdOrSavedEmissionsYear ( );
  void setEnergyConsumeTechnologyEmisFactor ( double val_in );
  double getEnergyConsumeTechnologyEmisFactor ( );
  double getEIrating();
  void setEIrating(double val_in);
  void calcAll();

  private:

  double m_spaceHeatingEnergy;
  double m_spaceHeatingEmisFactor;
  double m_spaceHeatingEmissionsYear;
  double m_secondarySpaceHeating;
  double m_secondarySpaceHeatEmisFactor;
  double m_secondarySpaceHeatingEmissionsYear;
  double m_energyForWaterHeating;
  double m_waterHeatingEmissionsFactor;
  double m_waterHeatingEmissionsYear;
  double m_spaceWaterHeating;
  double m_elecForPumpsFansEmisFactor;
  double m_elecForPumpsFansEmissionsYear;
  double m_energyProdOrSavedFactor;
  double m_energyConsumedYear;
  double m_totalCO2KgYear;
  double m_dwelCO2EmmRate;
  double m_lightingEmisFactor;
  double m_lightingEmissionsYear;
  double m_spaceAndWaterHeating;
  double m_energyProdOrSavedEmissionsYear;
  double m_energyConsumeTechnologyEmisFactor;
  double m_EIrating;
  void calcDwellingCO2EmisRate();
  void calcEIRating();
    // Private attributes
    //
    ConfigParser cf;
    DwellingDimensions *ptrDims; 
    WaterHeatingEnergy *ptrWaterHeatEnergy;
    SpaceHeating *ptrSpaceHeat;
    Gains *ptrGains;
    FuelCosts *ptrFuelCosts;
    //
    };

#endif // DWELLINGEMISSIONRATE_H
