/***************************************************************************
 *                                                                         * 
 *  DoubleReader.h Copyright (C) 2008 by Jon Rumble                           *  
 *  j.w.rumble@reading.ac.uk   				                   *
 *                                                                         *
 *                                                                         * 
 *  This file is part of HECS,                                             *
 *                                                                         *
 *  HECS is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation, either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *                                                                         *
 *  HECS is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/

#ifndef DOUBLEREADER_H
#define DOUBLEREADER_H 
 
#include "TableReader.h"
 
class DoubleReader : public TableReader {
 
public:
DoubleReader (const QString& fileName);
DoubleReader();
virtual ~DoubleReader ();
 
void readFile(const QString& fileNameIn);
QMap <double, QList <double> > getDoubleMap();
protected:
 
 
void parseFileD();
void parseLineD(QString lineIn, QMap <double,QList <double> >& ValueMap); 
private:
 
QMap <double, QList <double> > ValueMap;
};
 
 
#endif //END OF DOUBLEREADER_H

