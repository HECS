
/***************************************************************************
 *                                                                         * 
 *  DwellingDimensions.h Copyright (C) 2008 by Jon Rumble                  *              
 *  j.w.rumble@reading.ac.uk   				                   *
 *                                                                         *
 *  This file is part of HECS,                                             *                                
 *  HECS is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation, either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *  HECS is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/

#ifndef DWELLINGDIMENSIONS_H
#define DWELLINGDIMENSIONS_H 

#include "ConfigParser.h"
#include <QDate>
class DwellingDimensions {

    public:
        
        //Constructors
        DwellingDimensions ();
        DwellingDimensions (const ConfigParser& config);
        ~DwellingDimensions();

        //Getter/Setters
        double get_groundFloorArea();
        double get_groundFloorHeight();
        double get_groundFloorVolume();
        double get_firstFloorArea();
        double get_firstFloorHeight();
        double get_firstFloorVolume();
        double get_secondFloorArea();
        double get_secondFloorHeight();
        double get_secondFloorVolume();
        double get_thirdFloorArea();
        double get_thirdFloorHeight();
        double get_thirdFloorVolume();
        double get_totalFloorArea();
        double get_dwellingVolume();
        double get_avgStoreyHeight();
        double get_livingRoomArea();
        
        QString get_houseNumber();
        QString get_address();
        QString get_town();
        QString get_postcode();
        QString get_county();
        int     get_region();
        int     get_ageBand();
        int     get_propertyType();
        int     get_archType();
        int     get_habitableRooms();
        

        void set_groundFloorArea(double val_in);
        void set_groundFloorHeight(double val_in);
        void set_groundFloorVolume(double val_in);
        void set_firstFloorArea (double val_in);
        void set_firstFloorHeight (double val_in);
        void set_firstFloorVolume (double val_in);
        void set_secondFloorArea(double val_in);
        void set_secondFloorHeight(double val_in);
        void set_secondFloorVolume(double val_in);
        void set_thirdFloorArea (double val_in);
        void set_thirdFloorHeight (double val_in);
        void set_thirdFloorVolume (double val_in);
        void set_totalFloorArea (double val_in);
        void set_dwellingVolume (double val_in);
        void set_avgStoreyHeight(double val_in);
        void set_livingRoomArea(double val_in);
        void set_houseNumber (QString val_in);
        void set_address(QString val_in);
        void set_town(QString val_in);
        void set_postcode(QString val_in);
        void set_county(QString val_in);
        void set_region(int val_in);
        void set_ageBand(int val_in);
        void set_propertyType(int val_in);
        void set_archType(int val_in);
        void set_habitableRooms(int val_in);


        void calcAll();
        void clearAll();
    protected:
        //None

    private:
        
        //Private Methods
        void initVars();


        //Members

        // First Floor Area (m2)
        double m_firstFloorArea;
        double m_firstFloorHeight;
        double m_firstFloorVolume;
        // Second Floor Area (m2)
        double m_secondFloorArea;
        double m_secondFloorHeight;
        double m_secondFloorVolume;
        // Third and other Floor Area (m2)
        double m_thirdFloorArea;
        double m_thirdFloorHeight;
        double m_thirdFloorVolume;
        // Ground Floor Area (m2)
        double m_groundFloorArea;
        double m_groundFloorHeight;
        double m_groundFloorVolume;
        // Total Floor Area Of Dwelling (m2)
        double m_totalFloorArea;
        // Calculated Dwelling Volume (m3)
        double m_dwellingVolume;
        // Average Storey Height (M)
        double m_avgStoreyHeight;
        QString m_houseNumber;
        // Address Line 1
        QString m_address;
        //Town
        QString m_town;
        // Postcode
        QString m_postcode;
        // County
        QString m_county;
        // Region
        int m_region;
        //Property Age Band
        int m_ageBand;
        //Property Type
        int m_propertyType;
        //Architecture Type
        int m_archType;
        //Habitable Rooms
        int m_habitableRooms;

        ConfigParser cf;
        //Living Room Area
        double m_livingRoomArea;
        //Season
       
        void calcTotalFloorArea();
        void calcDwellingVol();
 
};



#endif

