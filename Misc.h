/***************************************************************************
 *                                                                         *
 *  Gains.h Copyright (C) 2008 by Jon Rumble                               *
 *  j.w.rumble@reading.ac.uk   				                   *
 *                                                                         *
 *  This file is part of HECS,                                             *
 *                                                                         *
 *  HECS is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation, either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *  HECS is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/


#ifndef MISC_H
#define MISC_H

#include <QHash>
#include <QString>
#include <QStringList>

#include <cmath>

double kWHtoGigaJoules(double val_in);

double lerp(const double& x1, 
                  double& x2, 
                  double& x3, 
                  double& y1, 
                  double& y3);

double calcWk(double area, double uValue);

double sqFeettoSquareMetres(double sqFeetIn);

QStringList pruneDuplicates(QStringList strListIn);

QString sedbukGrade(int sedbukVal);
#endif // MISC_H
