/***************************************************************************
 *                                                                         * 
 *  SpaceHeating.h Copyright (C) 2008 by Jon Rumble                        *  
 *  j.w.rumble@reading.ac.uk   				                   *
 *                                                                         *
 *                                                                         * 
 *  This file is part of HECS,                                             *
 *                                                                         *
 *  HECS is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation, either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *                                                                         *
 *  HECS is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/

#ifndef SPACEHEATING_H
#define SPACEHEATING_H 

#include "ConfigParser.h"
#include "Gains.h"

class SpaceHeating {

public:
SpaceHeating ();
SpaceHeating (const ConfigParser& config,Gains *gains);
~SpaceHeating ();


  void setFracHeatFromSecondary ( double val_in );
  double getFracHeatFromSecondary ( );
  void setEffcyMainHeatingSys ( int val_in );
  int getEffcyMainHeatingSys ( );
  void setEffcySecondaryHeatingSystem ( int val_in );
  int getEffcySecondaryHeatingSystem ( );
  void setSpaceHeatingFuelMainReq ( double val_in );
  double getSpaceHeatingFuelMainReq ( );
  void setSpaceHeatingFuelSecondary ( double val_in );
  double getSpaceHeatingFuelSecondary ( );

  void calcAll();
protected:

private:
  double m_fracHeatFromSecondary;
  int m_effcyMainHeatingSys;
  int m_effcySecondaryHeatingSystem;
  double m_spaceHeatingFuelMainReq;
  double m_spaceHeatingFuelSecondary;

  void initVars();
  void calcSpaceHeatingFuelMain();
  void calcSpaceHeatingFuelSec();

  Gains *ptrGains;
  ConfigParser cf;
};


#endif //END OF SPACEHEATING_H
