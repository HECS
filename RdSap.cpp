/***************************************************************************
 *                                                                         * 
 *  RdSap.cpp Copyright (C) 2008 by Jon Rumble                             *
 *  j.w.rumble@reading.ac.uk   				                   *
 *                                                                         *
 *  This file is part of HECS,                                             *                   
 *                                                                         * 
 *  HECS is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation, either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *  HECS is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/

#include "RdSap.h"

// Constructors/Destructors
//  


RdSap::RdSap (const QString& fileName) {
    m_fileName = fileName;
    initVars();
}
RdSap::~RdSap ( ) 
{
    destroySap();
}

//  
// Methods
//  

void RdSap::buildSap()
{
    //Try to read INI File
    //ConfigParser *configFile;
    QString uVals ("uValsEnglandWales");
    ConfigParser configFile(m_fileName);
    TableReader  uvalFile (uVals);
    //configFile.openFile(); 

    dwelling = new DwellingDimensions(configFile);
    ventilation = new Ventilation(configFile,dwelling);
    heatlosses = new HeatLosses(configFile,dwelling,ventilation);
    hotwater = new WaterHeating(configFile,dwelling);
    gains = new Gains(configFile,hotwater,heatlosses,dwelling);
    spaceheat = new SpaceHeating(configFile,gains);
    hotwaterenergy = new WaterHeatingEnergy(configFile,hotwater);
    fuelcosts = new FuelCosts(configFile,dwelling,spaceheat,hotwaterenergy,gains);
    emissions = new DwellingEmissionRate(configFile,
                                         dwelling,
                                         spaceheat,
                                         hotwaterenergy,
                                         gains,
                                         fuelcosts);

}

void RdSap::calcModel()
{
    dwelling->calcAll();
    ventilation->calcAll();
    heatlosses->calcAll();
    hotwater->calcAll();
    gains->calcAll();
    spaceheat->calcAll();
    hotwaterenergy->calcAll();
    fuelcosts->calcAll();
    emissions->calcAll();
}

void RdSap::outputTest()
{
    //std::cout.precision(3);
    //Dwelling Dimensions
    std::cout << "-------Dwelling Dimensions-----\n\n";
    
    std::cout << "Total Floor Area: " << dwelling->get_totalFloorArea()
              << "  Square Meters"
              << "\n"
              << "Dwelling Volume: " << dwelling->get_dwellingVolume() 
              << "  Cubic Metres "
              << "\n"
              << std::endl;

    //Ventilation
    std::cout << "------- Ventilation -------\n\n";

    std::cout << "\nNumber of Chimneys: " 
              << ventilation->get_numChimneys()
              << "\n"
              << "Number of open flues: "
              << ventilation->get_numOpenFlues()
              << "\n"
              << "Number of intermitent fans or passive vents: " 
              << ventilation->get_numPasivFanVents()
              << "\n"
              << "Number of flueless gas fires" 
              << ventilation->get_numGasFluelessFires()
              << "\n"
              << "Infiltration due to chimneys, flues and fans: " 
              << ventilation->get_cvfInfiltration()
              << "\n"
              << "Air changes per hour: " 
              << ventilation->get_numAirChanges()
              << "\n"
              << "Number of storeys in dwelling: " 
              << ventilation->get_numStoreys()
              << "\n"
              << "Additional infiltration: " 
              << ventilation->get_addtnInfiltration()
              << "\n"
              << "Structural infiltration: " 
              << ventilation->get_structInfiltration()
              << "\n"
              << "Suspended wooden floor: " 
              << ventilation->get_suspFloorType()
              << "\n"
              << "Draught Lobby: " 
              << ventilation->get_draughtLobby()
              << "\n"
              << "Percentage of doors & windows draught stripped: " 
              << ventilation->get_draughtStrippedPctg()
              << "\n"
              << "Window Infiltration: "
              << ventilation->get_windowInfiltration()
              << "\n"
              << "Infiltration Rate: " 
              << ventilation->get_infiltrationRate()
              << "\n"
              << "Number of sheltered sides: " 
              << ventilation->get_numShelteredSides()
              << "\n"
              << "Shelter Factor: " 
              << ventilation->get_shelterFactor()
              << "\n"
              << "Adjusted Infiltration Rate: " 
              << ventilation->get_adjInfiltrationRate()
              << "\n"
              << "Calculated effective air change rate: " 
              << ventilation->get_effecAirChangeRate()
              << "\n"
              << std::endl;

    std::cout << "------- Water Heating  -------\n\n";

    std::cout << "Output From Water Heater: " 
              << hotwater->getWaterHeaterOutput()
              << "\n"
              << "Distribution Loss: " 
              << hotwater->getDistributionLoss()
              << "\n"
               << "Hot Water Heat Gains: " 
              << kWHtoGigaJoules(hotwater->getHotWaterHeatGains())
              << " GJ\n"
              << std::endl;

    std::cout << "------- Heat Losses-------\n\n";

    std::cout << "Total Area Of Elements " 
              << heatlosses->getTotalAreaElems()
              << "\n"
              << "Fabric Heat Loss: "
              << heatlosses->getFabricHeatLoss()
              << "\n"
              << "Thermal Bridges " 
              << heatlosses->getThermalBridges()
              << "\n"
              << "Total Fabric Heat Loss: "
              << heatlosses->getTotalFabricHeatLoss()
              << "\n"
              << "Ventilation Heat Loss: "
              << heatlosses->getVentilationHeatLoss()
              << "\n"
              << "Heat Loss coefficent: "
              << heatlosses->getHeatLossCoeff()
              << "\n"
              << "Heat Loss parameter: "
              << heatlosses->getHeatLossParam()
              << "\n"
              << std::endl;

    std::cout << "------- Water Heating Parameters -------\n\n";

    std::cout << "Energy content of hot water from table 1: "
              << hotwater->getEnergyContentHotWater()
              << "\n"
              << "Energy Lost From water storage: "
              << hotwater->getEnergyLostWaterStore()
              << "\n"
              << "Output from water heater: "
              << hotwater->getWaterHeaterOutput()
              << "\n"
              << "Heat Gains from water heating: "
              << hotwater->getHotWaterHeatGains()
              << "\n"
              << std::endl;

    std::cout << "------- Internal Gains -------\n\n";

    std::cout << "Lights, appliances, cooking and metabolic: " 
              << gains->getLightAppCookMetabolicGains()
              << "\n"
              << "Reduction of internal gains due to low energy lighting: "
              << gains->getLowEnergyLightRed()
              << "\n"
              << "Additional gains from table 5a: "
              << gains->getAddtnGains()
              << "\n"
              << "Water Heating: "
              << gains->getWaterHeating()
              << "\n"
              << "Total Internal Gains: "
              << gains->getTotalIntGains()
              << "\n"
              << std::endl;

    std::cout << "------- Solar Gains -------\n\n";

    std::cout << "Total Solar Gains: " 
              << gains->getTotalSolarGains()
              << "\n"
              << "Total Gains: "
              << gains->getTotalGains()
              << "\n"
              << "Gain/Loss Ratio: "
              << gains->getGainLossRatio()
              << "\n"
              << "Utilisation Factor: "
              << gains->getUtilisationFactor()
              << "\n"
              << "Useful Gains: "
              << gains->getUsefulGains()
              << "\n"
              << std::endl; 

    std::cout << "------- Mean Internal Temperature -------\n\n";

    std::cout << "Mean Internal Temperature of Living Area: " 
              << gains->getMeanTempLivingArea()
              << "\n"
              << "Temperature Adjustment from table 4e, where appropriate: "
              << gains->getTempAdjustment()
              << "\n"
              << "Adjustment for Gains: "
              << gains->getAdjustForGains()
              << "\n"
              << "Adjusted living room temperature: "
              << gains->getAdjustLivingTemp()
              << "\n"
              << "Temperature Difference Between Zones: "
              << gains->getTempDiffZones()
              << "\n"
              << "Living Area Fraction: "
              << gains->getLivingAreaFrac()
              << "\n"
              << "Rest-of-house Fraction: "
              << gains->getRestOfHouseFrac()
              << "\n"
              << "Mean Internal Temperature: "
              << gains->getMeanIntTemp()
              << "\n"
              << std::endl; 

    std::cout << "------- Degree Days -------\n\n";

    std::cout << "Temperature Rise From Gains: " 
              << gains->getTempRiseFromGains()
              << "\n"
              << "Base Temperature: "
              << gains->getBaseTemp()
              << "\n"
              << "Degree Days: "
              << gains->getDegreeDays()
              << "\n"

              << std::endl;
     
    std::cout << "------- Space Heating Requirement -------\n\n";

    std::cout << "Space heating requirement (useful): " 
              << gains->getSpaceHeatingReq()
              << "\n"
              << std::endl;

    std::cout << "------- Energy Requirements -------\n\n";

    std::cout << "Fraction of heat from sec system table 11: " 
              << spaceheat->getFracHeatFromSecondary()
              << "\n"
              << "Efficency of main heating system: " 
              << spaceheat->getEffcyMainHeatingSys()
              << "\n"
              << "Efficency of secondary heating system: " 
              << spaceheat->getEffcySecondaryHeatingSystem()
              << "\n"
              << "Efficency of Water Heater % : " 
              << hotwaterenergy->getEffcyOfWaterHeater()
              << "\n"
              << "Energy Required for Water Heating: "
              << hotwaterenergy->getEnergyReqdForWaterHeat()
              << "\n"
              << "Total Electricity for the above equipment: "
              << hotwaterenergy->getTotalForAboveElec()
              << "\n"


              << std::endl;

    std::cout << "------- Fuel Costs-------\n\n";

    std::cout << "Primary Space Heating Cost: " 
              << fuelcosts->getSpaceHeatFuelCostPri()
              << "\n"
              << "Secondary Space Heating Cost: " 
              << fuelcosts->getSpaceHeatFuelCostSec()
              << "\n"
              << "Water Heating Cost (other fuel) : " 
              << fuelcosts->getWaterHeatingOtherFuelCost()
              << "\n"
              << "Water Heating Cost : " 
              << fuelcosts->getWaterHeatingOtherFuelCost()
              << "\n"
              << "Pump and fan energy cost: "
              << fuelcosts->getPumpFanEnergyCost()
              << "\n"
              << "Energy for lighting cost : " 
              << fuelcosts->getEnergyLightingCost()
              << "\n"
              << "Total Energy Cost: " 
              << fuelcosts->getTotalEnergyCost()
              << "\n"

              << std::endl;

    std::cout << "------- SAP Rating-------\n\n";

    std::cout << "SAP Energy Cost Factor: " 
              << fuelcosts->getSAPEnergyCostFactor()
              << "\n"
              << "SAP Rating: " 
              << fuelcosts->getSAPrating()
              << "\n"

              << std::endl;

    std::cout << "------- Dwelling CO2 Emission Rate -------\n\n";

    std::cout << "Space and water heating emis per year: " 
              << emissions->getSpaceHeatingEmissionsYear()
              << "\n"
              << "Emissions from Lighting: " 
              << emissions->getLightingEmissionsYear()
              << "\n"
              << "Emissions from Water Heating: " 
              << emissions->getWaterHeatingEmissionsYear()
              << "\n"
              << "Energy produced or saved in dwelling: "
              << emissions->getEnergyProdOrSavedEmissionsYear()
              << "\n"
              << "Energy consumed by the above technology: "
              << emissions->getEnergyConsumed()
              << "\n"
              << "Total CO2 Kg/Year: "
              << emissions->getTotalCO2KgYear()
              << "\n"
              << "Dwelling CO2 Emission Rate: " 
              << emissions->getDwelCO2EmmRate()
              << "\n"
              << "Environmental Impact (EI) Rating: "
              << emissions->getEIrating()
              << "\n"

            
              << std::endl;
}

void RdSap::destroySap()
{
    delete dwelling;
    delete ventilation;
    delete heatlosses;
    delete hotwater;
    delete hotwaterenergy;
    delete spaceheat; 
    delete gains;
    delete fuelcosts;
    delete emissions;
    delete sedbukList;
}

void RdSap::buildBoilerList()
{

    QString fileName = "bedf2005.dat"; 
    sedbukList = new SedbukList(fileName);

}


void RdSap::buildLT()
{
    /*tr.readFile("tableS3");
    tableS3 = tr.getTableMap();
    tr.readFile("tableS9");
    tableS9 = tr.getTableMap();*/

    TableReader trS3 ("tableS3");
    tableS3 = trS3.getTableMap();
    TableReader trS9 ("tableS9");
    tableS9 = trS9.getTableMap();
}

void RdSap::initVars()
  {

    buildLT();
    buildSap();
    calcModel();
    outputTest();
  }

// RdSAP Functions -------------

//S4 Window Area
//
void RdSap::windowArea()
{
    //House or Bungalow
    if(dwelling->get_propertyType() == 1 || dwelling->get_propertyType() == 2)
    {
      switch (dwelling->get_ageBand()){
          case 0: 
              m_RdWindowArea = (0.1220 * dwelling->get_totalFloorArea() + 6.875);
              break;
          case 1:
              m_RdWindowArea = (0.1220 * dwelling->get_totalFloorArea() + 6.875);
              break;
          case 2:
              m_RdWindowArea = (0.1220 * dwelling->get_totalFloorArea() + 6.875);
              break;
          case 3:
              m_RdWindowArea = (0.1294 * dwelling->get_totalFloorArea() + 5.515);
              break;
          case 4: 
              m_RdWindowArea = (0.1239 * dwelling->get_totalFloorArea() + 7.332);
              break;
          case 5:
              m_RdWindowArea = (0.1252 * dwelling->get_totalFloorArea() + 5.520);
              break;
          case 6:
              m_RdWindowArea = (0.1356 * dwelling->get_totalFloorArea() + 5.242);
              break;
          case 7:
              m_RdWindowArea = (0.0948 * dwelling->get_totalFloorArea() + 6.534);
              break;
          case 8:
              m_RdWindowArea = (0.1382 * dwelling->get_totalFloorArea() - 0.027);
              break;
          case 9: 
              m_RdWindowArea = (0.1435 * dwelling->get_totalFloorArea() - 0.403);
              break;
          case 10:
              m_RdWindowArea = (0.1435 * dwelling->get_totalFloorArea() - 0.403);
              break;
      }
}
    //Flat or Maisonette
    else 
    {
      switch (dwelling->get_ageBand()){
          case 0: 
              m_RdWindowArea = (0.0801 * dwelling->get_totalFloorArea() + 5.580);
              break;
          case 1:
              m_RdWindowArea = (0.0801 * dwelling->get_totalFloorArea() + 5.580);
              break;
          case 2:
              m_RdWindowArea = (0.0801 * dwelling->get_totalFloorArea() + 5.580);
              break;
          case 3:
              m_RdWindowArea = (0.0341 * dwelling->get_totalFloorArea() + 8.562);
              break;
          case 4: 
              m_RdWindowArea = (0.0717 * dwelling->get_totalFloorArea() + 6.560);
              break;
          case 5:
              m_RdWindowArea = (0.1199 * dwelling->get_totalFloorArea() + 1.975);
              break;
          case 6:
              m_RdWindowArea = (0.0510 * dwelling->get_totalFloorArea() + 4.554);
              break;
          case 7:
              m_RdWindowArea = (0.0813 * dwelling->get_totalFloorArea() + 3.744);
              break;
          case 8:
              m_RdWindowArea = (0.1148 * dwelling->get_totalFloorArea() + 0.392);
              break;
          case 9: 
              m_RdWindowArea = (0.1747 * dwelling->get_totalFloorArea() - 2.834 );
              break;
          case 10:
              m_RdWindowArea = (0.1747 * dwelling->get_totalFloorArea() - 2.834 );
              break;
      }
    }

}


void RdSap::ventilationParams()
{
    if(dwelling->get_propertyType() == 3 || dwelling->get_propertyType()== 4)
        ventilation->set_numShelteredSides(4);
    else
        ventilation->set_numShelteredSides(2);
}

// S16 Living Area Fraction
//
void RdSap::roomCountLivingArea()
{
    switch (dwelling->get_habitableRooms()){
        case 0: gains->setLivingAreaFrac(0.75); break;
        case 1: gains->setLivingAreaFrac(0.50); break;
        case 2: gains->setLivingAreaFrac(0.30); break;
        case 3: gains->setLivingAreaFrac(0.25); break;
        case 4: gains->setLivingAreaFrac(0.21); break;
        case 5: gains->setLivingAreaFrac(0.18); break;
        case 6: gains->setLivingAreaFrac(0.16); break;
        case 7: gains->setLivingAreaFrac(0.14); break;
        case 8: gains->setLivingAreaFrac(0.13); break;
        case 9: gains->setLivingAreaFrac(0.12); break;
        case 10: gains->setLivingAreaFrac(0.11); break;
        case 11: gains->setLivingAreaFrac(0.10); break;
        case 12: gains->setLivingAreaFrac(0.10); break;
        case 13: gains->setLivingAreaFrac(0.09); break;
        //Assume 15+
        default: gains->setLivingAreaFrac(0.09); break;
    }

}

void RdSap::cylinderSize()
{
    switch (m_RdCylinderSize) {
        case 1: hotwater->setCylinderVolume(110); break;
        case 2: hotwater->setCylinderVolume(110); break;
        case 3: hotwater->setCylinderVolume(160); break;
        case 4: hotwater->setCylinderVolume(210); break;

    }

}
/*void RdSap::spaceAndWaterHeating()
{
    
}*/


// RdSAP Accessors -------------

double RdSap::getRdWindowArea ()
{
    return m_RdWindowArea;
}

double RdSap::getRdTotalFloorArea()
{
    return m_RdTotalFloorArea;
}

int RdSap::getRdWindowReductionFactor()
{
    return m_RdWindowReductionFactor;
}

int RdSap::getRdCylinderSize()
{
    return m_RdCylinderSize;
}

void RdSap::setRdWindowArea (double val_in)
{
    m_RdWindowArea = val_in;
}
    
void RdSap::setRdTotalFloorArea(double val_in)
{
    m_RdWindowArea = val_in;
}

void RdSap::setRdWindowReductionFactor(double val_in)
{
    m_RdWindowReductionFactor = val_in;
}

void RdSap::setRdCylinderSize(int val_in)
{
    m_RdCylinderSize = val_in;
}


