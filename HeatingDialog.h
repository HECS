/***************************************************************************
 *                                                                         * 
 *  HeatingDialog.h Copyright (C) 2008 by Jon Rumble                       *  
 *  j.w.rumble@reading.ac.uk   				                   *
 *                                                                         *
 *                                                                         * 
 *  This file is part of HECS,                                             *
 *                                                                         *
 *  HECS is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation, either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *                                                                         *
 *  HECS is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/

#ifndef HEATINGDIALOG_H
#define HEATINGDIALOG_H 

#include "ui_heating.h"
#include "RdSap.h"
#include "Misc.h"
#include <QDialog>
#include <QString>
#include <QRegExp>
#include <QRegExpValidator>
#include <QHash>
#include <QHashIterator>
#include <QList>
//#include "Boiler.h"
#include "Sedbuk.h"

class HeatingDialog : public QDialog
{

    Q_OBJECT

    public:
    HeatingDialog (QWidget *parent =0);
    void setModel(RdSap *modelIn);
    //virtual ~HeatingDialog ();

    public slots:
    void onBoilerFuelTypeCbxChange();
    void onBoilerModelCbxChange();
    void onBoilerManufacturerCbxChange();


    signals:

    private: 
    Ui::HeatingDialog ui;
    RdSap *sapModel; 
    //SedbukList *sedbukHash;
    void initVars();
    int m_fuelFlag;

    void buildModelTable();
    //Internal Vars for widget

};


#endif //END OF HEATINGDIALOG_H
