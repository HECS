/***************************************************************************
 *                                                                         *
 *  Boiler.cpp Copyright (C) 2008 by Jon Rumble                            *
 *  j.w.rumble@reading.ac.uk   				                   *
 *                                                                         *
 *  This file is part of HECS,                                             *
 *                                                                         *
 *  HECS is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation, either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *  HECS is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/

#include "Boiler.h"

    Boiler::Boiler (int productIndexNo, 
                    QString brandName,
                    QString modelName,
                    QString modelQualifier,
                    double sapSeasonalEffcy,
                    int fuelType)
    {

       m_productIndexNo = productIndexNo;
       m_brandName = brandName;
       m_modelName = modelName;
       m_modelQualifier = modelQualifier;
       m_sapSeasonalEffcy = sapSeasonalEffcy;
       m_fuelType = fuelType;
    }   

    Boiler::~Boiler() { }
    Boiler::Boiler () { }


    int  Boiler::get_productIndexNo() 
    {
        return m_productIndexNo;
    }

    QString  Boiler::get_brandName ()
    {
        return m_brandName;
    }    
    
    QString  Boiler::get_modelName ()
    {
        return m_modelName;
    }
    
    QString  Boiler::get_modelQualifier ()
    {
        return m_modelQualifier;
    }   

    int Boiler::get_fuelType ()
    {
        return m_fuelType;
    }
    
    double Boiler::get_sapSeasonalEffcy ()
    {
       return  m_sapSeasonalEffcy;
    }
