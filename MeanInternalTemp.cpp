/***************************************************************************
 *                                                                         * 
 *  MeanInternalTemp.cpp Copyright (C) 2008 by Jon Rumble                  *
 *  j.w.rumble@reading.ac.uk   				                   *
 *                                                                         *
 *  This file is part of HECS,                                             *
 *                                                                         *
 *  HECS is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation, either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *  HECS is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/

#include "MeanInternalTemp.h"

// Constructors/Destructors
//  

MeanInternalTemp::MeanInternalTemp ( ) {
initAttributes();
}

MeanInternalTemp::~MeanInternalTemp ( ) { }

//  
// Methods
//  


// Accessor methods
//  


// Private attribute accessor methods
//  


void MeanInternalTemp::setIntTempLivingArea ( double new_var ) {
  m_intTempLivingArea = new_var;
}


double MeanInternalTemp::getIntTempLivingArea ( ) {
  return m_intTempLivingArea;
}


void MeanInternalTemp::setTempAdj ( double new_var ) {
  m_tempAdj = new_var;
}


double MeanInternalTemp::getTempAdj ( ) {
  return m_tempAdj;
}


void MeanInternalTemp::setAdjForGains ( double new_var ) {
  m_adjForGains = new_var;
}


double MeanInternalTemp::getAdjForGains ( ) {
  return m_adjForGains;
}


void MeanInternalTemp::setAdjLivingRoomTemp ( double new_var ) {
  m_adjLivingRoomTemp = new_var;
}


double MeanInternalTemp::getAdjLivingRoomTemp ( ) {
  return m_adjLivingRoomTemp;
}


void MeanInternalTemp::setTempDiffZone ( double new_var ) {
  m_tempDiffZone = new_var;
}


double MeanInternalTemp::getTempDiffZone ( ) {
  return m_tempDiffZone;
}


void MeanInternalTemp::setLivingAreaFraction ( double new_var ) {
  m_livingAreaFraction = new_var;
}


double MeanInternalTemp::getLivingAreaFraction ( ) {
  return m_livingAreaFraction;
}


void MeanInternalTemp::setRestOfHouseFrac ( double new_var ) {
  m_restOfHouseFrac = new_var;
}


double MeanInternalTemp::getRestOfHouseFrac ( ) {
  return m_restOfHouseFrac;
}


void MeanInternalTemp::setMeanIntTemp ( double new_var ) {
  m_meanIntTemp = new_var;
}


double MeanInternalTemp::getMeanIntTemp ( ) {
  return m_meanIntTemp;
}

void MeanInternalTemp::calcMeanInternalTemp ()
{

}

void MeanInternalTemp::initAttributes ( ) {
  /*m_intTempLivingArea = cf.getValueDouble("MeanInternalTemp:intTempLivingArea");
  m_tempAdj = cf.getValueDouble("MeanInternalTemp:tempAdj");
  m_adjForGains = cf.getValueDouble("MeanInternalTemp:adjForGains");
  m_adjLivingRoomTemp = cf.getValueDouble("MeanInternalTemp:adjLivingRoomTemp");
  m_tempDiffZone = cf.getValueDouble("MeanInternalTemp:tempDiffZone");
  m_livingAreaFraction = cf.getValueDouble("MeanInternalTemp:livingAreaFraction");
  m_restOfHouseFrac = cf.getValueDouble("MeanInternalTemp:restOfHouseFrac");
  m_meanIntTemp = cf.getValueDouble("MeanInternalTemp:meanIntTemp");*/
}

