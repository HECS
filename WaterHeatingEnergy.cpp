/***************************************************************************
 *                                                                         * 
 *  WaterHeatingEnergy.cpp Copyright (C) 2008 by Jon Rumble                             *  
 *  j.w.rumble@reading.ac.uk   				                   *
 *                                                                         *
 *                                                                         * 
 *  This file is part of HECS,                                             *
 *                                                                         *
 *  HECS is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation, either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *                                                                         *
 *  HECS is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/


#include "WaterHeatingEnergy.h"


WaterHeatingEnergy::WaterHeatingEnergy (const ConfigParser& config, WaterHeating *waterHeat) 
{
    cf = config;
    ptrWaterHeat = waterHeat;
    initVars();
    calcAll();
}

WaterHeatingEnergy::~WaterHeatingEnergy () { }

void WaterHeatingEnergy::setEffcyOfWaterHeater ( int val_in ) {
  m_effcyOfWaterHeater = val_in;
}

int WaterHeatingEnergy::getEffcyOfWaterHeater ( ) {
  return m_effcyOfWaterHeater;
}

void WaterHeatingEnergy::setEnergyReqdForWaterHeat ( double val_in ) {
  m_energyReqdForWaterHeat = val_in;
}

double WaterHeatingEnergy::getEnergyReqdForWaterHeat ( ) {
  return m_energyReqdForWaterHeat;
}

void WaterHeatingEnergy::setElecForCentralHeatingPumps ( double val_in ) {
  m_elecForCentralHeatingPumps = val_in;
}

double WaterHeatingEnergy::getElecForCentralHeatingPumps ( ) {
  return m_elecForCentralHeatingPumps;
}

void WaterHeatingEnergy::setElecForFanAssitFlue ( double val_in ) {
  m_elecForFanAssitFlue = val_in;
}

double WaterHeatingEnergy::getElecForFanAssitFlue ( ) {
  return m_elecForFanAssitFlue;
}

void WaterHeatingEnergy::setElecForWarmHeatingSysFans ( double val_in ) {
  m_elecForWarmHeatingSysFans = val_in;
}

double WaterHeatingEnergy::getElecForWarmHeatingSysFans ( ) {
  return m_elecForWarmHeatingSysFans;
}

void WaterHeatingEnergy::setElecForMechVentilation ( double val_in ) {
  m_elecForMechVentilation = val_in;
}

double WaterHeatingEnergy::getElecForMechVentilation ( ) {
  return m_elecForMechVentilation;
}

void WaterHeatingEnergy::setElecForKeepHotGasCombi ( double val_in ) {
  m_elecForKeepHotGasCombi = val_in;
}

double WaterHeatingEnergy::getElecForKeepHotGasCombi ( ) {
  return m_elecForKeepHotGasCombi;
}

void WaterHeatingEnergy::setElecForSolarWaterPump ( double val_in ) {
  m_elecForSolarWaterPump = val_in;
}

double WaterHeatingEnergy::getElecForSolarWaterPump ( ) {
  return m_elecForSolarWaterPump;
}

void WaterHeatingEnergy::setTotalForAboveElec ( double val_in ) {
  m_totalForAboveElec = val_in;
}

double WaterHeatingEnergy::getTotalForAboveElec ( ) {
  return m_totalForAboveElec;
}

void WaterHeatingEnergy::calcEnergyReqWaterHeating()
{
    m_energyReqdForWaterHeat =  ptrWaterHeat->getWaterHeaterOutput() *
                                                                100  /
                                                m_effcyOfWaterHeater;
}

void WaterHeatingEnergy::calcElecForAbove ()
{
    m_totalForAboveElec = m_elecForCentralHeatingPumps +
                          m_elecForFanAssitFlue +
                          m_elecForWarmHeatingSysFans +
                          m_elecForMechVentilation +
                          m_elecForKeepHotGasCombi +
                          m_elecForSolarWaterPump;
}

void WaterHeatingEnergy::calcAll()
{
    calcEnergyReqWaterHeating();
    calcElecForAbove();
}

void WaterHeatingEnergy::initVars()
{
  m_effcyOfWaterHeater = cf.getValueInt("EnergyRequirements:effcyOfWaterHeater");
  m_energyReqdForWaterHeat = cf.getValueDouble("EnergyRequirements:energyReqdForWaterHeat");
  m_elecForCentralHeatingPumps = cf.getValueDouble("EnergyRequirements:elecForCentralHeatingPumps");
  m_elecForFanAssitFlue = cf.getValueDouble("EnergyRequirements:elecForFanAssitFlue");
  m_elecForWarmHeatingSysFans = cf.getValueDouble("EnergyRequirements:elecForFanAssitFlue");
  m_elecForMechVentilation = cf.getValueDouble("EnergyRequirements:elecForMechVentilation");
  m_elecForKeepHotGasCombi = cf.getValueDouble("EnergyRequirements:elecForKeepHotGasCombi");
  m_elecForSolarWaterPump = cf.getValueDouble("EnergyRequirements:elecForKeepHotGasCombi");
  m_totalForAboveElec = cf.getValueDouble("EnergyRequirements:totalForAboveElec");
  calcElecForAbove();
}

// END OF WaterHeating.cpp
